window.notificationtitle;
window.notificationbody;

window.confirmfunction = ""; //execute function after user confirmed.

function notify(style,position) {
    if(style == "error"){
        icon = "fa fa-exclamation";
    }else if(style == "warning"){
        icon = "fa fa-warning";
    }else if(style == "success"){
        icon = "fa fa-check";
    }else if(style == "info"){
        icon = "fa fa-question";
    }else{
        icon = "fa fa-circle-o";
    }
    $.notify({
        title: window.notificationtitle,
        text: window.notificationbody,
        image: "<i class='"+icon+"'></i>"
    }, {
        style: 'metro',
        className: style,
        globalPosition:position,
        showAnimation: "show",
        showDuration: 300,
        hideDuration: 200,
        autoHide: true,
        clickToHide: true
    });
}
function notify2(style,position,object) {
    $("#"+object).notify({
        text: window.notificationbody
    }, {
        style: 'metro',
        className: 'nonspaced',
        elementPosition:position,
        showAnimation: "show",
        showDuration: 0,
        hideDuration: 0,
        autoHide: false,
        clickToHide: true
    });
}

function autohidenotify(style,position,title,message,delay) {
    if(style == "error"){
        icon = "fa fa-exclamation";
    }else if(style == "warning"){
        icon = "fa fa-warning";
    }else if(style == "success"){
        icon = "fa fa-check";
    }else if(style == "info"){
        icon = "fa fa-question";
    }else{
        icon = "fa fa-circle-o";
    }
    $.notify({
        title: title,
        text: message,
        image: "<i class='fa fa-warning'></i>"
    }, {
        style: 'metro',
        className: style,
        globalPosition:position,
        showAnimation: "show",
        showDuration: 0,
        hideDuration: 0,
        autoHideDelay: delay,
        autoHide: true,
        clickToHide: true
    });
}

function nconfirm(title, question) {
    $.notify({
        title: title,
        text: question + '<div class="clearfix"></div><br><a class="btn btn-sm btn-default yes">Yes</a> <a class="btn btn-sm btn-danger no">No</a>',
        image: "<i class='fa fa-warning'></i>"
    }, {
        style: 'metro',
        className: "cool",
        showAnimation: "show",
        showDuration: 0,
        hideDuration: 0,
        autoHide: false,
        clickToHide: false
    });
}

$(function(){
    //listen for click events from this style
    $(document).on('click', '.notifyjs-metro-base .no', function() {
        //programmatically trigger propogating hide event
        window.notConfirmed();
        $(this).trigger('notify-hide');
    });
    $(document).on('click', '.notifyjs-metro-base .yes', function() {
        //show button text
        window.confirmfunction();
        //hide notification
        $(this).trigger('notify-hide');
    });
})