/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 24/12/16 09:23.
 */

"use strict";

//Init Global Variables


//Default settings


//Events


//Functions
function searchFilter(object){
    var filter = $("#"+object.id).val(), count = 0;
    var elements = 0;
    $(".search-filter li").each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
        if($(this).is(':visible')) {
            elements++;
        }
    });

    if(elements == 0) {
        window.searchListEmpty();
    }
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}


// Notifications
//--------------------------------------------------------------------
function notification(type, header, message){
    window.notificationtitle = header+' ';
    window.notificationbody = message;
    notify(type);
}
function objectNotification(style, position, object, message){
    window.notificationbody = message;
    notify3(style, position, object);
}

function dbError() {
    notification('error', 'Error occurred', 'Sorry! Error occurred while connecting to the database. Try again later');
}

function noConnection() {
    notification('warning', 'No Connection', 'Unable to establish a connection with the server. Please check your internet connection!');
}

function closeWidget(object) {
    $('#'+object).addClass("animated bounceOutUp");
    window.setTimeout(function () {
        if ($('#'+object).data("is-app")) {

            $('#'+object).removeClass("animated bounceOutUp");
            if ($('#'+object).hasClass("ui-draggable")) {
                $('#'+object).find(".widget-popout").click();
            }
            $('#'+object).hide();
            $("a[data-app='" + $('#'+object).attr("id") + "']").addClass("clickable");
        } else {
            $('#'+object).remove();
        }
    }, 300);
}

function closeModal(id){
    $('#'+id).removeClass('md-show');
}

function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('.modal').on('show.bs.modal', centerModal);
$(window).on("resize", function () {
    $('.modal:visible').each(centerModal);
});

function noConnection(){
    notification('error', 'You are not connected!', 'Please check your network connection.')
}