/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/12/16 10:42.
 */

"use strict";

//region Global Variables

//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
$(function(){
    // function from the jquery form plugin
    $('.frm-image-upload').ajaxForm({
        beforeSend:function(){
            $(".progress").show();
            $('#img-about-us-header').fadeTo(400, 0.4)
        },
        uploadProgress:function(event,position,total,percentComplete){
            $(".progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            $(".sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $(".progress").hide();
        },
        complete:function(response){
            var res = JSON.parse(response.responseText);
            var timestamp = new Date().getTime();
            var imageUrl = window.domain+'assets/images/team/'+res+'.jpg?lastmod='+timestamp;
            $('#profile-image').attr('src', imageUrl);
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});


$(function(){
    // function from the jquery form plugin
    $('.frm-main-image-upload').ajaxForm({
        beforeSend:function(){
            $("#website-image-upload-progress").show();
            $('#website-main-slider-img').fadeTo(100, 0.4)
        },
        uploadProgress:function(event,position,total,percentComplete){
            $("#website-main-image-upload-progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            $("#website-main-image-upload-sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $("#website-image-upload-progress").hide();
        },
        complete:function(response){
            var res = JSON.parse(response.responseText);
            var timestamp = new Date().getTime();
            var imageUrl = window.domain+'assets/images/main_slider/'+res+'.jpg?lastmod='+timestamp;
            $('#website-main-slider-img').attr('src', imageUrl);
            $('#website-main-slider-img').fadeTo(100, 1)
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});

$(function(){
    var dealerid = window.dealerid;

    // function from the jquery form plugin
    $('.frm-slider-image-upload').ajaxForm({
        beforeSend:function(){
            var id = window.lastImageId;
            // $(".progress").show();

            var newImage = '<div class="col-md-2" id="website-home-slider-'+id+'"> ' +
                '               <img id="website-home-slider-image-'+id+'" src="'+window.domain+'assets/images/loading/ring.gif" /> ' +
                '               <span onclick="removeSliderImage('+id+');"><i class="fa fa-times-circle text-red-1"></i></span> ' +
                '           </div>';
            $('#slider-image-container').append(newImage);
        },
        uploadProgress:function(event,position,total,percentComplete){
            // $(".progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            // $(".sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $(".progress").hide();
        },
        complete:function(response){
            var id = window.lastImageId;
            window.lastImageId = parseInt(id+1);
            var res = JSON.parse(response.responseText);

            var imageUrl = window.domain+'assets/images/home_slider/'+dealerid+'/thumb/'+res+'.jpg';
            $('#website-home-slider-image-'+id).attr('src', imageUrl);
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});


$(function(){
    var dealerid = window.dealerid;

    // function from the jquery form plugin
    $('.frm-logo-image-upload').ajaxForm({
        beforeSend:function(){
            var id = window.lastLogoId;
            // $(".progress").show();

            var newImage = '<div class="col-md-2" id="website-logo-'+id+'"> ' +
                '               <img id="website-logo-image-'+id+'" src="'+window.domain+'assets/images/loading/ring.gif" style="width: 100px; height: 80px; border: 2px solid #797979" /> ' +
                '               <span onclick="removeLogo('+id+');"><i class="fa fa-times-circle text-red-1"></i></span> ' +
                '           </div>';
            $('#logo-image-container').append(newImage);
        },
        uploadProgress:function(event,position,total,percentComplete){
            // $(".progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
            // $(".sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            $(".progress").hide();
        },
        complete:function(response){
            var id = window.lastLogoId;
            window.lastLogoId = parseInt(id+1);
            var res = JSON.parse(response.responseText);

            var imageUrl = window.domain+'assets/images/logos/'+dealerid+'/'+res+'.jpg';
            $('#website-logo-image-'+id).attr('src', imageUrl);
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});

$(function(){
    // function from the jquery form plugin
    $('.frm-profile-image-upload').ajaxForm({
        beforeSend:function(){
            var id = window.teamId;
            $("#profile-image-upload-progress-"+id).show();
            $('#website-profile-image-'+id).fadeTo(100, 0.4);
        },
        uploadProgress:function(event,position,total,percentComplete){
            var id = window.teamId;
            $("#profile-image-upload-progress-"+id).width(percentComplete+'%'); //dynamicaly change the progress bar width
            $(".sr-only").html(percentComplete+'%'); // show the percentage number
        },
        success:function(){
            var id = window.teamId;
            $(".progress-"+id).hide();
        },
        complete:function(response){
            var id = window.teamId;
            var timestamp = new Date().getTime();
            var imageUrl = window.domain+'assets/images/team/'+id+'.jpg?lastmod='+timestamp;
            $('#website-profile-image-'+id).attr('src', imageUrl);
            $('#website-profile-image-'+id).fadeTo(100, 1)
            // show the image after success
        }
    });

    //set the progress bar to be hidden on loading
    $(".progress").hide();
});
//endregion