/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 21/12/16 11:29.
 */

"use strict";

//Init Global Variables


//Default settings


//Events


//Functions
function validateName() {
    var id = $('#activate-name');
    var name = id.val();

    if(name == '' || name == null) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff2526;"><i class="fa fa-exclamation-circle"></i> Please enter your name</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }
}

function validatePhone() {
    var id = $('#activate-phone');
    var phone = id.val();

    if(phone == '' || phone == null) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff2728;"><i class="fa fa-exclamation-circle"></i> Please enter your phone number</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }

    if(phone.length < 5) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff2728;"><i class="fa fa-exclamation-circle"></i> Please enter a valid phone number</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }
}

function validatePassword() {
    var id = $('#activate-password');
    var password = id.val();

    if(password == '' || password == null) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff2728;"><i class="fa fa-exclamation-circle"></i> Please enter a New Password here</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }

    if(password.length < 6) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff2122;"><i class="fa fa-exclamation-circle"></i> Password should have 6 characters minimum.</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }
}

function validateConfirmPassword() {
    var id = $('#activate-confirm-password');
    var confirmPassword = id.val();
    var password = $('#activate-password').val();

    if(password != confirmPassword) {
        if(!id.parent().hasClass('has-error')) {
            var idName = id.attr('id');
            id.parent().addClass('has-error');
            id.parent().append('<span id="'+idName+'-error-msg" class="error-msg" style="color: #ff4d4d;"><i class="fa fa-exclamation-circle"></i> Passwords does not match</span>').hide().fadeIn(300);
        }
    }
    else {
        removeError(id);
    }
}

function removeError(id) {
    id.parent().removeClass('has-error');
    var idName = id.attr('id');
    $('#'+idName+'-error-msg').fadeOut(200, function () {
        $(this).remove();
    })
}

$('.only-numbers').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if (e.keyCode == 43) {

        }
        else {
            return false;
        }
    }
});

function activate() {
    $('#activate-btn-activate').fadeOut(100, function () {
        $('#activate-loading-img').fadeIn(200);
    })
    var name = $.trim($('#activate-name').val());
    var phone = $.trim($('#activate-phone').val());
    var email = $.trim($('#activate-email').val());
    var password = $.trim($('#activate-password').val());
    var confirmPassword = $.trim($('#activate-confirm-password').val());
    var code = $.trim($('#active-code').val());

    if(name == '' || phone == '' || password == '' || confirmPassword == '') {
        $('#activate-name').trigger('blur');
        $('#activate-phone').trigger('blur');
        $('#activate-password').trigger('blur');
    }
    else if(password != confirmPassword) {
        $('#activate-confirm-password').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/active/{'+code+'}',
            data: {name:name, phone:phone, password:password, email:email, code:code},
            success: function (data) {
                if(data == 1) {
                    $('#activation-form').slideUp(300, function () {
                        $('#activate-success').slideDown(300);
                    });
                }
                else {
                    alert('Account cant acctivate. Try later...')
                    $('#activate-loading-img').fadeOut(200, function () {
                        $('#activate-btn-activate').fadeIn(300);
                    })
                }
            },
            error: function (xhr, textShort, errorThrown) {
                alert('Oops... Error occurred!');
                $('#activate-loading-img').fadeOut(200, function () {
                    $('#activate-btn-activate').fadeIn(300);
                })
            }
        });
    }
}