/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/12/16 09:11.
 */

"use strict";

//region Global Variables

//endregion

//region Default settings

//endregion

//region Events

//endregion

//region Functions
function changeInfo(object, type) {
    var id = $('#'+object.id);
    var value = id.val();

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/my-profile/change-info',
        data: {type:type, value:value},
        success: function (data) {
            if(data == 1)
            notification('success', 'Done', 'Successfully changed.');
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function iSwitchOnChange(id, status) {
    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/my-profile/change-info',
        data: {type:'visible', value:status},
        success: function (data) {
            if(data == 1)
                notification('success', 'Done', 'Your profile is '+(status == 1 ? 'visible' : 'hidden')+' from the website.');
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function overlayClicked() {
    $('#profile-image-choose').trigger('click');
}

function submitForm() {
    $('#profile-image-upload').submit();
}
//endregion