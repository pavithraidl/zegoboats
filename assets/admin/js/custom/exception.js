/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 15/12/16 15:13.
 */

"use strict";

//Init Global Variables
window.paginate = 1;
window.processing = false;
window.dataEnd = false;


//Default settings
var el = $('#btn-exception-reload').parents(".widget:first");
blockUI(el);
getExceptionList(1);

//Events
$(window).scroll(function () {
    var elementHeight = $('#exception-table-data').height();
    var scrollPosition = $('body').scrollTop() + 616;
    if (elementHeight < scrollPosition + 200) {
        if (window.dataEnd == false && window.processing == false) {
            getExceptionList(window.paginate);
        }
    }
});


//Functions
function getExceptionList(paginate) {
    window.processing = true;
    var el = $('#btn-exception-reload').parents(".widget:first");

    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'admin/exception/get-exception-list',
        data:{paginate:paginate, timezone:window.timezone},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                var tableData = "";
                var rows = 0;

                if(res != null) {
                    rows = res.length;
                }
                else {
                    rows = 0;
                }

                if(rows > 9) {
                    window.paginate++;
                }
                else if(rows > 0){
                    window.dataEnd == true;
                }
                else {
                    window.dataEnd == true;
                    setTimeout(function() {
                        unblockUI(el);
                        window.processing = false;
                    }, 600);
                }

                for(var i = 0; i < rows; i++) {
                    tableData += ('  <tr>' +
                    '           <td>'+res[i].id+'</td>' +
                    '           <td><strong>'+res[i].controller+'</strong></td>' +
                    '           <td>'+res[i].func+'</td>' +
                    '           <td>'+res[i].exe_msg+'</td>' +
                    '           <td><!--User--></td>' +
                    '           <td><!--Dealer--></td>' +
                    '           <td>'+(res[i].status == 2 ? '<span id="exception-status-'+res[i].id+'" class="label label-danger" style="cursor: pointer;" onclick="changeStatus('+res[i].id+');">Active</span>' : '<span id="exception-status-'+res[i].id+'" class="label label-success">Solved</span>')+'</td> ' +
                    '           <td data-toggle="tooltip" title="'+res[i].created_at_dt+'">'+res[i].created_at+'</td>' +
                    '       </tr>');
                }
                $('#exception-table-data').append(tableData);

                setTimeout(function() {
                    unblockUI(el);
                    window.processing = false;
                }, 600);
            }
            else {
                dbError();
                setTimeout(function() {
                    unblockUI(el);
                    window.processing = false;
                }, 600);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            setTimeout(function() {
                unblockUI(el);
                window.processing = false;
            }, 600);
            internetError();
        }
    });
}

function changeStatus(id) {
    $('#exception-status-'+id).text('Solved');
    $('#exception-status-'+id).removeClass('label-danger').addClass('label-success');
    jQuery.ajax({
        type: 'POST',
        url: window.domain + '/admin/exception/update-exception-status',
        data: {exception_id: id},
        success: function (data) {
            if (data != 0) {

            }
            else {
                $('#exception-status-'+id).removeClass('label-success').addClass('label-danger');
                $('#exception-status-'+id).text('Active');
                dbError();
            }

        },
        error: function (xhr, textStatus, errorThrown) {
            $('#exception-status-'+id).removeClass('label-success').addClass('label-danger');
            $('#exception-status-'+id).text('Active');
            internetError();
        }
    });
}