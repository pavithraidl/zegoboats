/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/12/16 12:31.
 */

"use strict";

//region Global Variables
window.removeImageId = null;
window.dealerid = $('#website-dealerid').val();
window.lastImageId = $('#website-last-image-id').val();
window.lastLogoId = $('#website-last-logo-id').val();
//endregion

//region Default settings
defaultIsSettings();
//endregion

//region Events

//endregion

//region Functions
function textBlockSave(id, block) {
    var value = ($(id).html());
    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/change-text-block',
        data: {value:value, block:block},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
                if(block == 1) {
                    window.textBlock1 = value;
                }
                else {
                    window.textBlock2 = value;
                }
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function publishValue(id, table, field) {
    var value = ($(id).val());

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/publish-value',
        data: {value:value, table:table, field:field},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
                if(table == 'website_video') {
                    updateVideoLink();
                }
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function publishTeamInfo(id, field, userid) {
    var value = ($(id).val());
    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/publish-team-info',
        data: {value:value, userid:userid, field:field},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function publishTestimonials(id, field, row) {
    var value = ($(id).val());
    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/publish-testimonials',
        data: {value:value, row:row, field:field},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function updateVideoLink() {
    var link = $('#website-video-url').val();

    $('#website-video').attr('src', link);
}

function cancelTextBlockEdit(id, block) {
    if(block == 1)
    $(id).html(window.textBlock1);
    else
        $(id).html(window.textBlock2);
}

function chooseMainImage() {
    $('#website-main-image-choose').trigger('click');
}

function chooseProfileImage(id) {
    $('#profile-image-choose-'+id).trigger('click');
}

function submitForm(id) {
    $(id).submit();
}

function submitTeamForm(id, teamId) {
    window.teamId = teamId;
    $(id).submit();
}

function removeSliderImage(id) {
    $('#website-home-slider-'+id).fadeTo(500, 0.5);
    window.removeImageId = id;
    window.removeImageType = 1;
    nconfirm('Are you sure', 'Do you really want to delete this image?');
}
window.confirmfunction = (function () {
    var id = window.removeImageId;
    var type = window.removeImageType;

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'admin/website/remove-slider-image',
        data: {id: id, type:type},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
                if(type == 1) {
                    $('#website-home-slider-'+id).fadeTo(400, 1);
                }
                else {
                    $('#website-logo-'+id).fadeTo(400, 1);
                }
            }
            else if(data == 1) {
                notification('success', 'Done', 'You have successfully removed that image');
                if(type == 1) {
                    $('#website-home-slider-'+id).fadeTo(400, 0, function () {
                        $('#website-home-slider-'+id).remove();
                    });
                }
                else {
                    $('#website-logo-'+id).fadeTo(400, 0, function () {
                        $('#website-logo-'+id).remove();
                    });
                }

            }
            else if(data == 0) {
                if(type == 1) {
                    $('#website-home-slider-'+id).fadeTo(400, 1);
                }
                else {
                    $('#website-logo-'+id).fadeTo(400, 1);
                }
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
});

window.notConfirmed = (function () {
    var id = window.removeImageId;
    $('#website-home-slider-'+id).fadeTo(300, 1);
    $('#website-logo-'+id).fadeTo(300, 1);
});

function openSliderImageFile() {
    $('#website-slider-image-choose').trigger('click');
}

function openLogoImageFile() {
    $('#website-logo-image-choose').trigger('click');
}


function publishImageDetails(id, field, imageId) {
    var value = ($(id).val());

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/publish-image-details',
        data: {value:value, field:field, imageid:imageId},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

function removeLogo(id) {
    $('#website-logo-'+id).fadeTo(500, 0.5);
    window.removeImageType = 2;
    window.removeImageId = id;
    nconfirm('Are you sure', 'Do you really want to delete this image?');
}


// is checkbox controls
function defaultIsSettings() {
    $('#is-website-visibility').removeClass('on');
}

function iSwitchOnChange(id, status) {
    if(id.substring(0, 3) == 'tm-') {
        var teamId = id.slice(3);
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'admin/website/change-team-visibility',
            data: {id:teamId, status:status},
            success: function (data) {
                if(data == 1) {

                }
                else
                    dbError();
            },
            error: function (xhr, shortText, errorThrown) {
                noConnection();
            }
        });
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'admin/website/change-visibility',
            data: {field:id, status:status},
            success: function (data) {
                if(data == 1) {
                    if(status == 1) {
                        var msg = id.capitalize()+' is visible on the website now';
                        notification('success', 'Visible', msg);
                    }
                    else if(status == 0) {
                        var msg = id.capitalize()+' is not visible on the website now';
                        notification('success', 'Hidden', msg);
                    }
                }
                else
                    dbError();
            },
            error: function (xhr, shortText, errorThrown) {
                noConnection();
            }
        });
    }
}

function publishFeatures(id, field, column) {
    var value = $.trim(($(id).val()));

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'admin/website/publish-features',
        data: {value:value, field:field, column:column},
        success: function (data) {
            if(data == 1) {
                notification('success', 'Published', 'Changes published to the website.');
            }
            else
                dbError();
        },
        error: function (xhr, shortText, errorThrown) {
            noConnection();
        }
    });
}

//endregion