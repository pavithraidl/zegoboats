/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 22/12/16 10:03.
 */

"use strict";

//region Global Variables
window.selectedDealer = null;
window.ownDealerId = null;
window.aLPaginate = 0;
//endregion

//region Default Settings
getDealersList();
//endregion

//region Events
$('#mm-btn-dealer-add').on('click', function () {
    addDealer();
});

$('#dealer-tab-activity-log').on('click', function () {
    triggerActivityListLoad();
});
//endregion

//region Functions
//region OnLoad Functions
//Load the dealers list in the left panel
function getDealersList() {
    var el = $('#dealer-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain+'admin/dealers/get-dealers-list',
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                var htmlAppend = '';

                if(window.selectedDealer == null) {
                    window.ownDealerId = res[0].id;
                    window.selectedDealer = res[0].id;
                }

                for(var i = 0; i < res.length; i++) {
                    htmlAppend += '  <li id="dealer-list-item-'+res[i].id+'" class="user-list-1 ' + (res[i].id == window.selectedDealer ? 'user-list-1-selected' : '') + '" onclick="selectDealer(' + res[i].id + ');" ' + (res[i].status == 3 ? 'data-toggle="tooltip" title="Account not activated yet"' : "") + (res[i].status ==2 ? 'style="opacity: 0.4;"' : '') + '>' +
                        '                   <div class="row" style="margin-bottom: -15px;"> ' +
                        '                       <div class="col-md-3 col-xs-4"> ' +
                        '                           <span class="rounded-image topbar-profile-image"> ' +
                        '                               <img id="dealer-avatar-'+res[i].id+'" ' + (res[i].status == 3 ? 'class="gray-scale"' : "") + ' src="'+window.domain+'/assets/admin/img/dealer/default/logo-40.png"> ' +
                        '                           </span> ' +
                        '                       </div> ' +
                        '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;"> ' +
                        '                           <div class="row"> ' +
                        '                               <span id="o-m-org-list-name-1" class="text-blue-3 pull-left" style="font-size: 15px;' + (res[i].status == 3 ? 'color:#A4A4A4 !important;' : "") + '"><strong>'+res[i].identity+'</strong></span> ' +
                        '                               <span id="o-m-org-list-loading-container-1"></span> ' +
                        '                           </div> ' +
                        '                       <div class="row"> ' +
                        '                       <p id="o-m-org-list-roll-1" style="color: rgba(100, 100, 100, 0.31); font-size:12px;">'+res[i].name+'</p> ' +
                        '                       </div>' +
                        '                           <input type="hidden" id="o-m-active-status-1" value="Value" /> ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <hr/> ' +
                        '               </li>';
                }
                getDealerBasicDetails();

                $('#dealer-dealer-list-container').html(htmlAppend);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    });
}

//get the basic details of the dealer located in the middle panel
function getDealerBasicDetails() {
    var el = $('#dealer-manage-widget');
    blockUI(el);
    var dealerId = window.selectedDealer;
    jQuery.ajax({
        type: 'GET',
        url: window.domain+'admin/dealer/get-dealer-basic-details',
        data: {dealerid:dealerId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                $('.ios-switch-md').removeClass('on');
                $('.ios-switch-sm').removeClass('on');
                if(res.status == 3) {
                    $('#is-dealer-status').fadeOut(200).addClass('on');
                    $('#dealer-image').addClass('gray-scale');
                    $('#dealer-idntity').attr('style', 'color:#A4A4A4 !important;');
                    $('#dealer-status-icon').addClass('text-gray-3').removeClass('text-green-2').removeClass('text-yellow-1');
                    $('#dealer-status-text').text('Pending Activation');
                    $('#dealer-url').attr('href', 'http://' + res.url);
                    $('#dealer-resend-activation-email').fadeIn(300);
                }
                else if(res.status == 2) {
                    $('#is-dealer-status').removeClass('on');
                    $('#dealer-image').removeClass('gray-scale');
                    $('#dealer-idntity').removeAttr('style');
                    $('#dealer-status-icon').addClass('text-yellow-1').removeClass('text-green-2').removeClass('text-gray-3');
                    $('#dealer-status-text').text('Deactivated');
                    $('#dealer-url').attr('href', 'http://' + res.url);
                    $('#dealer-resend-activation-email').fadeOut(300);

                }
                else if(res.status == 1) {
                    $('#is-dealer-status').addClass('on');
                    $('#dealer-image').removeClass('gray-scale');
                    $('#dealer-idntity').removeAttr('style');
                    $('#dealer-status-icon').addClass('text-green-2').removeClass('text-yellow-1').removeClass('text-gray-3');
                    $('#dealer-status-text').text('Active');
                    $('#dealer-url').attr('href', res.url);
                    $('#dealer-resend-activation-email').fadeOut(300);

                }

                //add values to settings input fields
                $('#dealer-settings-name').val(res.name);
                $('#dealer-settings-identity').val(res.identity);
                $('#dealer-settings-email').val(res.email);
                $('#dealer-settings-url').val(res.url);

                if(window.selectedDealer == window.ownDealerId) {
                    $('#is-dealer-status').fadeOut(200);
                    $('#dealer-tab-access').fadeOut(200);
                    $('#user-delete-user').fadeOut(200);
                    $('#dealer-delete-dealer').fadeOut(200);
                    $('#dealer-tab-container-access').removeClass('active in');
                    $('#dealer-tab-container-activity-log').removeClass('active in');
                    $('#dealer-tab-access').removeClass('active');
                    $('#dealer-tab-activity-log').removeClass('active');
                    $('#dealer-tab-settings').addClass('active');
                    $('#dealer-tab-container-settings').addClass('active in');
                }
                else {
                    $('#dealer-tab-access').fadeIn(200).removeClass('active');
                    $('#dealer-tab-container-access').removeClass('active in');
                    $('#dealer-tab-container-activity-log').removeClass('active in');
                    $('#dealer-tab-activity-log').removeClass('active');
                    $('#dealer-tab-settings').addClass('active');
                    $('#dealer-tab-container-settings').addClass('active in');
                    $('#dealer-delete-dealer').fadeIn(200);
                }

                if(res.phone == null || res.phone == '') {
                    $('#dealer-contact').fadeOut(200);
                }
                else {
                    $("#dealer-contact").fadeIn(200);
                    $('#dealer-contact').attr('href', 'tel:'+res.phone);
                }

                if(res.phone == null || res.phone == '') {
                    $('#dealer-contact').fadeOut(200);
                }
                else {
                    $("#dealer-contact").fadeIn(200);
                    $('#dealer-contact').attr('href', 'tel:'+res.phone);
                }

                if(res.email == null || res.email == '') {
                    $('#dealer-email').fadeOut(200);
                    $('#dealer-resend-activation-email').fadeOut(200);
                }
                else {
                    $("#dealer-email").fadeIn(200);
                    $('#dealer-email').attr('href', 'mailto:'+res.email);
                }

                if(res.visibility == 1) {
                    $('#is-dealer-visibility').addClass('on');
                }

                $('#dealer-idntity').text(res.identity);
                $('#dealer-name').text(res.name);
                $('#dealer-settings-business_park').val(res.businesspark);
                $('#dealer-settings-street').val(res.street);
                $('#dealer-settings-town').val(res.town);
                $('#dealer-settings-state').val(res.state);
                $('#dealer-settings-zip').val(res.zip);
                $('#dealer-settings-country').val(res.country);
                $('#dealer-settings-phone').val(res.phone);
                $('#dealer-settings-map_link').val(res.maplink);
                $('#dealer-settings-order').val(res.order);

                changeAccess();

                unblockUI(el);
            }


        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    })
}
//endregion

//region Object driven functions
function selectDealer(id) {
    $('.user-list-1-selected').removeClass('user-list-1-selected');
    $('#dealer-list-item-'+id).addClass('user-list-1-selected');
    window.selectedDealer = id;
    if(window.selectedDealer == window.ownDealerId) {
        $('#dealer-delete-dealer').fadeOut(200);
        $('#is-dealer-status').fadeOut(200);
    }
    else {
        $('#dealer-delete-dealer').fadeIn(200);
        $('#is-dealer-status').fadeIn(200)
    }
    getDealerBasicDetails();
    window.aLPaginate = 0;
    $('#dealer-activity-list').html('');
    triggerActivityListLoad();
}

function iSwitchOnChange(id, status) {
    var dealerId = window.selectedDealer;
    var el = $('#dealer-manage-widget');
    blockUI(el);

    if (id == 'dealer-status') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'admin/dealer/change-dealer-status',
            data: {dealerid:dealerId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 1) {
                    getDealersList();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-sy') {
        var systemId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/dealer/change-system-access',
            data: {systemid: systemId, status:status, selecteddealer:window.selectedDealer},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-op') {
        var operationId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/dealer/change-operation-access',
            data: {operationid: operationId, status:status, selecteddealer:window.selectedDealer},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else if(id == 'dealer-visibility') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/dealer/change-dealer-visibility',
            data: {status:status, dealerid:window.selectedDealer},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else {
        unblockUI(el);
    }
}

function deleteDealer() {
    nconfirm('Delete Permanently !', 'Do you really want to remove this dealer account ?');
}
window.confirmfunction = (function () {
    var dealerId = window.selectedDealer;

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'admin/dealer/remove-dealer-account',
        data: {dealerid: dealerId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 1) {
                notification('success', 'Done', 'You have successfully removed that dealer account');
                window.selectedDealer = null;
                getDealersList();
            }
            else if(data == 0) {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
});

window.notConfirmed = (function () {});

function resendActivationEmail() {
    var dealerId = window.selectedDealer;
    var el = $('#dealer-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'admin/dealer/resend-activation-email',
        data: {dealerid: dealerId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                notification('success', 'Sent', 'Activation email has resent to '+res);
                getDealersList();
            }
            else {
                dbError();
            }
            unblockUI(el);
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    });
}

function triggerActivityListLoad() {
    if (window.aLPaginate == 0) {
        var dealerId = window.selectedDealer;
        getActivityLog(1, dealerId);
    }
}

function changeAccess() {
    var dealerId = window.selectedDealer;
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'admin/dealer/get-access-list',
        data: {dealerid: dealerId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                for(var i = 0; res.sys.length > i; i++) {
                    $("#is-sa-sy-"+res.sys[i]).addClass('on');
                }
                for(var i = 0; res.ope.length > i; i++) {
                    $("#is-sa-op-"+res.ope[i]).addClass('on');
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function getActivityLog(paginate, dealerId) {
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        var loading = (
            '               <li id="dealer-activity-list-loading" class="media">' +
            '                   <p style="text-align: center;">' +
            '                       <img src="' + window.domain + '/assets/admin/img/loading/loading.gif" /> ' +
            '                   </p>' +
            '               </li>'
        );
        $('#dealer-activity-list').append(loading);
        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'admin/dealer/get-activity-list',
            data: {paginate: paginate, dealerid: dealerId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == -1) {
                    temPaginate = -1;
                    $('#dealer-activity-list-loading').fadeOut(250, function () {
                        $('#dealer-activity-list-loading').remove();
                    });
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }
                    $('#dealer-activity-list-loading').fadeOut(250, function () {
                        $('#dealer-activity-list-loading').remove();
                        $('#dealer-activity-list').append(activityList);
                        window.aLPaginate = temPaginate;
                        window.aLPaginate++;
                    });
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function checkEmailDuplicate() {
    var email = $.trim($('#mm-dealer-email').val());
    var id = $('#mm-dealer-email');

    if(email != '') {
        jQuery.ajax({
            type: 'GET',
            url: window.domain+'/admin/check-user-email-duplicate',
            data: {email:email},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == -1) {
                    errorExecution(id, 'This email has already used in the system.', 'email');
                }
                else if(data == 0) {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}


function addDealer() {
    var el = $('#add-new-dealer-widget');
    blockUI(el);
    var identity = $.trim($('#mm-dealer-identity').val());
    var name = $.trim($('#mm-dealer-name').val());
    var email = $.trim($('#mm-dealer-email').val());
    var emailId = $('#mm-dealer-email');
    var url = $.trim($('#mm-dealer-url').val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if(identity == '' || name == '' || url == '') {
        $('#mm-dealer-identity').trigger('blur');
        $('#mm-dealer-name').trigger('blur');
        $('#mm-dealer-email').trigger('blur');
        $('#mm-dealer-url').trigger('blur');
    }
    if(email != '' && !re.test(email)) {
        $('#mm-dealer-email').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'/admin/dealers/add-dealer',
            data: {identity:identity, name:name, email:email, url:url},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0){
                    dbError();
                }
                else if(data == -1) {
                    errorExecution(emailId, 'Sorry! This email address already has used in the system.', 'duplicate');
                }
                else {
                    getDealersList();
                    $('#mm-btn-dealer-cancel').trigger('click');
                    notification('success', 'Done', 'You have successfully added a new dealer to the system.');
                }
                unblockUI(el);
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }

}

function cancelDealer() {
    $('#mm-dealer-identity').val('');
    $('#mm-dealer-name').val('');
    $('#mm-dealer-email').val('');
    $('#mm-dealer-url').val('');

    $('#model-add-new-dealer .has-error').removeClass('has-error');
    $('#model-add-new-dealer .error-msg').remove();
}

function saveSettings(type) {
    var value = $('#dealer-settings-'+type).val();
    var dealerid = window.selectedDealer;

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'/admin/dealers/save-settings',
        data: {value:value, dealerid:dealerid, type:type},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 0){
                dbError();
            }
            else {
                getDealersList();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function saveEmail() {
    var email = $.trim($('#dealer-settings-email').val());
    var emailId = $('#dealer-settings-email');
    var dealerid = window.selectedDealer;


    jQuery.ajax({
        type: 'POST',
        url: window.domain+'/admin/dealers/save-email',
        data: {dealerid:dealerid, email:email},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == -1) {
                errorExecution(emailId, 'Sorry! This email address already has used in the system.', 'duplicate');
            }
            else if(data == 0){
                dbError();
            }
            else {
                getDealersList();
                removeError(email, 'duplicate');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });

}
//endregion
//endregion