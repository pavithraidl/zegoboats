/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 25/12/16 08:27.
 */

"use strict";

//region Global Variables
window.selectedUser = null;
window.ownUserId = null;
//endregion

//region Default settings
getUsersList();
//endregion

//region Events

//endregion

//region Functions
function getUsersList() {
    var el = $('#user-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.domain+'admin/users/get-users-list',
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                var htmlAppend = '';

                if(window.selectedUser == null) {
                    window.ownUserId = res[0].id;
                    window.selectedUser = res[0].id;
                }

                for(var i = 0; i < res.length; i++) {
                    htmlAppend += '  <li id="user-list-item-'+res[i].id+'" class="user-list-1 ' + (res[i].id == window.selectedUser ? 'user-list-1-selected' : '') + '" onclick="selectUser(' + res[i].id + ');" ' + (res[i].status == 3 ? 'data-toggle="tooltip" title="Account not activated yet"' : "") + (res[i].status ==2 ? 'style="opacity: 0.4;"' : '') + '>' +
                        '                   <div class="row" style="margin-bottom: -15px;"> ' +
                        '                       <div class="col-md-3 col-xs-4"> ' +
                        '                           <span class="rounded-image topbar-profile-image"> ' +
                        '                               <img id="user-avatar-'+res[i].id+'" ' + (res[i].status == 3 ? 'class="gray-scale"' : "") + ' src="'+res[i].avatar+'"> ' +
                        '                           </span> ' +
                        '                       </div> ' +
                        '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;"> ' +
                        '                           <div class="row"> ' +
                        '                               <span id="o-m-org-list-name-1" class="text-blue-3 pull-left" style="font-size: 15px;' + (res[i].status == 3 ? 'color:#A4A4A4 !important;' : "") + '"><strong>'+res[i].name+'</strong></span> ' +
                        '                               <span id="o-m-org-list-loading-container-1"></span> ' +
                        '                           </div> ' +
                        '                       <div class="row"> ' +
                        '                       <p id="o-m-org-list-roll-1" style="color: rgba(100, 100, 100, 0.31); font-size:12px;">'+res[i].jobtitle+'</p> ' +
                        '                       </div>' +
                        '                           <input type="hidden" id="o-m-active-status-1" value="Value" /> ' +
                        '                       </div> ' +
                        '                   </div> ' +
                        '                   <hr/> ' +
                        '               </li>';
                }
                getUserBasicDetails();

                $('#user-list-container').html(htmlAppend);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    });
}

function addUser() {
    var el = $('#add-new-user-widget');
    blockUI(el);
    var name = $.trim($('#mm-user-name').val());
    var email = $.trim($('#mm-user-email').val());
    var emailId = $('#mm-user-email');
    var jobTitle = $.trim($('#mm-user-job-title').val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(name == '' || jobTitle == '') {
        $('#mm-user-name').trigger('blur');
        $('#mm-user-email').trigger('blur');
        $('#mm-user-job-title').trigger('blur');
    }
    if(email != '' && !re.test(email)) {
        $('#mm-user-email').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'admin/users/add-user',
            data: {name:name, email:email, jobtitle: jobTitle},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0){
                    dbError();
                }
                else if(data == -1) {
                    errorExecution(emailId, 'Sorry! This email address already has used in the system.', 'duplicate');
                }
                else {
                    getUsersList();
                    $('#mm-btn-user-cancel').trigger('click');
                    notification('success', 'Done', 'User has successfully added to the system');
                }
                unblockUI(el);
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }

}

function cancelUser() {
    $('#mm-user-name').val('');
    $('#mm-user-email').val('');

    $('#model-add-new-user .has-error').removeClass('has-error');
    $('#model-add-new-user .error-msg').remove();
}

function getUserBasicDetails() {
    var el = $('#user-manage-widget');
    blockUI(el);
    var userId = window.selectedUser;
    jQuery.ajax({
        type: 'GET',
        url: window.domain+'admin/user/get-user-basic-details',
        data: {userid:userId},
        success: function (data) {
            if(data == 0) {
                dbError();
            }
            else if(data == 'Unauthorized') {
                window.location.reload();
            }
            else {
                var res = JSON.parse(data);
                $('.ios-switch-md').removeClass('on');
                $('.ios-switch-sm').removeClass('on');
                if(res.status == 3) {
                    $('#is-user-status').fadeOut(200).addClass('on');
                    $('#user-image').addClass('gray-scale').attr('src', res.avatar);
                    $('#user-name').attr('style', 'color:#A4A4A4 !important;');
                    $('#user-status-icon').addClass('text-gray-3').removeClass('text-green-2').removeClass('text-yellow-1');
                    $('#user-status-text').text('Pending Activation');
                    $('#user-resend-activation-email').fadeIn(300);
                }
                else if(res.status == 2) {
                    $('#is-user-status').removeClass('on');
                    $('#user-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#user-name').removeAttr('style');
                    $('#user-status-icon').addClass('text-yellow-1').removeClass('text-green-2').removeClass('text-gray-3');
                    $('#user-status-text').text('Deactivated');
                    $('#user-resend-activation-email').fadeOut(300);

                }
                else if(res.status == 1) {
                    $('#is-user-status').addClass('on');
                    $('#user-image').removeClass('gray-scale').attr('src', res.avatar);
                    $('#user-idntity').removeAttr('style');
                    $('#user-status-icon').addClass('text-green-2').removeClass('text-yellow-1').removeClass('text-gray-3');
                    $('#user-status-text').text('Active');
                    $('#user-url').attr('href', 'http://' + res.url);
                    $('#user-resend-activation-email').fadeOut(300);
                }

                //add values to settings input fields
                $('#user-settings-name').val(res.name);
                $('#user-settings-email').val(res.email);
                $('#user-settings-url').val(res.url);

                if(window.selectedUser == window.ownUserId) {
                    $('#is-user-status').fadeOut(200).removeClass('active');
                    $('#user-tab-access').fadeOut(200);
                    $('#user-tab-access-container').removeClass('active in');
                    $('#user-tab-activity-log').addClass('active');
                    $('#user-tab-activity-log-container').addClass('active in');
                    $('#user-delete-user').fadeOut(200);
                }
                else {
                    $('#user-tab-access').fadeIn(200).removeClass('active');
                    $('#user-tab-access-container').removeClass('active in');
                    $('#user-tab-activity-log').addClass('active');
                    $('#user-tab-activity-log-container').addClass('active in');
                    $('#user-delete-user').fadeIn(200);
                }

                if(res.phone == null || res.phone == '') {
                    $('#user-contact').fadeOut(200);
                }
                else {
                    $("#user-contact").fadeIn(200);
                    $('#user-contact').attr('href', 'tel:'+res.phone);
                }

                if(res.phone == null || res.phone == '') {
                    $('#user-contact').fadeOut(200);
                }
                else {
                    $("#user-contact").fadeIn(200);
                    $('#user-contact').attr('href', 'tel:'+res.phone);
                }

                if(res.email == null || res.email == '') {
                    $('#user-email').fadeOut(200);
                    $('#user-resend-activation-email').fadeOut(200);
                }
                else {
                    $("#user-email").fadeIn(200);
                    $('#user-email').attr('href', 'mailto:'+res.email);
                }

                $('#user-name').text(res.name);
                $('#user-job-title').text(res.jobtitle);

                changeAccess();
                var userId = window.selectedUser;
                getActivityLog(1, userId);

                unblockUI(el);
            }


        },
        error: function (xhr, textShort, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    })
}
//endregion

//region Object driven functions
function selectUser(id) {
    $('.user-list-1-selected').removeClass('user-list-1-selected');
    $('#user-list-item-'+id).addClass('user-list-1-selected');
    window.selectedUser = id;
    if(window.selectedUser == window.ownUserId) {
        $('#user-delete-user').fadeOut(200);
        $('#is-user-status').fadeOut(200);
    }
    else {
        $('#user-delete-user').fadeIn(200);
        $('#is-user-status').fadeIn(200)
    }
    getUserBasicDetails();
    window.aLPaginate = 0;
    $('#user-activity-list').html('');
    triggerActivityListLoad();
}

function iSwitchOnChange(id, status) {
    var userId = window.selectedUser;
    var el = $('#user-manage-widget');
    blockUI(el);

    if (id == 'user-status') {
        jQuery.ajax({
            type: 'POST',
            url: window.domain+'admin/user/change-user-status',
            data: {userid:userId, status:status},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 1) {
                    getUsersList();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-sy') {
        var systemId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/user/change-system-access',
            data: {systemid: systemId, status:status, selecteduser:window.selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
    else if(id.substring(0, 5) == 'sa-op') {
        var operationId = id.slice(6);
        jQuery.ajax({
            type: 'POST',
            url: window.domain + 'admin/user/change-operation-access',
            data: {operationid: operationId, status:status, selecteduser:window.selectedUser},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == 0) {
                    dbError();
                }
                unblockUI(el);
            },
            error: function (xhr, textShort, errorThrown) {
                noConnection();
                unblockUI(el);
            }
        });
    }
}

function deleteUser() {
    nconfirm('Delete Permanently !', 'Do you really want to remove this user account ?');
}
window.confirmfunction = (function () {
    var userId = window.selectedUser;

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'admin/user/remove-user-account',
        data: {userid: userId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == 1) {
                notification('success', 'Done', 'You have successfully removed that user account');
                window.selectedUser = null;
                getUsersList();
            }
            else if(data == 0) {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
});

window.notConfirmed = (function () {});

function resendActivationEmail() {
    var userId = window.selectedUser;
    var el = $('#user-manage-widget');
    blockUI(el);

    jQuery.ajax({
        type: 'POST',
        url: window.domain + 'admin/user/resend-activation-email',
        data: {userid: userId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                notification('success', 'Sent', 'Activation email has resent to '+res);
                getUsersList();
            }
            else {
                dbError();
            }
            unblockUI(el);
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
            unblockUI(el);
        }
    });
}

function triggerActivityListLoad() {
    if (window.aLPaginate == 0) {
        var userId = window.selectedUser;
        getActivityLog(1, userId);
    }
}

function changeAccess() {
    var userId = window.selectedUser;
    jQuery.ajax({
        type: 'GET',
        url: window.domain + 'admin/user/get-access-list',
        data: {userid: userId},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data != 0) {
                var res = JSON.parse(data);
                for(var i = 0; res.sys.length > i; i++) {
                    $("#is-sa-sy-"+res.sys[i]).addClass('on');
                }
                for(var i = 0; res.ope.length > i; i++) {
                    $("#is-sa-op-"+res.ope[i]).addClass('on');
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function getActivityLog(paginate, userId) {
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        var loading = (
            '               <li id="user-activity-list-loading" class="media">' +
            '                   <p style="text-align: center;">' +
            '                       <img src="' + window.domain + '/assets/admin/img/loading/loading.gif" /> ' +
            '                   </p>' +
            '               </li>'
        );
        if(!$('#user-activity-list-loading').length) {
            $('#user-activity-list').append(loading);
        }

        jQuery.ajax({
            type: 'GET',
            url: window.domain + 'admin/user/get-activity-list',
            data: {paginate: paginate, userid: userId},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == -1) {
                    temPaginate = -1;
                    $('#user-activity-list-loading').fadeOut(250, function () {
                        $('#user-activity-list-loading').remove();
                    });
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }
                    $('#user-activity-list-loading').fadeOut(250, function () {
                        $('#user-activity-list-loading').remove();
                        $('#user-activity-list').append(activityList);
                        window.aLPaginate = temPaginate;
                        window.aLPaginate++;
                    });
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function checkEmailDuplicate() {
    var email = $.trim($('#mm-user-email').val());
    var id = $('#mm-user-email');

    if(email != '') {
        jQuery.ajax({
            type: 'GET',
            url: window.domain+'/admin/check-user-email-duplicate',
            data: {email:email},
            success: function (data) {
                if(data == 'Unauthorized') {
                    window.location.reload();
                }
                else if(data == -1) {
                    errorExecution(id, 'This email has already used in the system.', 'email');
                }
                else if(data == 0) {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }

}

function saveEmail() {
    var email = $.trim($('#user-settings-email').val());
    var emailId = $('#user-settings-email');
    var userId = window.selectedUser;

    jQuery.ajax({
        type: 'POST',
        url: window.domain+'/admin/user/save-email',
        data: {userid:userId, email:email},
        success: function (data) {
            if(data == 'Unauthorized') {
                window.location.reload();
            }
            else if(data == -1) {
                errorExecution(emailId, 'Sorry! This email address already has used in the system.', 'duplicate');
            }
            else if(data == 0){
                dbError();
            }
            else {
                getDealersList();
                removeError(email, 'duplicate');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });

}

//endregion