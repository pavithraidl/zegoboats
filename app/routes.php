<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//region Frontend Routes
Route::get('/', array(
    'as' => 'Home',
    'uses' => 'HomeController@getHome'
));

Route::get('/dealers', array(
    'as' => 'dealers',
    'uses' => 'HomeController@getDealers'
));

Route::get('/contact', array(
    'as' => 'contact',
    'uses' => 'HomeController@getContact'
));

Route::post('/contact/send-email', array(
    'as' => 'contact-send-email',
    'uses' => 'HomeController@sendEmail'
));
//endregion

Route::group( array( 'before' => 'auth' ), function () {

    $access = new UserController();
    $access -> urlAccessControl();

    //region Website Routes
    Route::get('/admin', array(
        'as' => 'admin',
        'uses' => 'AdminController@getAdmin'
    ));

    Route::post('/admin/website/change-text-block', array(
        'as' => 'admin-website-change-text-block',
        'uses' => 'WebsiteController@changeTextBlock'
    ));

    Route::post('/admin/website/publish-value', array(
        'as' => 'admin-website-publish-value',
        'uses' => 'WebsiteController@publishValue'
    ));

    Route::post('/admin/website/publish-testimonials', array(
        'as' => 'admin-website-publish-testimonials',
        'uses' => 'WebsiteController@publishTestimonials'
    ));

    Route::post('/admin/website/remove-slider-image', array(
        'as' => 'admin-website-remove-slider-image',
        'uses' => 'WebsiteController@removeSliderImage'
    ));

    Route::post('/admin/website/change-main-image', array(
        'as' => 'admin-website-change-main-image',
        'uses' => 'WebsiteController@changeMainImage'
    ));

    Route::post('/admin/website/change-slider-image', array(
        'as' => 'admin-website-change-slider-image',
        'uses' => 'WebsiteController@changeSliderImage'
    ));

    Route::post('/admin/website/change-logo-image', array(
        'as' => 'admin-website-change-logo-image',
        'uses' => 'WebsiteController@changeLogoImage'
    ));

    Route::post('/admin/website/change-visibility', array(
        'as' => 'admin-website-change-visibility',
        'uses' => 'WebsiteController@changeVisibility'
    ));

    Route::post('/admin/website/change-team-visibility', array(
        'as' => 'admin-website-change-team-visibility',
        'uses' => 'WebsiteController@changeTeamVisibility'
    ));

    Route::post('/admin/website/profile-change-image', array(
        'as' => 'admin-website-profile-change-image',
        'uses' => 'WebsiteController@changeProfileImage'
    ));

    Route::post('/admin/website/publish-features', array(
        'as' => 'admin-website-publish-features',
        'uses' => 'WebsiteController@publishFeatures'
    ));

    Route::post('/admin/website/publish-team-info', array(
        'as' => 'admin-website-public-team-info',
        'uses' => 'WebsiteController@publishTeamInfo'
    ));

    Route::post('/admin/website/publish-image-details', array(
        'as' => 'admin/website/publish-image-details',
        'uses' => 'WebsiteController@publishImageDetails'
    ));
    //endregion

    //region Dealers Routes
    Route::get('/admin/dealers', array(
        'as' => 'admin-dealers',
        'uses' => 'DealersController@getDealers'
    ));

    Route::post('/admin/dealers/add-dealer', array(
        'as' => 'admin-dealers-add-dealer',
        'uses' => 'DealersController@addDealer'
    ));

    Route::get('/admin/dealers/get-dealers-list', array(
        'as' => 'admin-dealers-get-dealers-list',
        'uses' => 'DealersController@getDealersList'
    ));

    Route::get('/admin/dealer/get-dealer-basic-details', array(
        'as' => 'admin-dealer-get-dealer-basic-deatails',
        'uses' => 'DealersController@getDealerBasicDetails'
    ));

    Route::post('/admin/dealer/change-dealer-status', array(
        'as' => 'admin-dealer-change-dealer-status',
        'uses' => 'DealersController@postDealerStatus'
    ));

    Route::post('/admin/dealer/remove-dealer-account', array(
        'as' => 'admin-dealer-remove-dealer-account',
        'uses' => 'DealersController@postRemoveDealerAccount'
    ));

    Route::post('/admin/dealer/resend-activation-email', array(
        'as' => 'admin-dealer-resend-activation-email',
        'uses' => 'DealersController@postResendActivationEmail'
    ));

    Route::get('/admin/dealer/get-activity-list', array(
        'as' => 'admin-dealer-get-activity-list',
        'uses' => 'DealersController@getActivityList'
    ));

    Route::post('/admin/dealer/change-system-access', array(
        'as' => 'admin-dealer-change-system-access',
        'uses' => 'DealersController@changeDealerAccess'
    ));

    Route::post('/admin/dealer/change-operation-access', array(
        'as' => 'admin-dealer-change-operation-access',
        'uses' => 'DealersController@changeDealerOperationAccess'
    ));

    Route::get('/admin/dealer/get-access-list', array(
        'as' => 'admin-dealer-get-access-list',
        'uses' => 'DealersController@getAccessList'
    ));

    Route::get('/admin/check-user-email-duplicate', array(
        'as' => 'admin-check-user-email-duplicate',
        'uses' => 'UserController@checkDuplicateEmail'
    ));

    Route::post('/admin/dealers/save-settings', array(
        'as' => 'admin-dealer-save-settings',
        'uses' => 'DealersController@saveSettings'
    ));

    Route::post('/admin/dealers/save-email', array(
        'as' => 'admin-dealer-save-email',
        'uses' => 'DealersController@saveEmail'
    ));

    Route::post('/admin/dealer/change-dealer-visibility', array(
            'as' => 'admin-dealer-change-dealer-visibility',
            'uses' => 'DealersController@changeDealerVisibility'
        ));


    //endregion

    //region Users Routes
    Route::get('/admin/users', array(
        'as' => 'admin-users',
        'uses' => 'UserController@getUsers'
    ));

    Route::post('/admin/users/add-user', array(
        'as' => 'admin-users-add-user',
        'uses' => 'UserController@addUser'
    ));

    Route::get('/admin/users/get-users-list', array(
        'as' => 'admin-users-get-users-list',
        'uses' => 'UserController@getUserList'
    ));

    Route::get('/admin/user/get-user-basic-details', array(
        'as' => 'admin-user-get-user-basic-deatails',
        'uses' => 'UserController@getUserBasicDetails'
    ));

    Route::post('/admin/user/change-user-status', array(
        'as' => 'admin-user-change-user-status',
        'uses' => 'UserController@postUserStatus'
    ));

    Route::post('/admin/user/remove-user-account', array(
        'as' => 'admin-user-remove-user-account',
        'uses' => 'UserController@postRemoveUserAccount'
    ));

    Route::post('/admin/user/resend-activation-email', array(
        'as' => 'admin-user-resend-activation-email',
        'uses' => 'UserController@postResendActivationEmail'
    ));

    Route::get('/admin/user/get-activity-list', array(
        'as' => 'admin-user-get-activity-list',
        'uses' => 'UserController@getActivityList'
    ));

    Route::post('/admin/user/change-system-access', array(
        'as' => 'admin-user-change-system-access',
        'uses' => 'UserController@changeUserAccess'
    ));

    Route::post('/admin/user/change-operation-access', array(
        'as' => 'admin-user-change-operation-access',
        'uses' => 'UserController@changeUserOperationAccess'
    ));

    Route::get('/admin/user/get-access-list', array(
        'as' => 'admin-user-get-access-list',
        'uses' => 'UserController@getAccessList'
    ));

    Route::get('/admin/check-user-email-duplicate', array(
        'as' => 'admin-check-user-email-duplicate',
        'uses' => 'UserController@checkDuplicateEmail'
    ));

    Route::post('/admin/users/save-settings', array(
        'as' => 'admin-user-save-settings',
        'uses' => 'UserController@saveSettings'
    ));

    Route::post('/admin/user/save-email', array(
        'as' => 'admin-user-save-email',
        'uses' => 'UserController@saveEmail'
    ));
    //endregion

    //region My Profile
    Route::get('/admin/my-profile', array(
        'as' => 'admin-my-profile',
        'uses' => 'ProfileController@getMyProfile'
    ));

    Route::post('/admin/my-profile/change-info', array(
        'as' => 'admin-my-profile-change-info',
        'uses' => 'ProfileController@changeInfo'
    ));

    Route::post('/admin/profile/change-image', array(
        'as' => 'admin-profile-change-image',
        'uses' => 'ProfileController@changeImage'
    ));
    //endregion

    //region Exception Routes
    Route::get('/admin/exception', array(
        'as' => 'admin-exception',
        'uses' => 'ErrorController@getException'
    ));

    Route::get('/admin/exception/get-exception-list', array(
        'as' => 'admin-exception-get-exception-list',
        'uses' => 'ErrorController@getExceptionList'
    ));

    Route::post('/admin/exception/update-exception-status', array(
        'as' => 'admin-exception-update-exception-status',
        'uses' => 'ErrorController@postExceptionStatus'
    ));
    //endregion

    Route::get('/admin/icons', array(
        'as' => 'admin-icons',
        'uses' => 'HomeController@getIcons'
    ));

    Route::get( '/admin/logout', array(
        'as'   => 'log-out',
        'uses' => 'UserController@getLogOut'
    ));
});

Route::group( array( 'before' => 'guest' ), function () {
    Route::get('/admin/active/{code}', array(
        'as' => 'admin/active',
        'uses' => 'UserController@getActivate'
    ));

    Route::post('/admin/active/{code}', array(
        'as' => 'admin/post-active',
        'uses' => 'UserController@postActive'
    ));

    Route::get('/admin/login', array(
        'as' => 'admin-get-login',
        'uses' => 'UserController@getLogin'
    ));

    Route::post('/admin/login', array(
        'as' => 'admin-post-login',
        'uses' => 'UserController@postLogin'
    ));

});