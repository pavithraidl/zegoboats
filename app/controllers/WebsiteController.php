<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {13/12/16} {19:12}.
 */
 
class WebsiteController extends BaseController {

    public function getWebsite() {
        $access = new UserController();
        if($access ->systemAccessControl(2)) {
            return View::make('admin.website')->header('dealerId', Auth::user()->dealerid);
        }
        else {
            return View::make('admin.404');
        }
    }

    public function changeTextBlock() {
        $value = Input::get('value');
        $block = Input::get('block');

        try {
            $blockId = WebsiteTextBlocks::where('dealerid', Auth::user()->dealerid)->where('text_block', $block)->pluck('id');
            if($blockId) {
                WebsiteTextBlocks::where('id', $blockId)->update(array(
                    'content' => $value
                ));
            }
            else {
                $textBlock = new WebsiteTextBlocks();
                $textBlock -> content = $value;
                $textBlock -> text_block = $block;
                $textBlock -> dealerid = Auth::user()->dealerid;
                $textBlock -> save();
            }
            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeTextBlock', $ex);
            return 0;
        }
    }

    public function publishValue() {
        $value = Input::get('value');
        $table = Input::get('table');
        $field = Input::get('field');

        try {
            $id = DB::table($table)->where('dealerid', Auth::user()->dealerid)->pluck('id');
            if($id) {
                DB::table($table)->where('id', $id)->update(array(
                    $field => $value
                ));
            }
            else {
                DB::table($table)->insert(array(
                    $field => $value,
                    'dealerid' => Auth::user()->dealerid
                ));
            }


            $activity = new UserController();
            $activity -> saveActivity('Updated the '.$table);
            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeTextBlock', $ex);
            return 0;
        }
    }

    public function publishTestimonials() {
        $value = Input::get('value');
        $field = Input::get('field');
        $row = Input::get('row');

        try {
            $id = WebsiteTestimonials::where('dealerid', Auth::user()->dealerid)->where('row', $row)->pluck('id');
            if($id) {
                WebsiteTestimonials::where('id', $id)->update(array(
                    $field => $value
                ));
            }
            else {
                $testimonial = new WebsiteTestimonials();
                $testimonial -> dealerid = Auth::user()->dealerid;
                $testimonial -> row = $row;
                $testimonial -> status = 1;
                $testimonial -> created_at = \Carbon\Carbon::now('UTC');
                $testimonial -> save();
                $id = $testimonial -> id;

                WebsiteTestimonials::where('id', $id)->update(array(
                    $field => $value
                ));
            }

            $activity = new UserController();
            $activity -> saveActivity('Updated the Testimonial '.$row);
            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'publishTestimonials', $ex);
            return 0;
        }
    }

    public function changeMainImage() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;

        try {
            //get the directory name from the database
            $dirPath = "assets/images/main_slider/";
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "jpeg") {
                $fileType = 1;
            }

            if($fileType == 1 || $fileSize == 1){
                return json_encode("-1");
            }
            else{
                $name = sprintf("%s.jpg", Auth::user()->dealerid);
                Input::file('file')->move($dirPath,$name);

                //update image number

                return Auth::user()->dealerid;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeMainImage', $ex);

            return 0;
        }
    }

    public function removeSliderImage() {
        $id = Input::get('id');
        $type = Input::get('type');

        try {
            if($type == 1) {
                WebsiteImageSlider::where('id', $id)->where('dealerid', Auth::user()->dealerid)->update(array(
                    'status' => 0
                ));

                $activity = new UserController();
                $activity -> saveActivity('Removed an image from the home slider');
            }
            else if($type == 2) {
                WebsiteBottomLogo::where('id', $id)->where('dealerid', Auth::user()->dealerid)->update(array(
                    'status' => 0
                ));

                $activity = new UserController();
                $activity -> saveActivity('Removed an image from the bottom logo bar');
            }

            return 1;

        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'removeSliderImage', $ex);

            return 0;
        }
    }

    public function changeSliderImage() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;

        try {
            //get the directory name from the database
            $dirPath = "assets/images/home_slider/".Auth::user()->dealerid.'/';
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "jpeg") {
                $fileType = 1;
            }

            if($fileType == 1 || $fileSize == 1){
                return json_encode("-1");
            }
            else{
                $websiteImage = new WebsiteImageSlider();
                $websiteImage -> dealerid = Auth::user()->dealerid;
                $websiteImage -> alt = 'Zego Sports Boats Image';
                $websiteImage -> status = 1;
                $websiteImage -> save();
                $id = $websiteImage -> id;

                $name = sprintf("%s.jpg", $id);
                Input::file('file')->move($dirPath,$name);

                $src = $dirPath . $name;
                $thubmDir = $dirPath.'/thumb/';
                if (!file_exists($thubmDir)) {
                    mkdir($thubmDir, 0777);
                }

                $imageResize = new ImageProcessController();
                $imageResize->setImageSizes($src,$thubmDir, $name,100,75,60, 'jpg');

                return $id;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeSliderImage', $ex);

            return 0;
        }
    }

    public function changeLogoImage() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;

        try {
            //get the directory name from the database
            $dirPath = "assets/images/logos/".Auth::user()->dealerid.'/';
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "jpeg" && strtolower($imageFileType) != 'png') {
                $fileType = 1;
            }

            if($fileType == 1 || $fileSize == 1){
                return json_encode("-1");
            }
            else{
                $websiteLogo = new WebsiteBottomLogo();
                $websiteLogo -> dealerid = Auth::user()->dealerid;
                $websiteLogo -> alt = 'Zego Sports Boats support company logos';
                $websiteLogo -> status = 1;
                $websiteLogo -> save();
                $id = $websiteLogo -> id;

                $name = sprintf("%s.jpg", $id);
                Input::file('file')->move($dirPath,$name);

                return $id;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeLogoImage', $ex);

            return 0;
        }
    }

    public function changeVisibility() {
        $field = Input::get('field');
        $status = Input::get('status');

        try {
            $id = WebsiteVisibility::where('dealerid', Auth::user()->dealerid)->where('status', 1)->pluck('id');
            if($id) {
                WebsiteVisibility::where('id', $id)->update(array(
                    $field => $status
                ));
            }
            else {
                $visibility = new WebsiteVisibility();
                $visibility -> dealerid = Auth::user()->dealerid;
                $visibility -> action_bar = 0;
                $visibility -> team = 0;
                $visibility -> testimonials = 0;
                $visibility -> logo_bar = 0;
                $visibility -> status = 1;
                $visibility ->created_at = \Carbon\Carbon::now('UTC');
                $visibility -> save();
                $id = $visibility -> id;

                WebsiteVisibility::where('id', $id)->update(array(
                    $field => $status
                ));
            }

            return 1;
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeVisibility', $ex);

            return 0;
        }
    }

    public function changeTeamVisibility() {
        $id = Input::get('id');
        $status = Input::get('status');

        try {
            UserInfo::where('id', User::where('id', $id)->pluck('infoid'))->update(array(
                'visible' => $status
            ));

            return 1;

        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeTeamVisibility', $ex);

            return 0;
        }
    }

    public function changeProfileImage() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $userId = Input::get('userid');
        $fileSize = 0;
        $fileType = 0;

        try {
            //get the directory name from the database
            $dirPath = "assets/images/team/";
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "jpeg") {
                $fileType = 1;
            }

            if($fileType == 1 || $fileSize == 1){
                return json_encode("-1");
            }
            else{
                $name = sprintf("%s.jpg", $userId);
                Input::file('file')->move($dirPath,$name);

                //update image number



                $src = $dirPath . $name;
                $userAvatarDir = sprintf("assets/admin/img/users/%s/", $userId);
                if (!file_exists($userAvatarDir)) {
                    mkdir($userAvatarDir, 0777);
                }

                $imageResize = new ImageProcessController();
                $imageResize->setImageSizes($src,$userAvatarDir, 'user-35',35,35,60, 'jpg');
                $imageResize->setImageSizes($src, $userAvatarDir, 'user-100', 100, 100, 60, 'jpg');

                return $userId;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'changeProfileImage', $ex);

            return 0;
        }
    }

    public function publishFeatures() {
        $value = Input::get('value');
        $field = Input::get('field');
        $column = Input::get('column');
        $dealerId = Auth::user()->dealerid;

        try {
            $id = WebsiteFeatures::where('dealerid', Auth::user()->dealerid)->where('column', $column)->pluck('id');
            if($id) {
                WebsiteFeatures::where('id', $id)->update(array(
                    $field => $value
                ));
            }
            else {
                DB::table('website_features')->insert(array(
                    $field => $value,
                    'dealerid' => $dealerId,
                    'column' => $column
                ));
            }


            $activity = new UserController();
            $activity -> saveActivity('Updated the website features fields');

            return 1;

        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'publishFeatures', $ex);

            return 0;
        }
    }

    public function publishTeamInfo() {
        $value = Input::get('value');
        $field = Input::get('field');
        $userId = Input::get('userid');

        try {
            UserInfo::where('id', User::where('id', $userId)->pluck('infoid'))->update(array(
                    $field => $value
                ));

            return 1;
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'publishTeamInfo', $ex);

            return 0;
        }
    }

    public function publishImageDetails() {
        $value = Input::get('value');
        $field = Input::get('field');
        $imageId = Input::get('imageid');

        try {
            WebsiteImageSlider::where('id', $imageId)->update(array(
                $field => $value
            ));

            return 1;
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('WebsiteController', 'publishImageDetails', $ex);

            return 0;
        }
    }
}