<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getHome()
	{
        $domain = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $dealerId = null;
        $test = '';

        $dealerUrlList = Dealer::where('status', 1)->lists('url');
        foreach ($dealerUrlList as $url) {
            $urlCheck = str_replace("http://","",$url);
            $urlCheck = str_replace("www", "", $urlCheck);
            $urlCheck = preg_replace('/[^\p{L}\p{N}\s]/u', '', $urlCheck);
            $domain = preg_replace('/[^\p{L}\p{N}\s]/u', '', $domain);
            $domain = str_replace("http://", "", $domain);
            $domain = str_replace("www", "", $domain);

            if($urlCheck == $domain) {
                $dealerId = Dealer::where('url', $url)->where('status', 1)->pluck('id');
                break;
            }
        }

        if($dealerId != null) {
            return View::make('frontend.default', array(
                'dealerId' => $dealerId,
                'page' => 'home'
            ));
        }
		else {

            return View::make('frontend.404', array(
                'dealerId' => 14,
                'page' => '404'
            ));
        }
	}

	public function getDealers() {
        $domain = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $dealerId = null;

        $dealerUrlList = Dealer::where('status', 1)->lists('url');
        foreach ($dealerUrlList as $url) {
            $urlCheck = str_replace("http://","",$url);
            $urlCheck = str_replace('www', '', $urlCheck);
            $urlCheck = preg_replace('/[^\p{L}\p{N}\s]/u', '', $urlCheck);
            $domain = preg_replace('/[^\p{L}\p{N}\s]/u', '', $domain);
            $domain = str_replace("http://", "", $domain);
            $domain = str_replace("www", "", $domain);

            if($urlCheck == $domain) {
                $dealerId = Dealer::where('url', $url)->where('status', 1)->pluck('id');
                break;
            }
        }

        if($dealerId != null) {
            return View::make('frontend.dealers', array(
                'dealerId' => $dealerId,
                'page' => 'home'
            ));
        }
        else {
            return View::make('frontend.dealers', array(
                'dealerId' => 14,
                'page' => 'dealers'
            ));
        }
    }

    public function getContact() {
        $domain = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $dealerId = null;

        $dealerUrlList = Dealer::where('status', 1)->lists('url');
        foreach ($dealerUrlList as $url) {
            $urlCheck = str_replace("http://","",$url);
            $urlCheck = str_replace('www', '', $urlCheck);
            $urlCheck = preg_replace('/[^\p{L}\p{N}\s]/u', '', $urlCheck);
            $domain = preg_replace('/[^\p{L}\p{N}\s]/u', '', $domain);
            $domain = str_replace("http://", "", $domain);
            $domain = str_replace("www", "", $domain);

            if($urlCheck == $domain) {
                $dealerId = Dealer::where('url', $url)->where('status', 1)->pluck('id');
                break;
            }
        }

        if($dealerId != null) {
            return View::make('frontend.contact', array(
                'dealerId' => $dealerId,
                'page' => 'home'
            ));
        }
        else {
            return View::make('frontend.contact', array(
                'dealerId' => 14,
                'page' => 'contact'
            ));
        }
    }

    public function sendEmail() {
        $name = Input::get('name');
        $email = Input::get('email');
        $phone = Input::get('phone');
        $subject = Input::get('subject');
        $msg = Input::get('message');
        $dealerId = Input::get('dealerid');

        try {
            $dealerEmail = WebsiteFooter::where('dealerid', $dealerId)->pluck('email');
            if($email == '' || $email == null) {
                $email = Dealer::where('id', $dealerId)->pluck('email');
            }
            $businessPark = Dealer::where('id', $dealerId)->pluck('business_park');
            $street = Dealer::where('id', $dealerId)->pluck('street');
            $town = Dealer::where('id', $dealerId)->pluck('town');
            $zip = Dealer::where('id', $dealerId)->pluck('zip');
            $state = Dealer::where('id', $dealerId)->pluck('state');
            $country = Dealer::where('id', $dealerId)->pluck('country');

            $businessPark = ($businessPark != '' ? $businessPark.'<br/>': '');
            $street = ($street != '' ? $street.', ': '');
            $town = ($town != '' ? $town.'<br/>': '');
            $zip = ($zip != '' ? $zip.'<br/>': '');
            $country = ($country != '' ? $country.'<br/>': '');
            $address = '<br/>'.$businessPark.$street.$town.$state.' '.$zip.$country;

            $response = WebsiteFooter::where('dealerid', $dealerId)->pluck('response');
            $homeUrl = URL::To('/');

            Mail::send( 'admin.email.contact-enquiry', array(
                'name'      => $name,
                'email'  => $email,
                'phone' => $phone,
                'subject' => $subject,
                'msg' => $msg,
                'address' => $address,
                'homeurl' => $homeUrl,
            ), function ( $message ) use ( $dealerEmail) {
                $message->to( $dealerEmail, 'Zego Team' )->subject( 'User Enquiry' );
            });

            Mail::send( 'admin.email.contact-user-respond', array(
                'name'      => $name,
                'address' => $address,
                'response' => $response,
                'homeurl' => $homeUrl,
            ), function ( $message ) use ( $email, $name) {
                $message->to( $email, $name )->subject( 'Your message received' );
            });

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('HomeController', 'sendEmail', $ex);
        }
    }

    public function getIcons() {
        return View::make('admin.features-icons');
    }

}
