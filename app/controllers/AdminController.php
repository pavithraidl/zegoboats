<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {10/12/16} {09:43}.
 */
 
class AdminController extends BaseController {

    public function getAdmin() {
        return View::make('admin.website');
    }
}