<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {15/12/16} {14:54}.
 */
 
class ErrorController extends BaseController {

    public function getException() {
        $access = new UserController();
        if($access ->systemAccessControl(6)) {
            return View::make('admin.exception');
        }
        else {
            return View::make('admin.404');
        }
    }

    public function catchException($controller, $func, $ex) {
        try {
            $exception = new SystemException();
            $exception -> userid = null;
            $exception -> controller = $controller;
            $exception -> func = $func;
            $exception -> msg = $ex;
            $exception -> status = 2;
            $exception -> created_at = \Carbon\Carbon::now('UTC');;
            $exception -> save();

            return 1;
        }catch (Exception $ex) {
            Log::error($ex);

            return 0;
        }
    }

    public function getExceptionList() {
        $timezone = Input::get('timezone');
        $paginate = Input::get('paginate');
        try {
            $ret = null;
            $exceptionList = SystemException::where('status', '!=', 0)->orderBy('status', 'desc')->orderBy('created_at', 'desc')->forPage($paginate, 10)->lists('id');
            $timeSince = new CalculationController();

            foreach ($exceptionList as $exceptionId) {
//                $exception = SystemException::where('id', $exceptionId);
                $controller = SystemException::where('id', $exceptionId)->pluck('controller');
                $func = SystemException::where('id', $exceptionId)->pluck('func');
                $exeMsg = SystemException::where('id', $exceptionId)->pluck('msg');
                $status = SystemException::where('id', $exceptionId)->pluck('status');
                $createdAt = SystemException::where('id', $exceptionId)->pluck('created_at');

                //calculation
                $createdAtDT = (new DateTime($createdAt->setTimezone(new DateTimeZone($timezone))))->format('H:i:s d-m-Y');
                $createdAt = ($timeSince->timeAgo((new DateTime($createdAt->setTimezone(new DateTimeZone('UTC'))))->format('Y-m-d H:i:s')));

                $ret[] = array(
                    'id' => $exceptionId,
                    'controller' => $controller,
                    'func' => $func,
                    'exe_msg' => substr($exeMsg, 0, 400),
                    'status' => $status,
                    'created_at' => $createdAt,
                    'created_at_dt' => $createdAtDT
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $this->catchException('ErrorController', 'getExceptionList', $ex);
            return 0;
        }
    }

    public function postExceptionStatus() {
        $exceptionId = Input::get('exception_id');
        try {
            SystemException::where('id', $exceptionId)->update(array(
                'status' => 1
            ));

            $activity = new UserController();
            $activity -> saveActivity('Solve the exception under exception id '.$exceptionId);

            return 1;
        } catch (Exception $ex) {
            $this->saveExceptionDetails('ErrorController', 'postExceptionStatus', $ex);
            return 0;
        }
    }
}