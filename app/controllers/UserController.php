<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {10/12/16} {10:57}.
 */
 
class UserController extends BaseController {

    public function getLogin() {
        return View::make('admin.login');
    }

    public function getUsers() {
        $access = new UserController();
        if($access ->systemAccessControl(4)) {
            return View::make('admin.users');
        }
        else {
            return View::make('admin.404');
        }

    }

    public function getActivate($code) {
        try {
            if (User::where('code', '=', $code)->where('active', '=', 0)->pluck('id')) {
                $user = User::where('code', $code)->first();
                $name = UserInfo::where('id', $user->infoid)->pluck('name');

                return View::make('admin.activate', array(
                    'code' => $code,
                    'email' => $user->email,
                    'name' => $name
                ));
            } else {
                return View::make('error._404');
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'getActivate', $ex);
        }
    }

    public function postActive($code) {
        $password = Input::get('password');
        $email = Input::get('email');
        $name = Input::get('name');
        $phone = Input::get('phone');
        $code = Input::get('code');

        $userId = User::where('code', '=', $code)->where('active', 0)->where('email', $email)->pluck('id');

        if($userId) {
            User::where('id', $userId)->update(array(
                'active' => 1,
                'code' => '',
                'status' => 1,
                'password' => Hash::make($password)
            ));

            $roll = User::where('id', $userId)->pluck('roll');
            $dealerId = User::where('id', $userId)->pluck('dealerid');

            if($roll == 3) {
                Dealer::where('id', $dealerId)->update(array(
                    'name' => $name,
                    'phone' => $phone,
                    'status' => 1
                ));
            }

            $infoId = User::where('id', $userId)->pluck('infoid');

            UserInfo::where('id', $infoId)->update(array(
                'name' => $name,
                'phone' => $phone
            ));

            $this->saveActivity('Activated the account');

            return 1;
        }
        else {
            return -1;
        }
    }

    public function postLogin() {
        try{
            //validate the login details
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|email',
                'password' => 'required'
            ));

            if ($validator->fails()) {
                // Redirect to the sign in page with errors
                return Redirect::route('admin-get-login')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //remember user
                $remember = (Input::has('chk-remember')) ? true : false;

                // Sign in user to the dash board
                $email = Input::get('email');
                $password = Input::get('password');
                $previous = Input::get('previous');

                $auth = Auth::attempt(array(
                    'email' => $email,
                    'password' => $password,
                    'active' => 1
                ), $remember);

                if ($auth) {
                    if(User::where('email', $email)->pluck('active') == 0){
                        return Redirect::route('login')
                            ->with('global', 'This account is not Activated yet. Check your emails to active!');
                    }
                    else{
                        $this->saveActivity('Login to the system');
                        return View::make('admin.website');
                    }
                }
                else {
                    $this->saveActivity('Trying to login with wrong user details or deactivated account');
                    return Redirect::route('admin-get-login')
                        ->with('global', 'Email/password wrong, or account not activated!');
                }
            }
        }catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->catchException( 'AccountController', 'postLogin', $ex );

            return 0;
        }
    }

    public function getLogOut()
    {
        Auth::logout();
        $this->saveActivity('Logout from the system');
        return Redirect::route('admin-get-login');
    }

    public function saveActivity($activity) {
        try {
            $record = new ActivityLog();
            $record -> userid = Auth::user()->id;
            $record -> dealerid = Auth::user()->dealerid;
            $record -> activity = $activity;
            $record -> created_at = \Carbon\Carbon::now('UTC');
            $record -> save();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'saveActivity', $ex);
        }
    }

    public function checkDuplicateEmail() {
        $email = Input::get('email');

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id')) {
                return -1;
            }
            else {
                return 1;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'checkDuplicateEmail', $ex);
        }
    }

    public function systemAccessControl($systemId) {
        try {
            if(SystemRoutingAccess::where('systemid', $systemId)
                    ->where('userid', Auth::user()->id)
                    ->where('allow', 1)->pluck('id') ||
                System::where('id', $systemId)
                    ->where('allow_default', 1)->pluck('id') ||
                Auth::user()->roll < 3) {
                return true;
            }
            else {
                return false;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'systemAccessControl', $ex);
        }

    }

    public function urlAccessControl() {
        try {

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'urlAccessControl', $ex);
            Auth::logout();
            return View::make('admin.login');
        }
    }


    //Manage User control view responses
    //----------------------------------------------------------------------------------
    public function addUser() {
        $name = Input::get('name');
        $email = Input::get('email');
        $jobTitle = Input::get('jobtitle');
        $dealerId = Auth::user()->dealerid;

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {
                $code = str_random(60);

                $userInfo = new UserInfo();
                $userInfo -> name = $name;
                $userInfo -> jobtitle = $jobTitle;
                $userInfo -> save();
                $userInfoId = $userInfo -> id;

                $identity = Dealer::where('id', $dealerId)->pluck('identity');

                $user = new User();
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> roll = 4;
                $user -> dealerid = $dealerId;
                $user -> infoid = $userInfoId;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();

                $this->saveActivity('Created an account for '.$name.'('.$email.') to the system');

                if($email != '') {
                    Mail::send( 'admin.email.welcome-dealer', array(
                        'link'      => URL::route( 'admin/active', $code ),
                        'branch'  => $identity,
                        'name' => $name,
                        'homeurl' => URL::To('/')
                    ), function ( $message ) use ( $user, $email, $name) {
                        $message->to( $email, $name )->subject( 'Activate your account' );
                    });
                }

                return 1;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'addUser', $ex);
            return 0;
        }
    }

    public function getUserList() {
        try {
            $dealerId = Auth::user()->dealerid;
            $ownUserId = Auth::user()->id;

            $userList = User::where('status', '!=', 0)->where('dealerid', $dealerId)->lists('id');

            $key = array_search($ownUserId, $userList);
            unset($userList[$key]);
            array_unshift($userList, $ownUserId);

            foreach ($userList as $userId) {
                $infoId = User::where('id', $userId)->pluck('infoid');
                $name = UserInfo::where('id', $infoId)->pluck('name');
                $status = User::where('id', $userId)->pluck('status');
                $jobTitle = UserInfo::where('id', $infoId)->pluck('jobtitle');

                $profilePic = URL::To('/').'/assets/images/team/'.$userId.'.jpg';
                try {
                    getimagesize($profilePic);
                }
                catch(Exception $ex) {
                    $profilePic = URL::To('/').'/assets/admin/img/users/m/user-35.jpg';
                }

                $arr[] = array(
                    'id' => $userId,
                    'name' => $name,
                    'status' => $status,
                    'jobtitle' => $jobTitle,
                    'avatar' => $profilePic
                );
            }

            return json_encode($arr);
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UserController', 'getUsersList', $ex);
            return 0;
        }
    }

    public function getUserBasicDetails() {
        $userId = Input::get('userid');

        try {
            $infoId = User::where('id', $userId)->pluck('infoid');
            $name = UserInfo::where('id', $infoId)->pluck('name');
            $phone = UserInfo::where('id', $infoId)->pluck('phone');
            $email = User::where('id', $userId)->pluck('email');
            $status = User::where('id', $userId)->pluck('status');
            $jobTitle = UserInfo::where('id', $infoId)->pluck('jobtitle');

            $profilePic = URL::To('/').'/assets/images/team/'.$userId.'.jpg';
            try {
                getimagesize($profilePic);
            }
            catch(Exception $ex) {
                $profilePic = URL::To('/').'/assets/admin/img/users/m/user-35.jpg';
            }

            $arr = array(
                'id' => $userId,
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'status' => $status,
                'jobtitle' => $jobTitle,
                'avatar' => $profilePic
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'getUserBasicDetails', $ex);
            return 0;
        }
    }

    public function postUserStatus() {
        $userId = Input::get('userid');
        $status = Input::get('status');

        try {
            if($status == 0) $status = 2;
            User::where('id', $userId)->update(array(
                'status' => $status
            ));

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.UserInfo::where('id', User::where('id', $userId)->pluck('infoid'))->pluck('name').'('.User::where('id', $userId)->pluck('email').') User account');

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'postUserStatus', $ex);
            return 0;
        }
    }

    public function postRemoveUserAccount() {
        $userId = Input::get('userid');

        try {
            User::where('id', $userId)->update(array(
                'status' => 0,
                'active' => 0,
                'email' => ''
            ));

            $this -> saveActivity('Deleted - '.UserInfo::where('id', Auth::user()->infoid)->pluck('name').' User account');

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'postRemoveUserAccount', $ex);
            return 0;
        }
    }

    public function postResendActivationEmail() {
        $userId = Input::get('userid');

        try {
            $code = User::where('id', $userId)->pluck('code');
            $identity = Dealer::where('id', Auth::user()->dealerid)->pluck('identity');
            $infoId = User::where('id', $userId)->pluck('infoid');
            $name = UserInfo::where('id', $infoId)->pluck('name');
            $email = User::where('id', $userId)->pluck('email');

            Mail::send( 'admin.email.welcome-user', array(
                'link'      => URL::route( 'admin/active', $code ),
                'branch'  => $identity,
                'name' => $name,
            ), function ( $message ) use ( $email, $name) {
                $message->to( $email, $name )->subject( 'Activate your account' );
            });

            $this->saveActivity('Resent the activation email to '.$name.'('.$email.')');

            return json_encode($email);
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'postResendActivationEmail', $ex);
            return 0;
        }
    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $userId = Input::get('userid');
        $arr = [];

        try {
            $activityList = DB::table('activity_log')->where('userid', $userId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                    $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                    //calculations
                    $setCreatedAt = ($timeScence->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'getActivityList', $ex);
            return 0;
        }
    }

    public function changeUserAccess() {
        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $systemId = Input::get('systemid');

        try {
            if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->pluck('id')) {
                SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemRoutingAccess();
                $systemAccess -> systemid = $systemId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.UserInfo::where('id', User::where('id', $userId)->pluck('infoid'))->pluck('name').'('.User::where('id', $userId)->pluck('email').') access to '.System::where('id', $systemId)->pluck('name'));

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'changeUserAccess', $ex);
            return 0;
        }
    }

    public function changeUserOperationAccess() {
        $userId = Input::get('selecteduser');
        $status = Input::get('status');
        $operationId = Input::get('operationid');

        try {
            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $this -> saveActivity($status.' user - '.UserInfo::where('id', User::where('id', $userId)->pluck('infoid'))->pluck('name').'('.User::where('id', $userId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('operation'));

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'changeUserAccess', $ex);
            return 0;
        }
    }

    public function getAccessList() {
        $userId = Input::get('userid');

        try {
            $systemList = SystemRoutingAccess::where('userid', $userId)->where('allow', 1)->lists('systemid');
            $operationList = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->lists('operationid');

            $ret = array(
                'sys' => $systemList,
                'ope' => $operationList
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'getAccessList', $ex);
            return 0;
        }
    }

    public function saveEmail() {
        $email = Input::get('email');
        $userId = Input::get('userid');

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {
                User::where('id', $userId)->update(array('email' => $email));

                return 1;
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('UsersController', 'saveEmail', $ex);
            return 0;
        }
    }
}