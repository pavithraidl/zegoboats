<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {13/12/16} {09:50}.
 */
 
class DealersController extends BaseController {

    public function getDealers() {
        $access = new UserController();
        if($access ->systemAccessControl(3)) {
            return View::make('admin.dealers');
        }
        else {
            return View::make('admin.404');
        }
    }

    public function addDealer() {
        $identity = Input::get('identity');
        $name = Input::get('name');
        $email = Input::get('email');
        $url = Input::get('url');

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {
                $code = str_random(60);

                $dealer = new Dealer();
                $dealer -> identity = $identity;
                $dealer -> name = $name;
                $dealer -> email = $email;
                $dealer -> url = $url;
                $dealer -> mother = Auth::user()->dealerid;
                $dealer -> status = 3;
                $dealer -> visible = 1;
                $dealer -> save();
                $dealerId = $dealer -> id;

                $userInfo = new UserInfo();
                $userInfo -> name = $name;
                $userInfo -> save();
                $userInfoId = $userInfo -> id;

                $user = new User();
                $user -> email = $email;
                $user -> code = $code;
                $user -> active = 0;
                $user -> roll = 3;
                $user -> dealerid = $dealerId;
                $user -> infoid = $userInfoId;
                $user -> status = 3;
                $user -> created_at = \Carbon\Carbon::now('UTC');
                $user -> save();

                if($email != '') {
                    Mail::send( 'admin.email.welcome-dealer', array(
                        'link'      => URL::route( 'admin/active', $code ),
                        'branch'  => $identity,
                        'name' => $name,
                        'homeurl' => URL::To('/')
                    ), function ( $message ) use ( $user, $email, $name) {
                        $message->to( $email, $name )->subject( 'Activate your account' );
                    });
                }

                $activity = new UserController();
                $activity -> saveActivity('Added a new dealer '.$identity.'('.$email.') to the system');
                return 1;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'addDealer', $ex);
            return 0;
        }
    }

    public function getDealersList() {
        try {
            $ownDealerId = Auth::user()->dealerid;

            if(Auth::user()->roll < 3) {
                $dealersList = Dealer::where('status', '!=', 0)->lists('id');
            }
            else if(Auth::user()->roll == 3) {
                $dealersList = Dealer::where('status', '!=', 0)->where('mother', $ownDealerId)->lists('id');
            }

            $key = array_search($ownDealerId, $dealersList);
            unset($dealersList[$key]);
            array_unshift($dealersList, $ownDealerId);

            foreach ($dealersList as $dealerId) {
                $identity = Dealer::where('id', $dealerId)->pluck('identity');
                $name = Dealer::where('id', $dealerId)->pluck('name');
                $status = Dealer::where('id', $dealerId)->pluck('status');

                $arr[] = array(
                    'id' => $dealerId,
                    'identity' => $identity,
                    'name' => $name,
                    'status' => $status
                );
            }

            return json_encode($arr);
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'getDealersList', $ex);
            return 0;
        }
    }

    public function getDealerBasicDetails() {
        $dealerId = Input::get('dealerid');

        try {
            $identity = Dealer::where('id', $dealerId)->pluck('identity');
            $url = Dealer::where('id', $dealerId)->pluck('url');
            $name = Dealer::where('id', $dealerId)->pluck('name');
            $phone = Dealer::where('id', $dealerId)->pluck('phone');
            $email = Dealer::where('id', $dealerId)->pluck('email');
            $status = Dealer::where('id', $dealerId)->pluck('status');
            $business = Dealer::where('id', $dealerId)->pluck('business_park');
            $street = Dealer::where('id', $dealerId)->pluck('street');
            $town = Dealer::where('id', $dealerId)->pluck('town');
            $zip = Dealer::where('id', $dealerId)->pluck('zip');
            $state = Dealer::where('id', $dealerId)->pluck('state');
            $country = Dealer::where('id', $dealerId)->pluck('country');
            $mapLink = Dealer::where('id', $dealerId)->pluck('map_link');
            $visibility = Dealer::where('id', $dealerId)->pluck('visible');
            $order = Dealer::where('id', $dealerId)->pluck('order');

            $arr = array(
                'id' => $dealerId,
                'url' => $url,
                'identity' => $identity,
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'status' => $status,
                'businesspark' => $business,
                'street' => $street,
                'town' => $town,
                'zip' => $zip,
                'state' => $state,
                'country' => $country,
                'maplink' => $mapLink,
                'visibility' => $visibility,
                'order' => $order
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'getDealerBasicDetails', $ex);
            return 0;
        }
    }

    public function postDealerStatus() {
        $dealerId = Input::get('dealerid');
        $status = Input::get('status');

        try {
            if($status == 0) $status = 2;
            Dealer::where('id', $dealerId)->update(array(
                'status' => $status
            ));

            $activity = new UserController();
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity -> saveActivity($status.' dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').')');
            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'postDealerStatus', $ex);
            return 0;
        }
    }

    public function postRemoveDealerAccount() {
        $dealerId = Input::get('dealerid');

        try {
            Dealer::where('id', $dealerId)->update(array(
                'status' => 0
            ));

            User::where('dealerid', $dealerId)->update(array(
                'active' => 0,
                'status' => 0,
                'email' => ''
            ));

            $activity = new UserController();
            $activity -> saveActivity('Removed dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').')');
            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'postRemoveDealerAccount', $ex);
            return 0;
        }
    }

    public function postResendActivationEmail() {
        $dealerId = Input::get('dealerid');

        try {
            $code = User::where('dealerid', $dealerId)->pluck('code');
            $identity = Dealer::where('id', $dealerId)->pluck('identity');
            $infoId = User::where('dealerid', $dealerId)->pluck('infoid');
            $name = UserInfo::where('id', $infoId)->pluck('name');
            $email = Dealer::where('id', $dealerId)->pluck('email');

            Mail::send( 'admin.email.welcome-dealer', array(
                'link'      => URL::route( 'admin/active', $code ),
                'branch'  => $identity,
                'name' => $name,
            ), function ( $message ) use ( $email, $name) {
                $message->to( $email, $name )->subject( 'Activate your account' );
            });

            $activity = new UserController();
            $activity -> saveActivity('Resent the activation email to dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').')');

            return json_encode($email);
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'postResendActivationEmail', $ex);
            return 0;
        }
    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $dealerId = Input::get('dealerid');
        $arr = [];

        try {
            $activityList = DB::table('activity_log')->where('dealerid', $dealerId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeScence = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = DB::table('activity_log')->where('id', $activityId)->pluck('activity');
                    $createdAt = DB::table('activity_log')->where('id', $activityId)->pluck('created_at');

                    //calculations
                    $setCreatedAt = ($timeScence->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'getActivityList', $ex);
            return 0;
        }
    }

    public function changeDealerAccess() {
        $dealerId = Input::get('selecteddealer');
        $status = Input::get('status');
        $systemId = Input::get('systemid');

        try {
            $userId = User::where('dealerid', $dealerId)->pluck('id');
            if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->pluck('id')) {
                SystemRoutingAccess::where('systemid', $systemId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemRoutingAccess();
                $systemAccess -> systemid = $systemId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $activity = new UserController();
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity -> saveActivity($status.' dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').') access to '.System::where('id', $systemId)->pluck('name'));

            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'changeDealerAccess', $ex);
            return 0;
        }
    }

    public function changeDealerOperationAccess() {
        $dealerId = Input::get('selecteddealer');
        $status = Input::get('status');
        $operationId = Input::get('operationid');

        try {
            $userId = User::where('dealerid', $dealerId)->pluck('id');
            if(SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->pluck('id')) {
                SystemOperationAccess::where('operationid', $operationId)->where('userid', $userId)->update(array(
                    'allow' => $status
                ));
            }
            else {
                $systemAccess = new SystemOperationAccess();
                $systemAccess -> operationid = $operationId;
                $systemAccess -> userid = $userId;
                $systemAccess -> allow = $status;
                $systemAccess -> created_at = \Carbon\Carbon::now('UTC');
                $systemAccess -> save();
            }

            $systemId = SystemOperation::where('id', $operationId)->pluck('systemid');
            $activity = new UserController();
            $status = ($status == 1 ? 'Enabled' : 'Disabled');
            $activity -> saveActivity($status.' dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').') access in the '.System::where('id', $systemId)->pluck('name').' Page to '.SystemOperation::where('id', $operationId)->pluck('operation'));
            return 1;

        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'changeDealerAccess', $ex);
            return 0;
        }
    }

    public function getAccessList() {
        $dealerId = Input::get('dealerid');

        try {
            $userId = User::where('dealerid', $dealerId)->pluck('id');
            $systemList = SystemRoutingAccess::where('userid', $userId)->where('allow', 1)->lists('systemid');
            $operationList = SystemOperationAccess::where('userid', $userId)->where('allow', 1)->lists('operationid');

            $ret = array(
                'sys' => $systemList,
                'ope' => $operationList
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'getAccessList', $ex);
            return 0;
        }
    }

    public function saveSettings() {
        $dealerId = Input::get('dealerid');
        $value = Input::get('value');
        $type = Input::get('type');

        try {
            Dealer::where('id', $dealerId)->update(array(
                $type => $value
            ));

            $activity = new UserController();
            $activity -> saveActivity('change dealer - '.Dealer::where('id', $dealerId)->pluck('identity').'('.Dealer::where('id', $dealerId)->pluck('email').') '.$type);
            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'saveSettings', $ex);
            return 0;
        }
    }

    public function saveEmail() {
        $email = Input::get('email');
        $dealerId = Input::get('dealerid');

        try {
            if(User::where('email', $email)->where('status', '!=', 0)->pluck('id') && $email != '') {
                return -1;
            }
            else {
                Dealer::where('id', $dealerId)->update(array('email' => $email));
                User::where('dealerid', $dealerId)->update(array('email' => $email));

                return 1;
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'saveEmail', $ex);
            return 0;
        }
    }

    public function changeDealerVisibility() {
        $dealerId = Input::get('dealerid');
        $status = Input::get('status');

        try {
            Dealer::where('id', $dealerId)->update(array('visible' => $status));

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('DealersController', 'changeDealerVisibility', $ex);
            return 0;
        }
    }
}