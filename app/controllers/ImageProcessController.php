<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {29/12/16} {10:31}.
 */

class ImageProcessController  extends BaseController{

    public function setImageSizes( $src, $dirPath, $saveAs, $width, $height, $quality, $format ) {
        try {
            $img = imagecreatefromjpeg( $src );

            //get this values from user by submitting form ( either by crop or by textboxes)
            list( $old_width, $old_height, $type, $attr ) = getimagesize( $src );

            $dest = ImageCreateTrueColor( $width, $height );
            imagecopyresampled(
                $dest, //destination image
                $img, //source image
                0, //top left coordinate of the destination image in x direction
                0, //top left coordinate of the destination image in y direction
                0, //top left coordinate in x direction of source image that I want copying to start at
                0, //top left coordinate in y direction of source image that I want copying to start at
                $width, //190, thumbnail width
                $height, //190, thumbnail height
                $old_width, //how wide the rectangle from the source image we want thumbnailed is
                $old_height //how high the rectangle from the source image we want thumbnailed is
            );
            imagejpeg( $dest, $dirPath . $saveAs, $quality );

            return 1;
        } catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->catchException( "ImageProcessingController", "setImageSizes", $ex );

            return 0;
        }
    }

    public function cropImages() {

        $newNamePrefix = time() . '_';
        $manipulator   = new ImageManipulator( $_FILES['fileToUpload']['tmp_name'] );
        $width         = $manipulator->getWidth();
        $height        = $manipulator->getHeight();
        $centreX       = round( $width / 2 );
        $centreY       = round( $height / 2 );
        // our dimensions will be 200x130
        $x1 = $centreX - 100; // 200 / 2
        $y1 = $centreY - 65; // 130 / 2

        $x2 = $centreX + 100; // 200 / 2
        $y2 = $centreY + 65; // 130 / 2

        // center cropping to 200x130
        $newImage = $manipulator->crop( $x1, $y1, $x2, $y2 );
        // saving file to uploads folder
        $manipulator->save( 'uploads/' . $newNamePrefix . $_FILES['fileToUpload']['name'] );
        echo 'Done ...';
    }
}