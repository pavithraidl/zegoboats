<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - {13/12/16} {18:12}.
 */
 
class ProfileController extends BaseController {

    public function getMyProfile() {
        $access = new UserController();
        if($access ->systemAccessControl(5)) {
            return View::make('admin.my-profile');
        }
        else {
            return View::make('admin.404');
        }
    }

    public function changeInfo() {
        $type = Input::get('type');
        $value = Input::get('value');

        try {
            UserInfo::where('id', Auth::user()->infoid)->update(array(
                $type => $value
            ));

            $activity = new UserController();
            $activity->saveActivity('Changed the '.$type.' field.');

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> catchException('ProfileController', 'changeInfo', $ex);
            return 0;
        }
    }

    public function changeImage() {
        $target_file = $_FILES["file"]["name"];
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $fileSize = 0;
        $fileType = 0;

        try {
            //get the directory name from the database
            $dirPath = "assets/images/team/";
            //create the directory named by user id
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0777);
            }

            if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "jpeg") {
                $fileType = 1;
            }

            if($fileType == 1 || $fileSize == 1){
                return json_encode("-1");
            }
            else{
                $name = sprintf("%s.jpg", Auth::user()->id);
                Input::file('file')->move($dirPath,$name);

                //update image number



                $src = $dirPath . $name;
                $userAvatarDir = sprintf("assets/admin/img/users/%s/", Auth::user()->id);
                if (!file_exists($userAvatarDir)) {
                    mkdir($userAvatarDir, 0777);
                }

                $imageResize = new ImageProcessController();
                $imageResize->setImageSizes($src,$userAvatarDir, 'user-35',35,35,60, 'jpg');
                $imageResize->setImageSizes($src, $userAvatarDir, 'user-100', 100, 100, 60, 'jpg');

                return Auth::user()->id;
            }
        } catch ( Exception $ex ) {
            $exception = new ErrorController();
            $exception -> catchException('ProfileController', 'changeMainImage', $ex);

            return 0;
        }
    }
}