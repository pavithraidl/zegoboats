<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 31/12/16 15:54.
 */
?>

@extends('frontend.master')

@section('content')
    <div class="container clearfix">

    <?php
    $facebook = WebsiteFooter::where('dealerid', $dealerId)->pluck('facebook');
    $twitter = WebsiteFooter::where('dealerid', $dealerId)->pluck('twitter');
    $googleplus = WebsiteFooter::where('dealerid', $dealerId)->pluck('googleplus');
    $instagram = WebsiteFooter::where('dealerid', $dealerId)->pluck('instagram');

    $name = Dealer::where('id', $dealerId)->pluck('name');
    $businessPark = Dealer::where('id', $dealerId)->pluck('business_park');
    $street = Dealer::where('id', $dealerId)->pluck('street');
    $town = Dealer::where('id', $dealerId)->pluck('town');
    $zip = Dealer::where('id', $dealerId)->pluck('zip');
    $state = Dealer::where('id', $dealerId)->pluck('state');
    $country = Dealer::where('id', $dealerId)->pluck('country');

    $businessPark = ($businessPark != '' ? $businessPark.'<br/>': '');
    $street = ($street != '' ? $street.', ': '');
    $town = ($town != '' ? $town.'<br/>': '');
    $zip = ($zip != '' ? $zip.'<br/>': '');
    $country = ($country != '' ? $country.'<br/>': '');
    $address = '<br/>'.$businessPark.$street.$town.$state.' '.$zip.$country;
    $phone = WebsiteFooter::where('dealerid', $dealerId)->pluck('phone');
    $fax = WebsiteFooter::where('dealerid', $dealerId)->pluck('fax')
    ?>

        <!-- Contact Form
        ============================================= -->
        <div class="col_half">

            <div class="fancy-title title-dotted-border">
                <h3>Send us an Email</h3>
            </div>

            <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>

            <div class="form-process"></div>

            <div class="col_one_third">
                <label for="template-contactform-name">Name <small>*</small></label>
                <input type="text" id="contact-name" onblur="requiredValidator(this, 'Please enter your name')" class="sm-form-control required" />
            </div>

            <div class="col_one_third">
                <label for="template-contactform-email">Email <small>*</small></label>
                <input type="email" id="contact-email" onblur="requiredValidator(this, 'Please enter email'); emailValidator(this, 'Invalid email')" class="required email sm-form-control" />
            </div>

            <div class="col_one_third col_last">
                <label for="template-contactform-phone">Phone</label>
                <input type="text" id="contact-phone" class="sm-form-control" />
            </div>

            <div class="clear"></div>

            <div class="col_two_third">
                <label for="template-contactform-subject">Subject <small>*</small></label>
                <input type="text" id="contact-subject" onblur="requiredValidator(this, 'Please enter subject')" class="required sm-form-control" />
            </div>

            <div class="clear"></div>

            <div class="col_full">
                <label for="template-contactform-message">Message <small>*</small></label>
                <textarea class="required sm-form-control" onblur="requiredValidator(this, 'Please enter you message here')" id="contact-message" rows="6" cols="30"></textarea>
            </div>
            <div class="col_full">
                <div class="g-recaptcha" id="captcha" data-sitekey="6LeUYhAUAAAAAMHjDHc3dcajFO0Pr1oOw6b1ZlE6"></div>
            </div>
            <div class="col_full hidden">
                <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
            </div>
            <input type="hidden" id="contact-dealer-id" value="{{$dealerId}}" />

            <div class="col_full">
                <button id="contact-submit" type="submit" onclick="sendEmail();" tabindex="5" value="Submit" class="button button-3d nomargin">Submit Enquiry</button>
                <img id="contact-loading" src="{{URL::To('/')}}/assets/images/loading/ring.gif" style="width: 50px; height: auto; display: none;"/>
                <img id="contact-checked" src="{{URL::To('/')}}/assets/images/loading/checked.jpg" style="width: 50px; height: auto; position: absolute; left: 180px; display: none;" />
                <img id="contact-error" src="{{URL::To('/')}}/assets/images/loading/close.jpg" style="width: 50px; height: auto; position: absolute; left: 180px; display: none;" />
            </div>
        </div><!-- Contact Form End -->

        <!-- Google Map
        ============================================= -->
        <div class="col_half col_last">

            <section id="google-map" class="gmap" style="height: 410px;">
                <iframe src="{{Dealer::where('id', $dealerId)->pluck('map_link')}}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </section>

        </div><!-- Google Map End -->

        <div class="clear"></div>

        <!-- Contact Info
        ============================================= -->
        <div class="row clear-bottommargin">
            <?php
                $twitter = WebsiteFooter::where('dealerid', $dealerId)->pluck('twitter');
                if($twitter != '') $colMd = 3;
                else $colMd = 4;
            ?>
            <div class="col-md-{{$colMd}} col-sm-6 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a href="#"><i class="icon-map-marker2"></i></a>
                    </div>
                    <h3>We are located<span class="subtitle">{{$address}}</span></h3>
                </div>
            </div>

            <div class="col-md-{{$colMd}} col-sm-6 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a href="#"><i class="icon-phone3"></i></a>
                    </div>
                    <h3>Speak to Us<span class="subtitle">{{$phone}}</span></h3>
                </div>
            </div>

            <div class="col-md-{{$colMd}} col-sm-6 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a href="{{WebsiteFooter::where('dealerid', $dealerId)->pluck('facebook')}}" target="_blank"><i class="icon-facebook"></i></a>
                    </div>
                    <h3>Keep in touch with<span class="subtitle">Facebook</span></h3>
                </div>
            </div>

            @if($twitter != '')
            <div class="col-md-{{$colMd}} col-sm-6 bottommargin clearfix">
                <div class="feature-box fbox-center fbox-bg fbox-plain">
                    <div class="fbox-icon">
                        <a href="{{$twitter}}" target="_blank"><i class="icon-twitter2"></i></a>
                    </div>
                    <h3>Follow on<span class="subtitle">Twitter</span></h3>
                </div>
            </div>
            @endif
        </div><!-- Contact Info End -->

    </div>
@endsection