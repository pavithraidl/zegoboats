<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/12/16 02:07.
 */
?>

@extends('frontend.master')

@section('content')
    <?php
    $mainDealerId = 14;
    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 2)->where('allow', 1)->pluck('id')) {
        $imageDealerId = $dealerId;
    } else {
        $imageDealerId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 3)->where('allow', 1)->pluck('id')) {
        $topTextBlockId = $dealerId;
    } else {
        $topTextBlockId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 4)->where('allow', 1)->pluck('id')) {
        $videoId = $dealerId;
    } else {
        $videoId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 5)->where('allow', 1)->pluck('id')) {
        $bottomTextBlockId = $dealerId;
    } else {
        $bottomTextBlockId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 7)->where('allow', 1)->pluck('id')) {
        $actionBarId = $dealerId;
    } else {
        $actionBarId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 9)->where('allow', 1)->pluck('id')) {
        $testimonialsId = $dealerId;
    } else {
        $testimonialsId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 6)->where('allow', 1)->pluck('id')) {
        $logoId = $dealerId;
    } else {
        $logoId = $mainDealerId;
    }

    if (SystemOperationAccess::where('userid', User::where('dealerid', $dealerId)->where('roll', 3)->pluck('id'))->where('operationid', 21)->where('allow', 1)->pluck('id')) {
        $featureId = $dealerId;
    } else {
        $featureId = $mainDealerId;
    }
    ?>

    <section id="section-about" class="page-section">{{--Desktop Content--}}
        <div class="container clearfix hidden-xs hidden-sm">
            <div class="col-md-6" style="margin-bottom: 50px;">
                <div class="product-image" style="height: auto;">
                    <div class="fslider" data-pagi="true" data-arrows="true" data-thumbs="true">
                        <div class="flexslider">
                            <div class="slider-wrap"
                                 data-lightbox="gallery"> <?php $imageSliderList = WebsiteImageSlider::where('dealerid', $imageDealerId)->where('status', 1)->lists('id'); ?> @foreach($imageSliderList as $imageId)
                                    <div class="slide"
                                         data-thumb="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/thumb/{{$imageId}}.jpg">
                                        <a href="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/{{$imageId}}.jpg"
                                           title="{{WebsiteImageSlider::where('id', $imageId)->pluck('title')}}" data-lightbox="gallery-item"><img
                                                    src="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/{{$imageId}}.jpg"
                                                    alt="{{WebsiteImageSlider::where('id', $imageId)->pluck('alt')}}"></a>
                                    </div>@endforeach </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6"> <?php $content = WebsiteTextBlocks::where('dealerid', $topTextBlockId)->where('text_block', 1)->pluck('content'); echo htmlspecialchars_decode($content); ?> </div>
        </div>{{--Mobile Content--}}
        <div class="col-md-12 hidden-md hidden-lg">
            <div class="col-md-6" style="margin-bottom: 50px;">
                <div class="product-image" style="height: auto;">
                    <div class="fslider" data-pagi="true" data-arrows="true" data-thumbs="true">
                        <div class="flexslider">
                            <div class="slider-wrap"
                                 data-lightbox="gallery"> <?php $imageSliderList = WebsiteImageSlider::where('dealerid', $imageDealerId)->where('status', 1)->lists('id'); ?> @foreach($imageSliderList as $imageId)
                                    <div class="slide"
                                         data-thumb="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/thumb/{{$imageId}}.jpg">
                                        <a href="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/{{$imageId}}.jpg"
                                           title="{{WebsiteImageSlider::where('id', $imageId)->pluck('title')}}" data-lightbox="gallery-item"><img
                                                    src="{{URL::To('/')}}/assets/images/home_slider/{{$imageDealerId}}/{{$imageId}}.jpg"
                                                    alt="{{WebsiteImageSlider::where('id', $imageId)->pluck('alt')}}"></a>
                                    </div>@endforeach </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6"> <?php $content = WebsiteTextBlocks::where('dealerid', $topTextBlockId)->where('text_block', 1)->pluck('content'); echo htmlspecialchars_decode($content); ?> </div>
        </div>
    </section>
    <section class="page-section">{{--Desktop Content--}}
        <div class="container clearfix hidden-xs hidden-sm">
            <div class="col-md-6" style="margin-bottom: 50px;">
                <iframe src="{{WebsiteVideo::where('dealerid', $videoId)->pluck('link')}}" width="500" height="281"
                        frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <div class="col-md-6"> <?php $content = WebsiteTextBlocks::where('dealerid', $bottomTextBlockId)->where('text_block', 2)->pluck('content'); echo htmlspecialchars_decode($content); ?> </div>
        </div>{{--Mobile Content--}}
        <div class="col-md-12 hidden-md hidden-lg">
            <div class="col-md-6" style="margin-bottom: 50px;">
                <iframe src="{{WebsiteVideo::where('dealerid', $videoId)->pluck('link')}}" width="500" height="281"
                        frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <div class="col-md-6"> <?php $content = WebsiteTextBlocks::where('dealerid', $bottomTextBlockId)->where('text_block', 2)->pluck('content'); echo htmlspecialchars_decode($content); ?> </div>
        </div>
    </section>

    <section id="section-services" class="page-section topmargin-lg">
        <div class="heading-block center bottommargin-lg">
            <h2>Features</h2>
            <span>List of some features included in Zego Sports Boats.</span>
        </div>
        <div class="container clearfix">
            <?php
            $featuresList = WebsiteFeatures::where('dealerid', $featureId)->lists('id');
            $counter = 1;
            ?>
            @foreach($featuresList as $featureId)
                <div class="col_one_third @if($counter%3 == 0)col_last @endif">
                    <div class="feature-box fbox-center fbox-effect nobottomborder" data-animate="fadeIn">
                        <div class="fbox-icon">
                            <i class="fa {{WebsiteFeatures::where('id', $featureId)->pluck('icon')}}"></i>
                        </div>
                        <h3>{{WebsiteFeatures::where('id', $featureId)->pluck('title')}}</h3>
                        <p>{{WebsiteFeatures::where('id', $featureId)->pluck('description')}}</p>
                    </div>
                </div>
                @if($counter%3 == 0)
                    <div class="clear"></div>
                @endif
                <?php $counter = $counter + 1; ?>
            @endforeach
        </div>

        <div class="divider divider-short divider-center topmargin-lg"><i class="icon-star3"></i></div>
        <a href="{{WebsiteActionbar::where('dealerid', $actionBarId)->pluck('link')}}"
           class="button button-full button-dark center tright bottommargin-lg" target="_blank">
            <div class="container clearfix">
                {{WebsiteActionbar::where('dealerid', $actionBarId)->pluck('text')}}
                <button class="btn btn-lg btn-success">Action</button>
                <i class="icon-caret-right" style="top:4px;"></i>
            </div>
        </a>
    </section>

    <section>
        <div class="container clearfix">
            <div class="fancy-title title-border title-center"><h3>Zego Team</h3></div>
            <div id="oc-team-list"
                 class="owl-carousel team-carousel"> <?php $teamList = User::where('dealerid', $dealerId)->where('status', '!=', 0)->lists('id'); ?> @foreach($teamList as $userId) <?php $dirPath = URL::To('/') . '/assets/images/team/' . $userId . '.jpg'; $infoId = User::where('id', $userId)->pluck('infoid'); $name = UserInfo::where('id', $infoId)->pluck('name'); $jobTitle = UserInfo::where('id', $infoId)->pluck('jobtitle'); $description = UserInfo::where('id', $infoId)->pluck('description'); $facebook = UserInfo::where('id', $infoId)->pluck('facebook'); $twitter = UserInfo::where('id', $infoId)->pluck('twitter'); $googleplus = UserInfo::where('id', $infoId)->pluck('googleplus'); $visibility = UserInfo::where('id', $infoId)->pluck('visible'); try {
                    getimagesize($dirPath);
                } catch (Exception $ex) {
                    $dirPath = URL::To('/') . '/assets/images/team/0.jpg';
                }?> @if($visibility==1)
                    <div class="oc-item">
                        <div class="team team-list">
                            <div class="team-image"><img src="{{$dirPath}}" alt="Zego Boats Team -{{$name}}"></div>
                            <div class="team-desc">
                                <div class="team-title"><h4>{{$name}}</h4><span>{{$jobTitle}}</span></div>
                                <div class="team-content"><p>{{$description}}</p></div>@if($facebook !='') <a
                                        href="{{$facebook}}" class="social-icon si-rounded si-small si-facebook"> <i
                                            class="icon-facebook"></i> <i class="icon-facebook"></i>
                                </a> @endif @if($twitter !='') <a href="{{$twitter}}"
                                                                  class="social-icon si-rounded si-small si-twitter"> <i
                                            class="icon-twitter"></i> <i class="icon-twitter"></i>
                                </a> @endif @if($googleplus !='') <a href="{{$googleplus}}"
                                                                     class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i> <i class="icon-gplus"></i> </a> @endif </div>
                        </div>
                    </div>@endif @endforeach </div>
            <script type="text/javascript">jQuery(document).ready(function ($) {
                    var ocTeam = $("#oc-team-list");
                    ocTeam.owlCarousel({
                        responsive: {0: {items: 1}, 600: {items: 1}, 1000: {items: 2}},
                        margin: 30,
                        nav: false,
                        dots: true
                    });
                }); </script>
        </div>
    </section>
    <section id="section-testimonials" class="page-section section parallax dark"
             style="background-image: url('{{URL::To('/')}}/assets/images/paralax/1.jpg'); padding: 200px 0;"
             data-stellar-background-ratio="0.2">
        <div class="container clearfix">
            <div class="col_half nobottommargin">&nbsp;</div>
            <div class="col_half nobottommargin col_last">
                <div class="heading-block center"><h4>What Clients say?</h4> <span>Some of our Clients love us &amp; so we do!</span>
                </div>
                <div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding"
                     data-arrows="false">
                    <div class="flexslider">
                        <div class="slider-wrap"> <?php $testimonialsList = WebsiteTestimonials::where('dealerid', $testimonialsId)->where('status', 1)->where('text', '!=', '')->lists('id'); ?> @foreach($testimonialsList as $testimonialsId)
                                <div class="slide">
                                    <div class="testi-content">
                                        <p>{{WebsiteTestimonials::where('id', $testimonialsId)->pluck('text')}}</p>
                                        <div class="testi-meta">{{WebsiteTestimonials::where('id', $testimonialsId)->pluck('name')}}
                                            <span>{{WebsiteTestimonials::where('id', $testimonialsId)->pluck('position')}}</span>
                                        </div>
                                    </div>
                                </div>@endforeach </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="section footer-stick" style="margin-top: -60px;"><h3 class="center">Logos</h3>
        <div class="col-md-12"> <?php $logoList = WebsiteBottomLogo::where('dealerid', $logoId)->where('status', 1)->lists('id'); ?>
            <p class="col-md-12" style="text-align: center !important;"> @foreach($logoList as $logoListId) <img
                        src="{{URL::To('/')}}/assets/images/logos/{{$logoId}}/{{$logoListId}}.jpg"
                        alt="{{WebsiteBottomLogo::where('id', $logoListId)->pluck('alt')}}"
                        style="max-width: 200px; max-height: 200px; margin-left: 50px;"> @endforeach </p></div>
        <script type="text/javascript">jQuery(document).ready(function ($) {
                var ocClientsFull = $("#oc-clients-full");
                ocClientsFull.owlCarousel({
                    items: 5,
                    margin: 30,
                    loop: true,
                    nav: true,
                    navText: ['<i class="icon-angle-left"></i>', '<i class="icon-angle-right"></i>'],
                    autoplay: true,
                    autoplayHoverPause: false,
                    dots: false,
                    navRewind: false,
                    responsive: {
                        0: {items: 2},
                        480: {items: 3},
                        768: {items: 4},
                        992: {items: 5},
                        1200: {items: 7},
                        1400: {items: 8}
                    }
                });
            }); </script>
    </div>

@endsection