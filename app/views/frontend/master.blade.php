<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/12/16 02:07.
 */
?>

        <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <!-- Stylesheets
    ============================================= -->
    {{HTML::style('assets/css/fonts/google-fonts.css?v=001', array('async' => ''))}}
    {{HTML::style('assets/css/bootstrap.css?v=001', array('async' => ''))}}
    {{HTML::style('assets/style.css')}}
    {{HTML::style('assets/css/dark.css')}}
    {{HTML::style('assets/css/font-icons.css', array('async' => ''))}}
    {{HTML::style('assets/css/animate.css', array('async' => ''))}}
    {{HTML::style('assets/css/magnific-popup.css', array('async' => ''))}}
    {{HTML::style('assets/css/responsive.css')}}
    {{HTML::style('assets/admin/libs/font-awesome/css/font-awesome.min.css', array('async' => ''))}}

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="{{WebsiteSettings::where('id', $dealerId)->pluck('author')}}"/>
    <meta name="keywords" content="{{WebsiteSettings::where('id', $dealerId)->pluck('meta_keywords')}}">
    <meta name="description" content="{{WebsiteSettings::where('id', $dealerId)->pluck('meta_description')}}">
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript"  src="{{URL::To('/')}}/assets/admin/libs/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{URL::To('/')}}/assets/js/jquery.js"></script>
    <script type="text/javascript" src="{{URL::To('/')}}/assets/js/plugins.js"></script>
    <script async src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/admin/img/favicon.ico">

    {{--GA Scripts and Other Scripts--}}
    <?php
    $gaScript = WebsiteSettings::where('dealerid', $dealerId)->pluck('ga_script');
    $otherScript = WebsiteSettings::where('dealerid', $dealerId)->pluck('other_scripts');
    $scripts = $gaScript . $otherScript;
    echo htmlspecialchars_decode($scripts);
    ?>

    <title>Home - Zego Boats</title>
</head>
<?php
$facebook = WebsiteFooter::where('dealerid', $dealerId)->pluck('facebook');
$twitter = WebsiteFooter::where('dealerid', $dealerId)->pluck('twitter');
$googleplus = WebsiteFooter::where('dealerid', $dealerId)->pluck('googleplus');
$instagram = WebsiteFooter::where('dealerid', $dealerId)->pluck('instagram');
$youtube = WebsiteFooter::where('dealerid', $dealerId)->pluck('youtube');

$name = Dealer::where('id', $dealerId)->pluck('name');
$businessPark = Dealer::where('id', $dealerId)->pluck('business_park');
$street = Dealer::where('id', $dealerId)->pluck('street');
$town = Dealer::where('id', $dealerId)->pluck('town');
$zip = Dealer::where('id', $dealerId)->pluck('zip');
$state = Dealer::where('id', $dealerId)->pluck('state');
$country = Dealer::where('id', $dealerId)->pluck('country');

$businessPark = ($businessPark != '' ? $businessPark . '<br/>' : '');
$street = ($street != '' ? $street . ', ' : '');
$town = ($town != '' ? $town . '<br/>' : '');
$zip = ($zip != '' ? $zip . '<br/>' : '');
$country = ($country != '' ? $country . '<br/>' : '');
$address = '<br/>' . $businessPark . $street . $town . $state . ' ' . $zip . $country;
$phone = WebsiteFooter::where('dealerid', $dealerId)->pluck('phone');
$fax = WebsiteFooter::where('dealerid', $dealerId)->pluck('fax')
?>
<body class="stretched">
<div id="wrapper" class="clearfix">
    <div id="home" class="page-section"
         style="position:absolute;top:0;left:0;width:100%;height:200px;z-index:-2;"></div>@if($page=='home')
        <section id="slider" class="slider-parallax full-screen with-header swiper_wrapper clearfix">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper"> <?php $dirPath = sprintf('%s/assets/images/main_slider/%s.jpg?lastmod=1483062421697', URL::To('/'), $dealerId); try {
                        getimagesize($dirPath);
                    } catch (Exception $ex) {
                        $dirPath = URL::To('/') . '/assets/images/main_slider/bg-1.jpg';
                    }?>
                    <div class="swiper-slide dark" style="background-image: url('{{$dirPath}}');" title="{{WebsiteSettings::where('dealerid', $dealerId)->pluck('header-image-title')}}" role="img">
                        <div class="row" id="main-slider-top-row">
                            <div class="col-md-12 col-centered" id="main-slider-social-icons"
                                 style="position: relative;"><p
                                        style="text-align: center !important; margin-left: 43%; margin-top: 10px; box-shadow: 1px 1px 5px #3b46ff;"> @if($facebook !='')
                                        <a href="{{$facebook}}" class="social-icon si-rounded si-small si-facebook" target="_blank"> <i
                                                    class="icon-facebook"></i> <i class="icon-facebook"></i>
                                        </a> @endif @if($twitter !='') <a href="{{$twitter}}"
                                                                          class="social-icon si-rounded si-small si-twitter" target="_blank">
                                        <i class="icon-twitter"></i> <i class="icon-twitter"></i>
                                    </a> @endif @if($googleplus !='') <a href="{{$googleplus}}"
                                                                         class="social-icon si-rounded si-small si-gplus" target="_blank">
                                        <i class="icon-gplus"></i> <i class="icon-gplus"></i>
                                    </a> @endif @if($instagram !='') <a href="{{$instagram}}"
                                                                        class="social-icon si-rounded si-small si-instagram" target="_blank">
                                        <i class="icon-instagram"></i> <i class="icon-instagram"></i> </a> @endif
                                    @if($youtube !='') <a href="{{$youtube}}"
                                                            class="social-icon si-rounded si-small si-youtube" target="_blank">
                                        <i class="icon-youtube"></i> <i class="icon-youtube"></i> </a> @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>jQuery(document).ready(function ($) {
                    var swiperSlider = new Swiper('.swiper-parent', {
                        paginationClickable: false,
                        slidesPerView: 1,
                        grabCursor: true,
                        onSwiperCreated: function (swiper) {
                            $('[data-caption-animate]').each(function () {
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;
                                if (toAnimateDelay) {
                                    toAnimateDelayTime = Number(toAnimateDelay) + 750;
                                } else {
                                    toAnimateDelayTime = 750;
                                }
                                if (!$toAnimateElement.hasClass('animated')) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function () {
                                        $toAnimateElement.removeClass('not-animated').addClass(elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        },
                        onSlideChangeStart: function (swiper) {
                            $('#slide-number-current').html(swiper.activeIndex + 1);
                            $('[data-caption-animate]').each(function () {
                                var $toAnimateElement = $(this);
                                var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                $toAnimateElement.removeClass('animated').removeClass(elementAnimation).addClass('not-animated');
                            });
                        },
                        onSlideChangeEnd: function (swiper) {
                            $('#slider .swiper-slide').each(function () {
                                if ($(this).find('video').length > 0) {
                                    $(this).find('video').get(0).pause();
                                }
                            });
                            $('#slider .swiper-slide:not(".swiper-slide-active")').each(function () {
                                if ($(this).find('video').length > 0) {
                                    if ($(this).find('video').get(0).currentTime != 0) $(this).find('video').get(0).currentTime = 0;
                                }
                            });
                            if ($('#slider .swiper-slide.swiper-slide-active').find('video').length > 0) {
                                $('#slider .swiper-slide.swiper-slide-active').find('video').get(0).play();
                            }
                            $('#slider .swiper-slide.swiper-slide-active [data-caption-animate]').each(function () {
                                var $toAnimateElement = $(this);
                                var toAnimateDelay = $(this).attr('data-caption-delay');
                                var toAnimateDelayTime = 0;
                                if (toAnimateDelay) {
                                    toAnimateDelayTime = Number(toAnimateDelay) + 300;
                                } else {
                                    toAnimateDelayTime = 300;
                                }
                                if (!$toAnimateElement.hasClass('animated')) {
                                    $toAnimateElement.addClass('not-animated');
                                    var elementAnimation = $toAnimateElement.attr('data-caption-animate');
                                    setTimeout(function () {
                                        $toAnimateElement.removeClass('not-animated').addClass(elementAnimation + ' animated');
                                    }, toAnimateDelayTime);
                                }
                            });
                        }
                    });
                    $('#slider-arrow-left').on('click', function (e) {
                        e.preventDefault();
                        swiperSlider.swipePrev();
                    });
                    $('#slider-arrow-right').on('click', function (e) {
                        e.preventDefault();
                        swiperSlider.swipeNext();
                    });
                    $('#slide-number-current').html(swiperSlider.activeIndex + 1);
                    $('#slide-number-total').html(swiperSlider.slides.length);
                }); </script>
        </section> @endif<!-- Header --=============================================-->
    <header id="header" class="full-header">
        <div id="header-wrap">
            <div class="container clearfix"><h5 class="hidden-xs hidden-sm"
                                                style="text-align: center !important; position: absolute; top: 12px; width: 100%; z-index: 0; font-size: 28px; font-weight: 200;">{{Dealer::where('id', $dealerId)->pluck('identity')}}</h5>
                <h5 class="hidden-md hidden-lg"
                    style="text-align: center !important; position: absolute; top: 95px; width: 100%; z-index: 100; font-size: 20px; font-weight: 200;">{{Dealer::where('id', $dealerId)->pluck('identity')}}</h5>
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <!-- Logo=============================================-->
                <div id="logo"><a href="{{URL::Route('Home')}}" class="standard-logo"
                                  data-dark-logo="{{URL::To('/')}}/assets/images/zego-logo.png"><img
                                src="{{URL::To('/')}}/assets/images/zego-logo.png" alt="Zego Sports Boats Logo"></a> <a
                            href="{{URL::Route('Home')}}" class="retina-logo"
                            data-dark-logo="{{URL::To('/')}}/assets/images/zego-logo.png"><img
                                src="{{URL::To('/')}}/assets/images/zego-logo.png" alt="Zego Sports Boats Logo"></a>
                </div><!-- Primary Navigation=============================================-->
                <nav id="primary-menu" style="z-index: 1000 !important;">
                    <ul class="one-page-menu">
                        <li><a href="{{URL::Route('Home')}}">
                                <div @if($page=='home')style="color: #1ABC9C;"@endif>Home</div>
                            </a></li>
                        <li><a href="{{URL::Route('dealers')}}">
                                <div @if($page=='dealers')style="color: #1ABC9C;"@endif>Dealers</div>
                            </a></li>
                        <li><a href="{{URL::Route('contact')}}">
                                <div @if($page=='contact')style="color: #1ABC9C;"@endif>Contact Us</div>
                            </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header><!-- Content=============================================-->
    <section id="content">
        <div class="content-wrap">{{--Content connects here--}}@yield('content'){{--Content ends here--}}</div>
    </section><!-- Footer=============================================-->
    <footer id="footer" class="dark">
        <div class="container">
            <div class="footer-widgets-wrap clearfix">
                <div class="col_one_third">
                    <div class="widget clearfix"><img src="{{URL::To('/')}}/assets/images/logo-dark-small.png"
                                                      alt="Zego boats footer logo" class="footer-logo">
                        <p>{{WebsiteFooter::where('dealerid', $dealerId)->pluck('quote')}}</p></div>
                </div>
                <div class="col_one_third">
                    <div style="background: url('{{URL::To('/')}}/assets/images/support/world-map.png') no-repeat center center; background-size: 100%;">
                        <address><strong>Address:</strong><br>{{$address}}</address> @if($phone !='')<abbr
                                title="Phone Number"><strong>Phone:</strong></abbr>{{$phone}}<br>@endif @if($fax !='')
                            <abbr title="Fax"><strong>Fax:</strong></abbr>{{$fax}}<br>@endif </div>
                </div>
                <div class="col_one_third col_last">
                    <div class="widget quick-contact-widget clearfix">
                        <div class="widget clearfix"></div>
                        <div class="widget clearfix"> @if($facebook !='') <a href="{{$facebook}}"
                                                                             class="social-icon si-rounded si-small si-facebook" target="_blank">
                                <i class="icon-facebook"></i> <i class="icon-facebook"></i>
                            </a> @endif @if($twitter !='') <a href="{{$twitter}}"
                                                              class="social-icon si-rounded si-small si-twitter" target="_blank"> <i
                                        class="icon-twitter"></i> <i class="icon-twitter"></i>
                            </a> @endif @if($googleplus !='') <a href="{{$googleplus}}"
                                                                 class="social-icon si-rounded si-small si-gplus" target="_blank"> <i
                                        class="icon-gplus"></i> <i class="icon-gplus"></i>
                            </a> @endif @if($instagram !='') <a href="{{$instagram}}"
                                                                class="social-icon si-small si-rounded si-instagram" target="_blank"> <i
                                        class="icon-instagram"></i> <i class="icon-instagram"></i> </a> @endif
                            @if($youtube !='') <a href="{{$youtube}}"
                                                    class="social-icon si-small si-rounded si-youtube" target="_blank"> <i
                                        class="icon-youtube"></i> <i class="icon-youtube"></i> </a> @endif</div>
                    </div>
                </div>
            </div>
        </div><!-- Copyrights=============================================-->
        <div id="copyrights">
            <div class="container clearfix">
                <div class="col_half"> Copyrights &copy;{{date('Y')}}All Rights Reserved by Zego International
                    Limited.
                </div>
                <div class="col_half col_last tright">{{--<div class="fright clearfix">--}}{{--<div class="copyrights-menu copyright-links nobottommargin">--}}{{--<a href="#">Home</a>/<a href="#">About</a>/<a href="#">Options</a>/<a href="#">Dealers</a>/<a href="#">Contact Us</a>--}}{{--</div>--}}{{--</div>--}}</div>
            </div>
        </div>
    </footer>
</div><!-- Go To Top=============================================-->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/js/functions.js')}}
{{HTML::script('/assets/admin/libs/jstimezone/jstz.min.js')}}
{{HTML::script('/assets/admin/js/custom/validator.js')}}
{{HTML::script('/assets/admin/js/custom/constant.js')}}
{{HTML::script('/assets/js/custom/email.js')}}

</body>
</html>
