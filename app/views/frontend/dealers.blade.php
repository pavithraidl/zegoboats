<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 31/12/16 14:19.
 */
?>

@extends('frontend.master')

@section('content');
    <div class="section">

        <div class="container clearfix">
            <?php
                $dealerList = Dealer::where('status', '!=', 0)->where('visible', 1)->orderBy('order', 'asc')->orderBy('country', 'asc')->lists('id');
                $counter = 1;
            ?>
            @foreach($dealerList as $dealerPageId)
                @if(Dealer::where('id', $dealerPageId)->pluck('identity') != 'System Account')
                    <div class="col_one_third nobottommargin @if($counter%3 == 0) col_last @endif" style="margin-top: 40px;">
                        <div class="feature-box fbox-plain">
                            <div class="fbox-icon">
                                <a href="#"><i class="i-alt">{{$counter}}.</i></a>
                            </div>
                            <h3>{{Dealer::where('id', $dealerPageId)->pluck('identity')}}</h3>
                            <p>
                                <strong>Contact: </strong>{{Dealer::where('id', $dealerPageId)->pluck('name')}}<br/>
                                <?php
                                    $businessPark = Dealer::where('id', $dealerPageId)->pluck('business_park');
                                    $street = Dealer::where('id', $dealerPageId)->pluck('street');
                                    $town = Dealer::where('id', $dealerPageId)->pluck('town');
                                    $state = Dealer::where('id', $dealerPageId)->pluck('state');
                                    $zip = Dealer::where('id', $dealerPageId)->pluck('zip');
                                    $country = Dealer::where('id', $dealerPageId)->pluck('country');
                                    $phone = Dealer::where('id', $dealerPageId)->pluck('phone');
                                    $visit = Dealer::where('id', $dealerPageId)->pluck('url');

                                    if($businessPark != '') $businessPark = '<br/>'.$businessPark;
                                    if($street != '') $street = '<br/>'.$street;
                                    if($town != '') $town = '<br/>'.$town;
                                    if($state != '') $state = ' '.$state;
                                    if($zip != '') $zip = ' '.$zip;
                                    if($country != '') $country = '<br/>'.$country;

                                    if($country == '') {
                                        $address = '';
                                    }
                                    else {
                                        $address = $businessPark.$street.$town.$state.$zip.$country;
                                    }

                                ?>
                                @if($address != '')
                                    <strong>Address: </strong><div style="margin-left: 40px; margin-top: -20px; margin-bottom: -30px; color: #7b7b7b;"> {{$address}}</div><br>
                                @endif
                                <br/><br/>
                            @if($phone != '')
                                <strong>Phone: <a href="tel:{{$phone}}"> {{$phone}}</a></strong><br/>
                            @endif
                            @if($visit != '#')
                                <strong>Visit: </strong><a href="{{$visit}}">{{$visit}}</a>
                            @endif
                            </p>
                        </div>
                    </div>
                    <?php $counter++ ?>
                @endif
            @endforeach
        </div>
    </div>
@endsection