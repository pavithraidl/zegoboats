<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 31/12/16 09:32.
 */
?>

@extends('frontend.master')

@section('content')
    <div class="container clearfix">

        <div class="col_half nobottommargin">
            <div class="error404 center">404</div>
        </div>

        <div class="col_half nobottommargin col_last">

            <div class="heading-block nobottomborder">
                <h4>Ooopps.! The Page you were looking for, couldn't be found.</h4>
                <span>Try to check with our dealers list</span>
            </div>

            <a href="{{URL::Route('dealers')}}" ><button class="btn btn-sm btn-success">View Dealers</button> </a>

        </div>

    </div>
@endsection
