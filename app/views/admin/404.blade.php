<?php
/*************************************************************************
 * 
 * IDL CONFIDENTIAL
 * __________________
 * 
 *  [20014] - [2024] IDL Creations Incorporated 
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of IDL Creations Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to IDL Creations Incorporated
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from IDL Creations Incorporated.
 *
 * Developed by - Pavithra Isuru D. Liyanage
 * Authorized by - IDL Creaitons Incorporated
 * Created on - 11/26/2014.
 */
 ?>

 <!--|--------------------------------------------------------------------------
 	| Authorized 404 Page
 	|--------------------------------------------------------------------------
 	|
 	| This page act as the 404 page for loged in users
 	|
 	|
 	*/-->
 {{--import the header and footer from template.blade.php file--}}
 @extends('admin.master')

 @section('content')
 <div class="content" style="background-color: #ffffff;">
 <div class="center-block">
 {{--{{HTML::image('assets/img/404.jpg', '404 error image', array('style' => 'position: relative;margin-left:20%;width:30%;height:auto;min-width:200px;'))}}--}}
 </div>
<div class="full-content-center animated flipInX">
			<h1>404</h1>
			<h2>The page you are looking is not available in the system</h2><br>
			<br>
			<a class="btn btn-primary btn-sm" href="{{URL::route('admin')}}"><i class="fa fa-angle-left"></i> Back to Dashboard</a>
		</div>
 </div>
 @endsection