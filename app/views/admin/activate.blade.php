<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 21/12/16 10:51.
 */
?>

        <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Activate | Zego Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="Zego dsahboard login">
    <meta name="author" content="Pavithra Isuru">

    <!-- Base Css Files -->
{{HTML::script('/assets/admin/libs/jquery/jquery-1.11.1.min.js')}}
{{HTML::style('/assets/admin/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css')}}
{{HTML::style('/assets/admin/libs/bootstrap/css/bootstrap.min.css')}}
{{HTML::style('/assets/admin/libs/font-awesome/css/font-awesome.min.css')}}
{{HTML::style('/assets/admin/libs/animate-css/animate.min.css')}}
<!-- Code Highlighter for Demo -->

    <!-- Extra CSS Libraries Start -->
{{HTML::style('/assets/admin/css/style.css')}}
<!-- Extra CSS Libraries End -->
    {{HTML::style('/assets/admin/css/style-responsive.css')}}

    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/admin/img/favicon.ico">
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-152x152.png" />
</head>
<body class="fixed-left login-page">
<!-- Begin page -->
<div class="container">
    <div class="full-content-center">
        <p class="text-center"><a href="#"><img src="{{URL::To('/')}}/assets/admin/img/logo.png" style="width: 150px; height: auto;;" alt="Logo"></a></p>
        <div class="row">
            <div class="col-md-12">
                <h2 style="color: #ffffff;">Welcome to Zego Boats Admin Panel</h2>
                <br/>
            </div>
        </div>
        <div id="activation-form" class="login-wrap animated flipInX">
            <div class="login-block">
                <h5 style="color: #dadada; margin-bottom: 20px;">Please fill the following fields to complete your activation</h5>
                <div class="form-group login-input">
                    <i class="fa fa-user overlay"></i>
                    <input id="activate-name" onblur="validateName();" type="text" class="form-control text-input" placeholder="Your Name" value="{{$name}}" required="true" maxlength="32">
                </div>
                <div class="form-group login-input">
                    <i class="fa fa-phone overlay"></i>
                    <input id="activate-phone" type="text" onblur="validatePhone();" class="form-control text-input" placeholder="Your Phone Number" required="true" tabindex="1" maxlength="16">
                </div><br/>
                <div class="form-group login-input">
                    <i class="fa fa-envelope overlay"></i>
                    <input id="activate-email" name="email" type="email" class="form-control text-input" placeholder="Email" required="true" value="{{$email}}" disabled>
                </div>
                <div class="form-group login-input">
                    <i class="fa fa-key overlay"></i>
                    <input id="activate-password" type="password" onblur="validatePassword();" class="form-control text-input" placeholder="New Password" required="true" tabindex="2" maxlength="60">
                </div>
                <div class="form-group login-input">
                    <i class="fa fa-key overlay"></i>
                    <input id="activate-confirm-password" onblur="validateConfirmPassword();"  type="password" class="form-control text-input" placeholder="Confirm New Password" required="true" tabindex="3" maxlength="60">
                </div>
                <input type="hidden" id="active-code" value="{{$code}}" />
                <div class="row">
                    <div class="col-sm-6 col-lg-push-3">
                        <button type="submit" onclick="activate();" id="activate-btn-activate" class="btn btn-success btn-block" tabindex="4">ACTIVATE</button>
                        <img id="activate-loading-img" src="{{URL::To('/')}}/assets/admin/img/loading/loading.gif" style="display: none;"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="activate-success" style="display: none;">
            <div class="col-md-12">
                <p style="text-align: center;">
                    <h4 style="color: #ffffff;">Your account has activated. You can now log in to the system.</h4>
                    <a href="{{URL::Route('admin-get-login')}}"><button class="btn btn-lg btn-success">Login</button> </a>
                </p>

            </div>
        </div>
    </div>
</div>
<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
</body>
{{HTML::script('/assets/admin/js/custom/constant.js')}}
{{HTML::script('/assets/admin/js/custom/activate.js')}}
</html>
