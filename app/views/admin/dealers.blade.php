<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 09:45.
 */
?>

@extends('admin.master')

@section('content')
    @include('admin.includes.new-dealer')
    <div class = "page-heading">
        <div class="row" style="margin-right: 20px;">
            <h1 class="pull-left"><i class = 'fa fa-building'></i> Dealers Manage</h1>
            @if(SystemOperationAccess::where('operationid', 11)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
            <button class="pull-right btn btn-blue-3 btn-sm md-trigger" style="margin-top: 20px;" data-modal="mm-new-dealer"><i class="fa fa-plus-circle"></i> Add Dealer</button>
            @endif
        </div>
    </div>
    <div class = "page-content">
        <div class = "col-md-12">
            <div class="widget" id="dealer-manage-widget">
                <div class="widget-content padding">
                    <div class = "col-md-3 bs-callout bs-callout-info" style = "border-radius: 10px; margin-top: -10px;">
                        <div class = "row">
                            <div class = "col-md-12">
                                <div class = "form-group form-search search-box has-feedback">
                                    <span id = "searchSpan">
                                        <input type = "text" class = "form-control full-rounded" onkeyup = "searchFilter(this);" id = "searchText" placeholder = "Search..">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class = "row" style = "margin-top: 20px;">
                            <ul id = "dealer-dealer-list-container" class = "search-filter" style = "list-style-type: none; margin-left: -30px;">
                                {{--JQuery append--}}
                            </ul>
                        </div>
                    </div>
                    <div class = "col-md-3" style = "padding: 5px;">
                        <div class = " shadow-pane-1" style = "margin-top: -10px;">
                            <div class = "row">
                                <div class = "col-md-12">
                                    <div data-toggle = "tooltip" title = "Active/Deactive Dealer" style = "visibility: visible; position: absolute;">
                                        <input data-id="dealer-status" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" checked hidden = "hidden"/>
                                    </div>

                                    <p style = "text-align: center;">
                                        <img id="dealer-image" class="img-circle" src="{{URL::To('/')}}/assets/admin/img/dealer/default/logo-100.png" />
                                    </p>
                                </div>
                            </div>
                            <div class = "row">
                                <h4 class = "text-blue-3" style = "text-align: center; font-size: 18px;">
                                    <strong id = "dealer-idntity">NULL</strong>
                                </h4>
                            </div>
                            <div class = "row" style = "margin-top: -10px;">
                                <p style = "text-align: center; font-weight: 600; color: rgba(0, 0, 0, 0.30)" id="dealer-name">null</p>
                            </div>
                            <div class = "row">
                                <ul style = "list-style-type: none; margin-left: -20px;margin-top: 20px;">
                                    <li style = "margin-top: 5px;">
                                        <a href = "#">
                                            <i id = "dealer-status-icon" class = "fa fa-bookmark text-green-2"></i>
                                            &nbsp;<span id = "dealer-status-text">Active</span></a>
                                    </li>
                                    <li style = "margin-top: 5px;">
                                        <a id="dealer-url" href = "#">
                                            <i id = "o-m-account-active-status-icon" class = "fa fa-globe text-blue-1"></i>
                                            &nbsp;<span>View Website</span></a>
                                    </li>
                                    {{--<li style = "margin-top: 5px;">--}}
                                        {{--<a id="dealer-contact" href = "#">--}}
                                            {{--<i id = "o-m-account-active-status-icon" class = "fa fa-phone text-green-3"></i>--}}
                                            {{--&nbsp;<span>Contact</span></a>--}}
                                    {{--</li>--}}
                                    {{--<li style = "margin-top: 5px;">--}}
                                        {{--<a id="dealer-email" href = "#">--}}
                                            {{--<i id = "o-m-account-active-status-icon" class = "fa fa-envelope text-yellow-1"></i>--}}
                                            {{--&nbsp;<span id = "o-m-account-active-status-text">Email</span></a>--}}
                                    {{--</li>--}}
                                    <li style = "margin-top: 15px;">
                                        <button id ="dealer-resend-activation-email" onclick="resendActivationEmail();" class = "btn btn-default" style = "width: 90%;text-align: center; display: none;">
                                            <i class = "fa fa-envelope"></i> Resend Activation Email
                                        </button>
                                        @if(SystemOperationAccess::where('operationid', 14)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                                        <button id = "dealer-delete-dealer" onclick="deleteDealer();" class = "btn btn-danger" style = "width: 90%;text-align: center; visibility: visible; margin-top: 10px; display: none;">
                                            <i class = "fa fa-trash"></i> Remove Dealer
                                        </button>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class = "col-md-6">
                        <ul id = "o-m-org-manage-container" class = "nav nav-tabs nav-simple">
                            <li id="dealer-tab-settings" class="active">
                                <a id="dealer-tab-settings" href="#dealer-tab-container-settings" data-toggle="tab"><i class="fa fa-wrench"></i>
                                Settings</a>
                            </li>
                            @if(SystemOperationAccess::where('operationid', 12)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <li class = "" id="dealer-tab-access">
                                <a href = "#dealer-tab-container-access" data-toggle = "tab"><i class = "icon-lock-alt text-orange-3"></i>
                                    Access</a>
                            </li>
                            @endif
                            @if(SystemOperationAccess::where('operationid', 13)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <li id="dealer-tab-activity-log" class = "">
                                <a id = "dealer-tab-activity-log" href = "#dealer-tab-container-activity-log" data-toggle = "tab"><i class = "fa fa-bullhorn text-green-3"></i>
                                    Activity Log</a>
                            </li>
                            @endif
                        </ul>

                        <div class = "tab-content" style = "padding: 20px;">
                            <!-- / .tab-pane Settings Log-->
                            <div class = "tab-pane fade active in" id = "dealer-tab-container-settings" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                <div class = "row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom: 20px;">
                                            <label for="dealer-settings-visibility">Show in the Frontend Dealers List</label>
                                            <input id = "dealer-settings-visibility" data-id = "dealer-visibility" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right"/>
                                        </div>
                                        <div class = "form-group">
                                            <label>Dealership Name</label>
                                            <input id = "dealer-settings-identity" onblur="requiredValidator(this, 'Please enter the dealer\'s identity.');" onchange="saveSettings('identity');" onkeyup="requiredValidator(this, 'Please enter the dealer\'s identity.')" type = "text" class = "form-control" placeholder = "NZ - Mt Roskill" maxlength = "32" required tabindex="5">
                                        </div>
                                        <div class = "form-group">
                                            <label>Main Contact Name</label>
                                            <input id = "dealer-settings-name" onblur="requiredValidator(this, 'Please enter the dealer\'s name.');" onchange="saveSettings('name');" onkeyup="requiredValidator(this, 'Please enter the dealer\'s name.');" type = "text" class = "form-control" placeholder = "Jone Doe" maxlength = "32" required tabindex="6">
                                        </div>
                                        <div class = "form-group">
                                            <label>Main Contact Email</label>
                                            <input id = "dealer-settings-email" type = "email" class = "form-control" placeholder = "someone&#64;example.com" onchange="saveEmail();"  maxlength = "60" required tabindex="7">
                                        </div>
                                        <div class = "form-group">
                                            <label>Website Link</label>
                                            <input id = "dealer-settings-url" onblur="requiredValidator(this, 'Please enter the website URL.');" onchange="saveSettings('url');" onkeyup="requiredValidator(this, 'Please enter the website URL.')" type = "url" class = "form-control" placeholder = "zego.co.nz" maxlength = "64" tabindex="8">
                                        </div>
                                        <div class = "form-group">
                                            <label>Dealer Order</label>
                                            <input id = "dealer-settings-order" onblur="" onchange="saveSettings('order');" type = "number" class = "form-control" placeholder = "1 - top, 1000 - last" maxlength = "64" tabindex="9">
                                        </div>

                                        <h4 style="margin-top: 30px;">Contact Details</h4>
                                        <div class="form-group">
                                            <label for="dealer-settings-business_park">Business Park Name</label>
                                            <input id="dealer-settings-business_park" onchange="saveSettings('business_park');" type = "text" class = "form-control" maxlength="32" />
                                        </div>
                                        <div class="form-group">
                                            <label for="dealer-settings-street">Street Address</label>
                                            <input id="dealer-settings-street" onchange="saveSettings('street');" type = "text" class = "form-control" maxlength="32" />
                                        </div>
                                        <div class="form-group">
                                            <label for="dealer-settings-town">Town/City</label>
                                            <input id="dealer-settings-town" onchange="saveSettings('town');" type = "text" class = "form-control" maxlength="32" />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="dealer-settings-state">State</label>
                                            <input id="dealer-settings-state" onchange="saveSettings('state');" type = "text" class = "form-control" maxlength="32" />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="dealer-settings-zip">Zip/Post Code</label>
                                            <input id="dealer-settings-zip" onchange="saveSettings('zip');" type = "text" class = "form-control" maxlength="10" />
                                        </div>
                                        <div class="form-group">
                                            <label for="dealer-settings-country">Country</label>
                                            <input id="dealer-settings-country" onchange="saveSettings('country');" type = "text" class = "form-control" maxlength="32" />
                                        </div><br/>
                                        <div class="form-group">
                                            <label for="dealer-settings-phone">Main Phone</label>
                                            <input id="dealer-settings-phone" onchange="saveSettings('phone');" type = "text" class = "form-control" maxlength="14" />
                                        </div>
                                        <div class="form-group">
                                            <label for="dealer-settings-map_link">Map Link</label>
                                            <input id="dealer-settings-map_link" onchange="saveSettings('map_link');" type = "text" class = "form-control" maxlength="4000" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- / .tab-pane -->
                        @if(SystemOperationAccess::where('operationid', 12)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <!-- / .tab-pane Access-->
                            <div class = "tab-pane fade" id = "dealer-tab-container-access">
                                <div id="o-m-access-control-container" class = "row">
                                    <div class="panel-group accordion-toggle" id="accordiondemo">
                                        <?php
                                        $systemList = System::where('status', 1)->where('visibility', 1)->lists('id');
                                        ?>
                                        @foreach($systemList as $systemId)
                                            @if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion{{$systemId}}">
                                                                <h4 style="margin-top: 0; margin-bottom: 5px;" id="o-m-text-container-1" onclick="">
                                                                    <i class = "{{System::where('id', $systemId)->pluck('icon')}}"></i> {{System::where('id', $systemId)->pluck('name')}}
                                                                </h4>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="accordion{{$systemId}}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <ul style="list-style-type: none;">
                                                                <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px; font-size: 18px; font-weight: 600;">
                                                                    <div class="col-md-7" id="user-operation-text-container-{{$systemId}}">
                                                                        <i class="{{System::where('id', $systemId)->pluck('icon')}}"></i> &nbsp;Access {{System::where('id', $systemId)->pluck('name')}}
                                                                    </div>
                                                                    <div class="col-md-4" style="cursor: pointer">
                                                                        <input id = "user-access-system-operation-{{$systemId}}" data-id = "sa-sy-{{$systemId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-md pull-left" checked/>
                                                                    </div>
                                                                </li>
                                                                <hr>
                                                                <?php $systemOperationList = SystemOperation::where('status', 1)->where('systemid', $systemId)->lists('id'); ?>
                                                                @foreach($systemOperationList as $operationId)
                                                                    <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px;">
                                                                        <div class="col-md-7" id="user-operation-text-container-{{$operationId}}">
                                                                            <i class="{{SystemOperation::where('id', $operationId)->pluck('icon')}}"></i> &nbsp;{{SystemOperation::where('id', $operationId)->pluck('operation')}}
                                                                        </div>
                                                                        <div class="col-md-4" style="cursor: pointer">
                                                                            <input id = "user-access-system-operation-{{$operationId}}" data-id = "sa-op-{{$operationId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-left" checked/>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- / .tab-pane Activity Log-->
                            <div class = "tab-pane fade" id = "dealer-tab-container-activity-log" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                <div id = "dealer-activity-list-container" class = "row">
                                    <ul id = "dealer-activity-list" class = "media-list">

                                    </ul>
                                </div>
                            </div>
                            <!-- / .tab-pane -->
                        </div>
                        <!-- / .tab-content -->
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('assets/admin/js/custom/dealers-manage.js')}}
@endsection