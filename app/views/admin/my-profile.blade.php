<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 18:13.
 */
?>

@extends('admin.master')

@section('content')
    <div class = "page-heading">
        <div class="row" style="margin-right: 20px;">
            <h1 class="pull-left"><i class = 'fa fa-user'></i> Team Profile</h1>
            @if(SystemOperationAccess::where('operationid', 19)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
            <input type = "checkbox" data-id="profile-visible" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" checked/>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="oc-item">
                <div class="team team-list">
                    <div class="team-image">
                        <?php
                            $dirPath = URL::To('/').'/assets/images/team/'.Auth::user()->id.'.jpg';
                            $name = UserInfo::where('id', Auth::user()->infoid)->pluck('name');
                        try {
                            getimagesize($dirPath);
                        }
                        catch(Exception $ex) {
                            $dirPath = URL::To('/').'/assets/images/team/0.jpg';
                        }
                        ?>
                        <img id="profile-image" src="{{$dirPath}}" alt="Zego Boats Team - {{$name}}">
                        <div style="display: none;">
                            <form action="{{URL::Route('admin-profile-change-image')}}" method="post" id="profile-image-upload" enctype="multipart/form-data" class="frm-image-upload">
                                <input type="file" name="file" id="profile-image-choose" onchange="submitForm();" accept="image/jpeg, image/jpg" style="display: none;">
                            </form>
                        </div>

                        <div class="upload-img-hover" onclick="overlayClicked();">
                            <p><i class="fa fa-upload"></i> Click here to Change Image</p>
                        </div>
                        <div id = "profile-image-upload-progress" class = "progress" style = "height: 8px;width: 240px;">
                            <div id = "db-home-main-image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                <span class = "sr-only">0% Complete</span>
                            </div>
                        </div>
                    </div>
                    <div class="team-desc">
                        <div class="team-title"><h4><input id="profile-name" class="input-trans-1" type="text" onchange="changeInfo(this, 'name')" value="{{UserInfo::where("id", Auth::user()->infoid)->pluck('name')}}" style="border: none;" maxlength="16" /></h4></div>
                        <div class="team-content">
                            <p><textarea id="profile-notes" class="team-text-area" placeholder="Enter your description here..." style="width: 80%; height: 200px; border: none;" onchange="changeInfo(this, 'description');" maxlength="520">{{UserInfo::where('id', Auth::user()->infoid)->pluck('description')}}</textarea></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div id="basic-form">
                <form role="form">
                    <div class="form-group">
                        <label for="profile-facebook">Facebook Link</label>
                        <input id="profile-facebook" type="url" onchange="changeInfo(this, 'facebook');" value="{{UserInfo::where('id', Auth::user()->infoid)->pluck('facebook')}}" class="form-control" placeholder="Facebook profile URL" maxlength="128">
                    </div>
                    <div class="form-group">
                        <label for="profile-twitter">Twitter Link</label>
                        <input id="profile-twitter" type="url" onchange="changeInfo(this, 'twitter');" class="form-control" value="{{UserInfo::where('id', Auth::user()->infoid)->pluck('twitter')}}" placeholder="Twitter profile URL" maxlength="128">
                    </div>
                    <div class="form-group">
                        <label for="profile-googleplus">Google Plus Link</label>
                        <input id="profile-googleplus" type="url" onchange="changeInfo(this, 'googleplus');" class="form-control" value="{{UserInfo::where('id', Auth::user()->infoid)->pluck('googleplus')}}" placeholder="Google Plus profile URL" maxlength="128">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/admin/js/custom/my-profile.js')}}
@endsection
