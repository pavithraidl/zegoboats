<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 19:12.
 */
?>

@extends('admin.master')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.css">
    {{HTML::style('assets/admin/libs/summernote/bootstrap.css')}}
    {{HTML::style('assets/admin/libs/summernote/summernote.css')}}
    {{HTML::script('assets/admin/libs/summernote/bootstrap.js')}}
@endsection
@section('content')
    <div class = "page-heading">
        <div class="row" style="margin-right: 20px;">
            <h1 class="pull-left"><i class = 'fa fa-user'></i> Web Site</h1>
        </div>
    </div>
    <?php
        $actionBar = WebsiteVisibility::where('dealerid', Auth::user()->dealerid)->pluck('action_bar');
        $team = WebsiteVisibility::where('dealerid', Auth::user()->dealerid)->pluck('team');
        $testimonials = WebsiteVisibility::where('dealerid', Auth::user()->dealerid)->pluck('testimonials');
        $logoBar = WebsiteVisibility::where('dealerid', Auth::user()->dealerid)->pluck('logo_bar');
    ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel-group accordion-toggle" id="accordiondemo">
                @if(SystemOperationAccess::where('operationid', 1)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" style="color: #19ffe5;">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#main-slider">
                                <i class="fa fa-barcode"></i> Main Slider
                            </a>
                        </h4>
                    </div>
                    <div id="main-slider" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-10 col-md-offset-1">
                                <p class="main-slider-img">
                                <?php
                                    $dirPath = sprintf('%s/assets/images/main_slider/%s.jpg?lastmod=1483062421697', URL::To('/'), Auth::user()->dealerid);
                                    try {
                                        getimagesize($dirPath);
                                    }
                                    catch(Exception $ex) {
                                        $dirPath = URL::To('/').'/assets/images/main_slider/bg-1.jpg';
                                    }
                                ?>
                                    <img id="website-main-slider-img" src="{{$dirPath}}" style="width: 80%; border: 3px solid #a7a7a7;" />
                                    <span id="website-main-image-overlay" onclick="chooseMainImage();"><i class="fa fa-upload"></i> Click Here to Change the Image<br/>Must be 1920x1080 px resolution</span>
                                <div id = "website-image-upload-progress" class = "progress" style = "height: 8px;width: 100%;">
                                    <div id = "website-main-image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                        <span id="website-main-image-upload-sr-only" class = "sr-only">0% Complete</span>
                                    </div>
                                </div>
                                <div style="display: none;">
                                    <form action="{{URL::Route('admin-website-change-main-image')}}" method="post" id="website-change-main-image-form" enctype="multipart/form-data" class="frm-main-image-upload">
                                        <input type="file" name="file" id="website-main-image-choose" onchange="submitForm('#website-change-main-image-form');" accept="image/jpeg, image/jpg" style="display: none;">
                                    </form>
                                </div>
                                </p>
                                <div class="form-group col-md-8 col-md-offset-2" style="margin-top: 10px;">
                                    <input id="website-header-image-title" placeholder="Image Title" class="form-control" onchange="publishValue('#website-header-image-title', 'website_settings', 'header-image-title');" value="{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('header-image-title')}}" type="text" maxlength="16" style="margin-bottom: 5px;" />
                                    <input id="website-header-image-alt" placeholder="Image ALT" class="form-control" onchange="publishValue('#website-header-image-alt', 'website_settings', 'header-image-alt');" value="{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('header-image-alt')}}" type="text" maxlength="16" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 2)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#image-slider">
                                <i class="icon icon-picture-2"></i> Image Slider
                            </a>
                        </h4>
                    </div>
                    <div id="image-slider" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <?php
                                    $imageList = WebsiteImageSlider::where('dealerid', Auth::user()->dealerid)->where('status', 1)->lists('id');
                                ?>
                                <div id="slider-image-container">
                                @foreach($imageList as $imageId)
                                    <div class="col-md-3" id="website-home-slider-{{$imageId}}">
                                        <img src="{{URL::To('/')}}/assets/images/home_slider/{{Auth::user()->dealerid}}/thumb/{{$imageId}}.jpg" />
                                        <span onclick="removeSliderImage({{$imageId}});"><i class="fa fa-times-circle text-red-1"></i></span>
                                        <div class="form-group col-md-12" style="margin-left: -30px; margin-top: 10px;">
                                            <input id="website-image-slider-title-{{$imageId}}" placeholder="Image Title" class="form-control" onchange="publishImageDetails('#website-image-slider-title-{{$imageId}}', 'title', {{$imageId}});" value="{{WebsiteImageSlider::where('id', $imageId)->where('status', '!=', 0)->pluck('title')}}" type="text" maxlength="16" style="margin-bottom: 5px;" />
                                            <input id="website-image-slider-alt-{{$imageId}}" placeholder="Image ALT" class="form-control" onchange="publishImageDetails('#website-image-slider-alt-{{$imageId}}', 'alt', {{$imageId}});" value="{{WebsiteImageSlider::where('id', $imageId)->where('status', '!=', 0)->pluck('alt')}}" type="text" maxlength="16" />
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                                <div class="col-md-3">
                                    <img onclick="openSliderImageFile();" src="{{URL::To('/')}}/assets/admin/img/icons/img-upload.png" />
                                    <div id = "website-image-slider-upload-progress" class = "progress" style = "height: 8px;width: 240px;">
                                        <div id = "db-home-main-image-upload-progress-bar" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                            <span class = "sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <form action="{{URL::Route('admin-website-change-slider-image')}}" method="post" id="website-change-slider-image-form" enctype="multipart/form-data" class="frm-slider-image-upload">
                                            <input type="file" name="file" id="website-slider-image-choose" onchange="submitForm('#website-change-slider-image-form');" accept="image/jpeg, image/jpg" style="display: none;">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 3)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#top-text-block">
                                <i class="fa fa-align-left"></i> Top Text Block
                            </a>
                        </h4>
                    </div>
                    <div id="top-text-block" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="container" id="app">
                                <vue-html-editor :model.sync="text" ></vue-html-editor>
                                <div id="website-text-block-1-holder" style="display: none !important;">@{{text}}</div>
                            </div>
                            <p style="text-align: center; margin-top: 20px;">
                                {{--<button class="btn btn-sm btn-danger" onclick="cancelTextBlockEdit('#website-text-block-1-holder', 1);">Cancel</button>--}}
                                <button class="btn btn-sm btn-success" onclick="textBlockSave('#website-text-block-1-holder', 1);">Publish</button>
                            </p>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 4)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#video">
                                <i class="icon icon-video-2"></i> Video
                            </a>
                        </h4>
                    </div>
                    <div id="video" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-6">
                                <iframe id="website-video" width="560" height="315" src="{{WebsiteVideo::where('dealerid', Auth::user()->dealerid)->pluck('link')}}" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Youtube Link</label>
                                    <input id="website-video-url" type="url" class="form-control" placeholder="https://www.youtube.com/embed/1NKP1i8ltdw" value="{{WebsiteVideo::where('dealerid', Auth::user()->dealerid)->pluck('link')}}">
                                    <p style="text-align: center; margin-top: 20px;">
                                        {{--<button class="btn btn-sm btn-danger">Cancel</button>--}}
                                        <button class="btn btn-sm btn-success" onclick="publishValue('#website-video-url', 'website_video', 'link');">Publish</button>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 40px;">
                                <h4>How to get the youtube link</h4>
                                <p style="text-align: left">
                                    <strong>Step 01:</strong><br/>
                                    Open you video in youtube and click share button bottom of the video.
                                </p>
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <img src="{{URL::To('/')}}/assets/admin/img/guide/youtubelink1.jpg" height="300px;"/>
                                </div><br/><br/>
                                <p>
                                    <strong>Step 02:</strong><br/>
                                    Select Embed tab and carefully copy just the link located inside the (src="copy_this_link_inside_the_quotation_marks") quotation marks and past it here.
                                </p>
                                <div class="col-md-12">
                                    <img src="{{URL::To('/')}}/assets/admin/img/guide/youtubelink2.jpg" height="300px;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 5)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#bottom-text-box">
                                <i class="fa fa-align-left"></i> Bottom Text Box
                            </a>
                        </h4>
                    </div>
                    <div id="bottom-text-box" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="container" id="app2">
                                <vue-html-editor :model.sync="text"></vue-html-editor>
                                <div id="website-text-block-2-holder" style="display: none !important;">@{{text}}</div>
                            </div>
                            <p style="text-align: center; margin-top: 20px;">
                                {{--<button class="btn btn-sm btn-danger">Cancel</button>--}}
                                <button class="btn btn-sm btn-success" onclick="textBlockSave('#website-text-block-2-holder', 2);">Publish</button>
                            </p>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 21)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordiondemo" href="#features">
                                    <i class="fa fa-dashboard"></i> Features
                                </a>
                            </h4>
                        </div>
                        <div id="features" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <p style="text-align: left">
                                            Open <a href="{{URL::Route('admin-icons')}}" target="_blank">icon page</a> and copy past the relevant icon name to the icon field.</a>
                                        </p>
                                    </div>
                                    @for($i = 1; $i < 7; $i++)
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 20px;">
                                            <h4>Features {{$i}}</h4>
                                            <div class="form-group col-md-6">
                                                <input id="website-features-title-{{$i}}" placeholder="Features title here..." class="form-control" onchange="publishFeatures('#website-features-title-{{$i}}', 'title', {{$i}});" value="{{WebsiteFeatures::where('dealerid', Auth::user()->dealerid)->where('column', $i)->pluck('title')}}" type="text"/>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input id="website-features-ico-{{$i}}" placeholder="Copy past the icon name from the icon page Ex:fa-adjust" class="form-control" onchange="publishFeatures('#website-features-ico-{{$i}}', 'icon', {{$i}});" value="{{WebsiteFeatures::where('dealerid', Auth::user()->dealerid)->where('column', $i)->pluck('icon')}}" type="text" />
                                            </div>
                                            <div class="col-md-12">
                                                <textarea class="form-control" id="website-features-description-{{$i}}" onchange="publishFeatures('#website-features-description-{{$i}}', 'description', {{$i}});">{{WebsiteFeatures::where('dealerid', Auth::user()->dealerid)->where('column', $i)->pluck('description')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 7)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#action-bar">
                                <i class="icon icon-phone"></i> Action Bar
                            </a>
                        </h4>
                    </div>
                    <div id="action-bar" class="panel-collapse collapse">
                        <div class="panel-body">
                            <input type = "checkbox" data-id="action_bar" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" @if($actionBar == 1) checked @endif/>
                            <div class="col-md-12" style="margin-top: 20px;">
                                <div class="form-group col-md-12">
                                    <input id="website-action-bar-title" placeholder="Action bar title here..." class="form-control" onchange="publishValue('#website-action-bar-title', 'website_actionbar', 'text');" onblur="requiredValidator(this, 'Please enter action bar title here')" value="{{WebsiteActionbar::where('dealerid', Auth::user()->dealerid)->pluck('text')}}" type="text"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <input id="website-action-bar-email" placeholder="Action bar link here... (email - mailto:someone@host.com, phone - tel:+64556..., link-just copy and paste)" class="form-control" onchange="publishValue('#website-action-bar-email', 'website_actionbar', 'link');" onblur="requiredValidator(this, 'Please fill the action bar link field');" value="{{WebsiteActionbar::where('dealerid', Auth::user()->dealerid)->pluck('link')}}" type="text" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 8)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#team">
                                <i class="icon icon-vcard"></i> Team
                            </a>
                        </h4>
                    </div>
                    <div id="team" class="panel-collapse collapse">
                        <div class="panel-body">
                            <input type = "checkbox" data-id="team" class = "ios-switch ios-switch-success ios-switch-sm pull-right" @if($team == 1) checked @endif/>
                            <div class="col-md-12">
                                <?php
                                    $teamList = User::where('dealerid', Auth::user()->dealerid)->where('status', '!=', 0)->lists('id');
                                ?>
                                @foreach($teamList as $teamId)
                                <div class="col-md-6">
                                    <div class="oc-item">
                                        <div class="team team-list">
                                            <div class="team-image">
                                                <?php
                                                $dirPath = URL::To('/').'/assets/images/team/'.$teamId.'.jpg';
                                                $name = UserInfo::where('id', User::where('id', $teamId)->pluck('infoid'))->pluck('name');
                                                $jobTitle = UserInfo::where('id', User::where('id', $teamId)->pluck('infoid'))->pluck('jobtitle');
                                                try {
                                                    getimagesize($dirPath);
                                                }
                                                catch(Exception $ex) {
                                                    $dirPath = URL::To('/').'/assets/images/team/0.jpg';
                                                }
                                                ?>
                                                <img id="website-profile-image-{{$teamId}}" src="{{$dirPath}}" alt="Zego Boats Team - {{$name}}" />
                                                <div class="upload-img-hover" onclick="chooseProfileImage({{$teamId}});">
                                                    <p><i class="fa fa-upload"></i> Click here to Change Image</p>
                                                </div>
                                                    <div id = "profile-image-upload-progress-{{$teamId}}" class = "progress-{{$teamId}}" style = "height: 8px;width: 240px;">
                                                        <div id = "profile-image-upload-progress-bar-{{$teamId}}" class = "progress-bar progress-bar-striped active" role = "progressbar" aria-valuenow = "45" aria-valuemin = "0" aria-valuemax = "100" style = "width: 0%;">
                                                            <span class = "sr-only">0% Complete</span>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;">
                                                        <form action="{{URL::Route('admin-website-profile-change-image')}}" method="post" id="profile-image-upload-{{$teamId}}" enctype="multipart/form-data" class="frm-profile-image-upload">
                                                            <input type="file" name="file" id="profile-image-choose-{{$teamId}}" onchange="submitTeamForm('#profile-image-upload-{{$teamId}}', {{$teamId}});" accept="image/jpeg, image/jpg" style="display: none;">
                                                            <input type="hidden" name="userid" value="{{$teamId}}" />
                                                        </form>
                                                    </div>
                                            </div>
                                            <div class="team-desc">
                                                <input type = "checkbox" data-id="tm-{{$teamId}}" class = "ios-switch ios-switch-warning ios-switch-sm pull-right" @if(UserInfo::where('id', User::where('id', $teamId)->pluck('infoid'))->pluck('visible')) checked @endif/>
                                                <div class="team-title"><h4><input id="website-team-name" onchange="publishTeamInfo('#website-team-name', 'name', {{$teamId}})" placeholder="Member Name..." type="text" value="{{$name}}" class="form-control" style="border: none; margin-top: 10px; box-shadow: none;" /> </h4>
                                                    <span><input id="website-team-job-title" placeholder="Job Title..." type="text" onchange="publishTeamInfo('#website-team-job-title', 'jobtitle', {{$teamId}})" value="{{$jobTitle}}" class="form-control" style="border: none; margin-top: 10px; box-shadow: none; color: #08ba9d; margin-top: -10px;" /></span>
                                                </div>
                                                <div class="team-content">
                                                    <p><textarea id="website-team-description" onchange="publishTeamInfo('#website-team-description', 'description', {{$teamId}})" class="form-control" style="border: none; box-shadow: none; min-height: 200px;" placeholder="Member description here...">{{UserInfo::where('id', User::where('id', $teamId)->pluck('infoid'))->pluck('description')}}</textarea> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 9)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#testimonials">
                                <i class="icon icon-article-alt"></i> Testimonials
                            </a>
                        </h4>
                    </div>
                    <div id="testimonials" class="panel-collapse collapse">
                        <div class="panel-body">
                            <input type = "checkbox" data-id="testimonials" class = "ios-switch ios-switch-success ios-switch-sm pull-right" @if($testimonials == 1) checked @endif/>
                            @for($i = 1; $i < 7; $i++)
                            <div class="col-md-12" style="margin-top: 20px;">
                                <h4>Testimonial Quote {{$i}}:</h4>
                                <div class="form-group col-md-12">
                                    <?php
                                        $text = WebsiteTestimonials::where('dealerid', Auth::user()->dealerid)->where('row', $i)->pluck('text');
                                        $name = WebsiteTestimonials::where('dealerid', Auth::user()->dealerid)->where('row', $i)->pluck('name');
                                        $position = WebsiteTestimonials::where('dealerid', Auth::user()->dealerid)->where('row', $i)->pluck('position');
                                    ?>
                                    <input id="website-testimonials-quote-{{$i}}" placeholder="Testimonial Quote..." class="form-control" onchange="publishTestimonials('#website-testimonials-quote-{{$i}}', 'text', {{$i}});" value='{{$text}}' type="text"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="website-testimonials-name-{{$i}}" placeholder="Client Name..." class="form-control" onchange="publishTestimonials('#website-testimonials-name-{{$i}}', 'name', {{$i}});" value="{{$name}}" type="text" />
                                </div>
                                <div class="form-group col-md-6">
                                    <input id="website-testimonials-position-{{$i}}" placeholder="Client Position..." class="form-control" onchange="publishTestimonials('#website-testimonials-position-{{$i}}', 'position', {{$i}});" value="{{$position}}" type="text" />
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 6)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#bottom-logo">
                                <i class="icon icon-picture-2"></i> Bottom Logo Bar
                            </a>
                        </h4>
                    </div>
                    <div id="bottom-logo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <input type = "checkbox" data-id="logo_bar" class = "ios-switch ios-switch-success ios-switch-sm pull-right" @if($logoBar == 1) checked @endif/>
                            <div class="col-md-12">
                                <?php
                                $imageList = WebsiteBottomLogo::where('dealerid', Auth::user()->dealerid)->where('status', 1)->lists('id');
                                ?>
                                <div id="logo-image-container">
                                    @foreach($imageList as $imageId)
                                        <div class="col-md-2" id="website-logo-{{$imageId}}">
                                            <img src="{{URL::To('/')}}/assets/images/logos/{{Auth::user()->dealerid}}/{{$imageId}}.jpg" style="width: 100px; height: 80px; border: 2px solid #797979"/>
                                            <span onclick="removeLogo({{$imageId}});"><i class="fa fa-times-circle text-red-1"></i></span>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-2">
                                    <img onclick="openLogoImageFile();" src="{{URL::To('/')}}/assets/admin/img/icons/img-upload.png"  style="width: 100px; height: 80px; border: 2px solid #797979"/>
                                    <div style="display: none;">
                                        <form action="{{URL::Route('admin-website-change-logo-image')}}" method="post" id="website-change-logo-image-form" enctype="multipart/form-data" class="frm-logo-image-upload">
                                            <input type="file" name="file" id="website-logo-image-choose" onchange="submitForm('#website-change-logo-image-form');" accept="image/jpeg, image/jpg, image/png" style="display: none;">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(SystemOperationAccess::where('operationid', 10)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#footer">
                                <i class="icon icon-minus-1"></i> Other Settings
                            </a>
                        </h4>
                    </div>
                    <div id="footer" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <h4>Footer Quote</h4>
                                <div class="form-group col-md-12">
                                    <label for="website-footer-quote">Quote</label>
                                    <input id="website-footer-quote" placeholder="Enter quote here..." class="form-control" onchange="publishValue('#website-footer-quote', 'website_footer', 'quote');" onblur="requiredValidator(this, 'Please enter the footer quote here...')" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('quote')}}" type="text" maxlength="200"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Contact</h4>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-phone">Phone</label>
                                    <input id="website-footer-phone" placeholder="Enter phone number..." class="form-control" onchange="publishValue('#website-footer-phone', 'website_footer', 'phone');" onblur="requiredValidator(this, 'Please enter the phone number here...')" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('phone')}}" type="text" maxlength="24"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-fax">Fax</label>
                                    <input id="website-footer-fax" placeholder="Enter fax number..." class="form-control" onchange="publishValue('#website-footer-fax', 'website_footer', 'fax');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('fax')}}" type="text" maxlength="24"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Social Media</h4>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-facebook">Facebook</label>
                                    <input id="website-footer-facebook" placeholder="Enter the facebook link here..." class="form-control" onchange="publishValue('#website-footer-facebook', 'website_footer', 'facebook');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('facebook')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-twitter">Twitter</label>
                                    <input id="website-footer-twitter" placeholder="Enter the twitter link here..." class="form-control" onchange="publishValue('#website-footer-twitter', 'website_footer', 'twitter');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('twitter')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-googleplus">Google Plus</label>
                                    <input id="website-footer-googleplus" placeholder="Enter the google plus link here..." class="form-control" onchange="publishValue('#website-footer-googleplus', 'website_footer', 'googleplus');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('googleplus')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-instagram">Instagram</label>
                                    <input id="website-footer-instagram" placeholder="Enter the instagram link here..." class="form-control" onchange="publishValue('#website-footer-instagram', 'website_footer', 'instagram');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('instagram')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-youtube">Youtube</label>
                                    <input id="website-footer-youtube" placeholder="Enter the youtube channel link here..." class="form-control" onchange="publishValue('#website-footer-youtube', 'website_footer', 'youtube');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('youtube')}}" type="text" maxlength="128" />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h4>Communication</h4>
                                <div class="form-group col-md-6">
                                    <label for="website-footer-email">Enquiry Receiving Email:</label>
                                    <input id="website-footer-email" placeholder="Enter the enquiry receiving email here..." class="form-control" onchange="publishValue('#website-footer-email', 'website_footer', 'email');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('email')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="website-footer-response">Enquiry Response:</label>
                                    <input id="website-footer-response" placeholder="Type here the enquiry auto response message to the client..." class="form-control" onchange="publishValue('#website-footer-response', 'website_footer', 'response');" value="{{WebsiteFooter::where('dealerid', Auth::user()->dealerid)->pluck('response')}}" type="text" maxlength="128"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(SystemOperationAccess::where('operationid', 20)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#seo">
                                <i class="icon icon-keyboard"></i> Seo Settings
                            </a>
                        </h4>
                    </div>
                    <div id="seo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <h4>SEO Settings</h4>
                                <div class="form-group col-md-6">
                                    <label for="website-seo-author">Author</label>
                                    <input id="website-seo-author" placeholder="Enter meta author tag field..." class="form-control" onchange="publishValue('#website-seo-author', 'website_settings', 'author');" onblur="requiredValidator(this, 'Please enter the meta author here...')" value="{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('author')}}" type="text" maxlength="16"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="website-seo-keywords">Meta Keywords</label>
                                    <input id="website-seo-keywords" placeholder="KEYWORD1 KEYWORD2 KEYPHRASE1 etc. about 30 to 40 unique words" class="form-control" onchange="publishValue('#website-seo-keywords', 'website_settings', 'meta_keywords');" onblur="requiredValidator(this, 'Please enter the meta keywords here...')" value="{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('meta_keywords')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="website-seo-description">Meta Description</label>
                                    <input id="website-seo-description" placeholder="An accurate, keyword-rich description about 150 characters" class="form-control" onchange="publishValue('#website-seo-description', 'website_settings', 'meta_description');" onblur="requiredValidator(this, 'Please enter the meta description here...')" value="{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('meta_description')}}" type="text" maxlength="128"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="website-seo-ga-script">Google Analytics Scripts</label>
                                    <textarea id="website-seo-ga-script" class="form-control" onchange="publishValue('#website-seo-ga-script', 'website_settings', 'ga_script');" rows="10" placeholder="Place the google analytics scripts here..." maxlength="4000">{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('ga_script')}} </textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="website-seo-other-script">Other Scripts</label>
                                    <textarea id="website-seo-other-script" class="form-control" onchange="publishValue('#website-seo-other-script', 'website_settings', 'other_scripts');" rows="15" placeholder="Place scripts here..." maxlength="4000">{{WebsiteSettings::where('dealerid', Auth::user()->dealerid)->pluck('other_scripts')}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <?php
    $id = WebsiteImageSlider::where('status', '!=', 10)->max('id');
    $id = $id+1;
    $logoId = WebsiteBottomLogo::where('status', '!=', 10)->max('id');
    $logoId = $logoId+1;
    ?>
    <input id="website-last-image-id" type="hidden" value="{{$id}}" />
    <input id="website-last-logo-id" type="hidden" value="{{$logoId}}" />
    <input id="website-dealerid" type="hidden" value="{{Auth::user()->dealerid}}" />
    <input id="website-text-block-1" type="hidden" value='{{WebsiteTextBlocks::where('text_block', 1)->where('dealerid', Auth::user()->dealerid)->pluck('content')}}' />
    <input id="website-text-block-2" type="hidden" value='{{WebsiteTextBlocks::where('text_block', 2)->where('dealerid', Auth::user()->dealerid)->pluck('content')}}' />
    <script>
        window.textBox1 = $('#website-text-block-1').val();
        window.textBox2 = $('#website-text-block-2').val();
    </script>
@endsection

@section('scripts')
    {{HTML::script('assets/admin/libs/summernote/summernote.js')}}
    {{HTML::script('assets/admin/libs/text-editor/editor.js')}}
    {{HTML::script('assets/admin/js/custom/website.js')}}
@endsection
