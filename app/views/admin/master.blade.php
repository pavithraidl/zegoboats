<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 10/12/16 09:41.
 */
?>
        <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dashboard | Zego Boats</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="Zego Boats Admin">
    <meta name="author" content="Pavithra Isuru">

    <!-- Base Css Files -->
{{HTML::style('assets/admin/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css')}}
{{HTML::style('assets/admin/libs/bootstrap/css/bootstrap.min.css')}}
{{HTML::style('assets/admin/libs/font-awesome/css/font-awesome.min.css')}}
{{HTML::style('assets/admin/libs/fontello/css/fontello.css')}}
{{HTML::style('assets/admin/libs/animate-css/animate.min.css')}}
{{HTML::style('assets/admin/libs/nifty-modal/css/component.css')}}
{{HTML::style('assets/admin/libs/magnific-popup/magnific-popup.css')}}
{{HTML::style('assets/admin/libs/ios7-switch/ios7-switch.css')}}
{{HTML::style('assets/admin/libs/pace/pace.css')}}
{{HTML::style('assets/admin/libs/sortable/sortable-theme-bootstrap.css')}}
{{HTML::style('assets/admin/libs/bootstrap-datepicker/css/datepicker.css')}}
{{HTML::style('assets/admin/libs/jquery-icheck/skins/all.css')}}
<!-- Code Highlighter for Demo -->
{{HTML::style('assets/admin/libs/prettify/github.css')}}

<!-- Extra CSS Libraries Start -->
{{HTML::style('assets/admin/libs/morrischart/morris.css')}}
{{ HTML::style('assets/admin/libs/jquery-notifyjs/styles/metro/notify-metro.css') }}
{{HTML::style('assets/admin/css/style.css')}}
    @yield('styles')
<!-- Extra CSS Libraries End -->
{{HTML::style('assets/admin/css/style-responsive.css')}}

{{HTML::script('/assets/admin/libs/jquery/jquery-1.11.1.min.js')}}
{{ HTML::script('assets/admin/js/pages/jquery.form.js')}}
{{HTML::script('assets/admin/js/custom/image-uploader-ajax.js')}}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/admin/img/favicon.ico">
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-152x152.png" />
</head>
<body class="fixed-left">
<!-- Modal Start -->
<!-- Modal Logout -->
<div class="md-modal md-just-me" id="logout-modal">
    <div class="md-content">
        <h3><strong>Logout</strong> Confirmation</h3>
        <div>
            <p class="text-center">Are you sure want to logout from this awesome system?</p>
            <p class="text-center">
                <button class="btn btn-danger md-close">Nope!</button>
                <a href="{{URL::Route('log-out')}}" class="btn btn-success md-close">Yeah, I'm sure</a>
            </p>
        </div>
    </div>
</div>
<!-- Begin page -->
<div id="wrapper" style="background-color: #FFFFFF;">
    <?php
        $profilePic = URL::To('/').'/assets/images/team/'.Auth::user()->id.'.jpg';
        $name = UserInfo::where('id', Auth::user()->infoid)->pluck('name');
        $arr = explode(' ',trim($name));
            try {
                $fName = $arr[0];
            }catch(Exception $ex) {
                $fName = '';
            }
        try {
            $lName = $arr[1];
        }catch(Exception $ex) {
            $lName = '';
        }

        try {
            getimagesize($profilePic);
        }
        catch(Exception $ex) {
            $profilePic = URL::To('/').'/assets/admin/img/users/m/user-35.jpg';
        }
    ?>
    <!-- Top Bar Start -->
    <div class="topbar">
        <div class="topbar-left">
            <div class="logo">
                <h1><a href="#"><img src="{{URL::To('/')}}/assets/admin/img/logo.png" alt="Logo" style="width: 135px; height: auto;"></a></h1>
            </div>
            <button class="button-menu-mobile open-left">
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-collapse2">
                    <ul class="nav navbar-nav navbar-right top-navbar">
                        <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a></li>
                        <li class="dropdown topbar-profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="rounded-image topbar-profile-image"><img src="{{$profilePic}}"></span> {{$fName}} <strong>{{$lName}}</strong> <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">My Profile</a></li>
                                <li><a href="#">Change Password</a></li>
                                <li><a href="#">Account Setting</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-help-2"></i> Help</a></li>
                                <li><a href="lockscreen.html"><i class="icon-lock-1"></i> Lock me</a></li>
                                <li><a class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Left Sidebar Start -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!-- Search form -->
            <form role="search" class="navbar-form">

            </form>
            <div class="clearfix"></div>
            <!--- Profile -->
            <div class="profile-info">
                <div class="col-xs-4">
                    <a href="profile.html" class="rounded-image profile-image"><img src="{{$profilePic}}"></a>
                </div>
                <div class="col-xs-8">
                    <div class="profile-text">Welcome <b>{{$fName}}</b></div>
                    <div class="profile-buttons">
                        <a href="{{URL::Route('log-out')}}" title="Sign Out"><i class="fa fa-power-off text-red-1"></i></a>
                    </div>
                </div>
            </div>
            <!--- Divider -->
            <div class="clearfix"></div>
            <hr class="divider" />
            <div class="clearfix"></div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <ul>
                    <?php
                        $systemList = System::where('status', 1)->where('visibility', 1)->lists('id');
                        $otherList = System::where('status', 1)->where('visibility', 2)->lists('id');
                    ?>
                    @foreach($systemList as $systemId)
                        @if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                            <li>
                                <a href='{{URL::Route(System::where('id', $systemId)->pluck('route'))}}'><i class='{{System::where('id', $systemId)->pluck('icon')}}'></i><span>{{System::where('id', $systemId)->pluck('name')}}</span></a>
                            </li>
                        @endif
                    @endforeach
                    @foreach($otherList as $otherId)
                        @if(Auth::user()->roll == 1)
                            <li>
                                <a href='{{URL::Route(System::where('id', $otherId)->pluck('route'))}}'><i class='{{System::where('id', $otherId)->pluck('icon')}}'></i><span>{{System::where('id', $otherId)->pluck('name')}}</span></a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- Left Sidebar End -->
    <!-- Start right content -->
    <div class="content-page">
        <!-- ============================================================== -->
        <!-- Start Content here -->
        <!-- ============================================================== -->
        <div class="content" style="background-color: #FFFFFF">

            @yield('content')

            <!-- Footer Start -->

        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->
    </div>
    <!-- End right content -->
    <footer>
        IDL Creative &copy; {{date('Y')}}
        <div class="footer-links pull-right">
            <a href="#">About</a><a href="#">Support</a><a href="#">Terms of Service</a><a href="#">Legal</a><a href="#">Help</a><a href="#">Contact Us</a>
        </div>
    </footer>
    <!-- Footer End -->
</div>
<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{{HTML::script('/assets/admin/libs/bootstrap/js/bootstrap.min.js')}}
{{HTML::script('/assets/admin/libs/jqueryui/jquery-ui-1.10.4.custom.min.js')}}
{{HTML::script('/assets/admin/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js')}}
{{HTML::script('/assets/admin/libs/jquery-detectmobile/detect.js')}}
{{HTML::script('/assets/admin/libs/jquery-animate-numbers/jquery.animateNumbers.js')}}
{{HTML::script('/assets/admin/libs/ios7-switch/ios7.switch.js')}}
{{HTML::script('/assets/admin/libs/fastclick/fastclick.js')}}
{{HTML::script('/assets/admin/libs/jquery-blockui/jquery.blockUI.js')}}
{{HTML::script('/assets/admin/libs/bootstrap-bootbox/bootbox.min.js')}}
{{HTML::script('/assets/admin/libs/jquery-slimscroll/jquery.slimscroll.js')}}
{{HTML::script('/assets/admin/libs/jquery-sparkline/jquery-sparkline.js')}}
{{HTML::script('/assets/admin/libs/nifty-modal/js/classie.js')}}
{{HTML::script('/assets/admin/libs/nifty-modal/js/modalEffects.js')}}
{{HTML::script('/assets/admin/libs/sortable/sortable.min.js')}}
{{HTML::script('/assets/admin/libs/bootstrap-fileinput/bootstrap.file-input.js')}}
{{HTML::script('/assets/admin/libs/bootstrap-select/bootstrap-select.min.js')}}
{{HTML::script('/assets/admin/libs/bootstrap-select2/select2.min.js')}}
{{HTML::script('/assets/admin/libs/magnific-popup/jquery.magnific-popup.min.js')}}
{{HTML::script('/assets/admin/libs/pace/pace.min.js')}}
{{HTML::script('/assets/admin/libs/bootstrap-datepicker/js/bootstrap-datepicker.js')}}
{{HTML::script('/assets/admin/libs/jquery-icheck/icheck.min.js')}}

<!-- Demo Specific JS Libraries -->
{{HTML::script('/assets/admin/libs/prettify/prettify.js')}}

{{HTML::script('/assets/admin/js/init.js')}}
{{ HTML::script('assets/admin/libs/jquery-notifyjs/notify.min.js') }}
{{ HTML::script('assets/admin/libs/jquery-notifyjs/styles/metro/notify-metro.js') }}
{{HTML::script('/assets/admin/js/pages/notifications.js')}}
<!-- Page Specific JS Libraries -->
{{HTML::script('/assets/admin/libs/d3/d3.v3.js')}}
{{HTML::script('/assets/admin/libs/morrischart/morris.min.js')}}
{{HTML::script('/assets/admin/libs/jstimezone/jstz.min.js')}}
{{HTML::script('/assets/admin/js/custom/constant.js')}}
{{HTML::script('/assets/admin/js/custom/public.functions.js')}}
{{HTML::script('/assets/admin/js/custom/validator.js')}}
@yield('scripts')
</body>
</html>
