<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login | Zego Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="Zego dsahboard login">
    <meta name="author" content="Pavithra Isuru">

    <!-- Base Css Files -->
    {{HTML::style('/assets/admin/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css')}}
    {{HTML::style('/assets/admin/libs/bootstrap/css/bootstrap.min.css')}}
    {{HTML::style('/assets/admin/libs/font-awesome/css/font-awesome.min.css')}}
    {{HTML::style('/assets/admin/libs/animate-css/animate.min.css')}}
    <!-- Code Highlighter for Demo -->

    <!-- Extra CSS Libraries Start -->
    {{HTML::style('/assets/admin/css/style.css')}}
    <!-- Extra CSS Libraries End -->
    {{HTML::style('/assets/admin/css/style-responsive.css')}}

    <link rel="shortcut icon" href="{{URL::To('/')}}/assets/admin/img/favicon.ico">
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::To('/')}}/assets/admin/img/apple-touch-icon-152x152.png" />
</head>
<body class="fixed-left login-page">
<!-- Begin page -->
<div class="container">
    <div class="full-content-center">
        <p class="text-center"><a href="#"><img src="{{URL::To('/')}}/assets/admin/img/logo.png" style="width: 150px; height: auto;;" alt="Logo"></a></p>
        <div class="login-wrap animated flipInX">
            <form role="form" action="{{ URL::route('admin-post-login') }}" method="post" id="registerForm">
                <div class="login-block">
                    @if( Session::has('global') )
                        <p style="color:@if($errors->has('email') || Session::has('global') && Session::get('global') != 'Activated! you can now sign in' ) red @else #00ca6d @endif">* {{ Session::get('global') }}</p>
                    @endif
                    <form role="form" action="index.html">
                        <div class="form-group login-input">
                            <i class="fa fa-user overlay"></i>
                            <input id="email" name="email" type="email" class="form-control text-input" placeholder="Email" required="true">
                            @if($errors->has('email'))
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $emailerror=$errors->first('email'); }}</label>
                            @endif
                        </div>
                        <div class="form-group login-input">
                            <i class="fa fa-key overlay"></i>
                            <input id="password" name="password"  type="password" class="form-control text-input" placeholder="********" required="true">
                            @if($errors->has('password'))
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $emailerror=$errors->first('password'); }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember" id="remember"/>
                            <label for="remember">Remember me</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-push-3">
                                <button type="submit" id="register" class="btn btn-success btn-block">LOGIN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
</body>
</html>