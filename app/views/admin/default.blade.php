<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 10/12/16 09:41.
 */
?>

@extends('admin.master')

@section('content')
    <!-- Start info box -->
    <div class="row top-summary">
        <div class="col-lg-3 col-md-6">
            <div class="widget green-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon-globe-inv"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOTAL <b>VISITORS</b></p>
                        <h2><span class="animate-number" data-value="5153" data-duration="2000">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-caret-up rel-change"></i> <b>9%</b> more in average
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget darkblue-2 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="fa fa-suitcase"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">UNIQUE <b>VISITORS</b></p>
                        <h2><span class="animate-number" data-value="2399" data-duration="1000">0</span></h2>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-caret-down rel-change text-red-1"></i> <b>11%</b> less in average
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget orange-4 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">RETURNING <b>VISITORS</b></p>
                        <h2>$<span class="animate-number" data-value="2754" data-duration="1000">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-caret-up rel-change"></i> <b>17%</b> more in average
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="widget lightblue-1 animated fadeInDown">
                <div class="widget-content padding">
                    <div class="widget-icon">
                        <i class="icon icon-website"></i>
                    </div>
                    <div class="text-box">
                        <p class="maindata">TOP <b>WEBSITE</b></p>
                        <h2><span class="animate-number" data-value="648" data-duration="500">0</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-caret-up rel-change"></i> <b>6%</b> increase in users
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>
    <!-- End of info box -->

    <div class="row">
        <div class="col-lg-8 portlets">
            <div id="website-statistics1" class="widget">
                <div class="widget-header transparent">
                    <h2><i class="icon-chart-line"></i> <strong>Website</strong> Statistics</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                        <a class="hidden" id="dropdownMenu1" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                        <a href="#" class="widget-popout hidden tt" title="Pop Out/In"><i class="icon-publish"></i></a>
                        <a href="#" class="widget-maximize hidden"><i class="icon-resize-full-1"></i></a>
                        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="website-statistic" class="statistic-chart">
                        <div class="row stacked">
                            <div class="col-sm-12">
                                <div class="toolbar">
                                    <div class="pull-left">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-default btn-xs">Daily</a>
                                            <a href="#" class="btn btn-default btn-xs active">Monthly</a>
                                            <a href="#" class="btn btn-default btn-xs">Yearly</a>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <a class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                                Export <i class="icon-down-open-2"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="#">Export as PDF</a></li>
                                                <li><a href="#">Export as CSV</a></li>
                                                <li><a href="#">Export as PNG</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="icon-cog-2"></i></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div id="morris-home" class="morris-chart" style="height: 270px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-lg-4 portlets">
            <div class="widget darkblue-3">
                <div class="widget-header transparent">
                    <h2><strong>Server</strong> Status</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                        <a class="hidden" id="dropdownMenu1" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                        <a href="#" class="widget-popout hidden tt" title="Pop Out/In"><i class="icon-publish"></i></a>
                        <a href="#" class="widget-maximize hidden"><i class="icon-resize-full-1"></i></a>
                        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div id="website-statistic2" class="statistic-chart">

                        <div class="col-sm-12 stacked">
                            <h4><i class="fa fa-circle-o text-green-1"></i> Server Loads</h4>
                            <div class="col-sm-8 status-data">

                                <div class="col-xs-12">
                                    <div class="row stacked">
                                        <div class="col-xs-4 text-center right-border">
                                            Processes<br>
                                            <span class="animate-number" data-value="322" data-duration="3000">0</span>
                                        </div>
                                        <div class="col-xs-4 text-center right-border">
                                            Connections<br>
                                            <span class="animate-number" data-value="4789" data-duration="3000">0</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            Avg. Load<br>
                                            <span class="animate-number" data-value="76" data-duration="3000">0</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="progress progress-xs">
                                    <div style="width: 72%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="72" role="progressbar" class="progress-bar bg-orange-2" title="Average Load: 76%" data-placement="right" data-toggle="tooltip">
                                        <span class="sr-only">72% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 text-center">
                                <div class="ws-load echart" data-percent="50"><span class="percent"></span></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="home-chart-2"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="morris-bar-home"class="morris-chart" style="height: 170px; display: none;"></div>
    <div id="home-chart-3" style="visibility: hidden;"></div>
@endsection

@section('scripts')
    {{HTML::script('/assets/admin/js/pages/index.js')}}
@endsection
