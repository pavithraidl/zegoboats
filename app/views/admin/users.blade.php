<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 17:57.
 */
?>

@extends('admin.master')

@section('content')
    @include('admin.includes.new-user')
    <div class = "page-heading">
        <div class="row" style="margin-right: 20px;">
            <h1 class="pull-left"><i class = 'fa icon-user-2'></i> User Manage</h1>
            @if(SystemOperationAccess::where('operationid', 15)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
            <button class="pull-right btn btn-blue-3 btn-sm md-trigger" style="margin-top: 20px;" data-modal="mm-new-user"><i class="fa fa-plus-circle"></i> Add User</button>
            @endif
        </div>
    </div>
    <div class = "page-content">
        <div class = "col-md-12">
            <div class="widget" id="user-manage-widget">
                <div class="widget-content padding">
                    <div class = "col-md-3 bs-callout bs-callout-info" style = "border-radius: 10px; margin-top: -10px;">
                        <div class = "row">
                            <div class = "col-md-12">
                                <div class = "form-group form-search search-box has-feedback">
                                    <span id = "searchSpan">
                                        <input type = "text" class = "form-control full-rounded" onkeyup = "searchFilter(this);" id = "searchText" ng-model = "searchText" placeholder = "Search..">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id = "o-m-org-list-container" class = "row" style = "margin-top: 20px;">
                            <ul id = "user-list-container" class = "search-filter" style = "list-style-type: none; margin-left: -30px;">
                                {{--jQuery append--}}
                            </ul>
                        </div>
                    </div>
                    <div class = "col-md-3" style = "padding: 5px;">
                        <div class = " shadow-pane-1" style = "margin-top: -10px;">
                            <div class = "row">
                                <div class = "col-md-12">
                                    <div data-toggle = "tooltip" title = "Active/Deactive Dealer" style = "visibility: visible; position: absolute;">
                                        <input data-id = "user-status" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" checked hidden = "hidden"/>
                                    </div>

                                    <p style = "text-align: center;">
                                        <img id="user-image" class="img-circle" src="{{URL::To('/')}}/assets/admin/img/users/m/user-100.jpg" style="width: 100px; height: 100px;" />
                                    </p>
                                </div>
                            </div>
                            <div class = "row" style = "margin-top: -10px;">
                                <p style = "text-align: center; font-weight: 600; color: rgba(0, 0, 0, 0.30)" id = "user-job-title">Null</p>
                            </div>
                            <div class = "row">
                                <h4 class = "text-blue-3" style = "text-align: center; font-size: 18px;">
                                    <strong id = "user-name">NULL</strong>
                                </h4>
                            </div>
                            <div class = "row">
                                <ul style = "list-style-type: none; margin-left: -20px;margin-top: 20px;">
                                    <li style = "margin-top: 5px;">
                                        <a href = "#">
                                            <i id = "user-status-icon" class = "fa fa-bookmark text-green-2"></i>
                                            &nbsp;<span id = "user-status-text">Active</span></a>
                                    </li>
                                    <li style = "margin-top: 5px;">
                                        <a id="user-email" href = "#">
                                            <i id = "o-m-account-active-status-icon" class = "fa fa-envelope text-yellow-1"></i>
                                            &nbsp;<span id = "o-m-account-active-status-text">Email</span></a>
                                    </li>
                                    <li style = "margin-top: 15px;">
                                        <button id ="user-resend-activation-email" onclick="resendActivationEmail();" class = "btn btn-default" style = "width: 90%;text-align: center; display: none;">
                                            <i class = "fa fa-envelope"></i> Resend Activation Email
                                        </button>
                                        @if(SystemOperationAccess::where('operationid', 18)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                                        <button id = "user-delete-user" onclick="deleteUser();" class = "btn btn-danger" style = "width: 90%;text-align: center; visibility: visible; margin-top: 10px; display: none;">
                                            <i class = "fa fa-trash"></i> Remove User
                                        </button>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class = "col-md-6">
                        <ul id = "o-m-org-manage-container" class = "nav nav-tabs nav-simple">
                            <li id="user-tab-settings">
                                <a href="#user-tab-settings-container" data-toggle="tab"><i class="fa fa-wrench"></i> Settings</a>
                            </li>
                            @if(SystemOperationAccess::where('operationid', 16)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <li  id="user-tab-access">
                                <a href = "#user-tab-access-container" data-toggle = "tab"><i class = "icon-lock-alt text-orange-3"></i>
                                    Access</a>
                            </li>
                            @endif
                            @if(SystemOperationAccess::where('operationid', 17)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <li id = "user-tab-activity-log" class = "active">
                                <a href = "#user-tab-activity-log-container" data-toggle = "tab"><i class = "fa fa-bullhorn text-green-3"></i>
                                    Activity Log</a>
                            </li>
                            @endif
                        </ul>

                        <div class = "tab-content" style = "padding: 20px;">
                            <div class = "tab-pane fade" id = "user-tab-settings-container" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                <div class="form-group">
                                    <label for="user-email">User Email:</label>
                                    <input id = "user-settings-email" type = "email" class = "form-control" placeholder = "someone&#64;example.com" onchange="saveEmail();"  maxlength = "60" required tabindex="7">
                                </div>
                            </div>
                            @if(SystemOperationAccess::where('operationid', 16)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <!-- / .tab-pane Access-->
                            <div class = "tab-pane fade"  id = "user-tab-access-container">
                                <div id="o-m-access-control-container" class = "row">
                                    <div class="panel-group accordion-toggle" id="accordiondemo">
                                        <?php
                                        $systemList = System::where('status', 1)->where('visibility', 1)->lists('id');
                                        ?>
                                        @foreach($systemList as $systemId)
                                            @if(SystemRoutingAccess::where('systemid', $systemId)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3 || System::where('id', $systemId)->where('allow_default', 1)->pluck('id'))
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordiondemo" href="#accordion{{$systemId}}">
                                                                <h4 style="margin-top: 0; margin-bottom: 5px;" id="o-m-text-container-1" onclick="">
                                                                    <i class = "{{System::where('id', $systemId)->pluck('icon')}}"></i> {{System::where('id', $systemId)->pluck('name')}}
                                                                </h4>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="accordion{{$systemId}}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <ul style="list-style-type: none;">
                                                                <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px; font-size: 18px; font-weight: 600;">
                                                                    <div class="col-md-7" id="user-operation-text-container-{{$systemId}}">
                                                                        <i class="{{System::where('id', $systemId)->pluck('icon')}}"></i> &nbsp;Access {{System::where('id', $systemId)->pluck('name')}}
                                                                    </div>
                                                                    <div class="col-md-4" style="cursor: pointer">
                                                                        <input id = "user-access-system-operation-{{$systemId}}" data-id = "sa-sy-{{$systemId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-md pull-left" checked/>
                                                                    </div>
                                                                </li>
                                                                <hr>
                                                                <?php $systemOperationList = SystemOperation::where('status', 1)->where('systemid', $systemId)->lists('id'); ?>
                                                                @foreach($systemOperationList as $operationId)
                                                                    <li class="hover-hili-1" style="padding-left: 20px !important; margin-left: -50px; min-height: 35px;">
                                                                        <div class="col-md-7" id="user-operation-text-container-{{$operationId}}">
                                                                            <i class="{{SystemOperation::where('id', $operationId)->pluck('icon')}}"></i> &nbsp;{{SystemOperation::where('id', $operationId)->pluck('operation')}}
                                                                        </div>
                                                                        <div class="col-md-4" style="cursor: pointer">
                                                                            <input id = "user-access-system-operation-{{$operationId}}" data-id = "sa-op-{{$operationId}}" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-left" checked/>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(SystemOperationAccess::where('operationid', 17)->where('userid', Auth::user()->id)->where('allow', 1)->pluck('id') || Auth::user()->roll < 3)
                            <!-- / .tab-pane Activity Log-->
                            <div class = "tab-pane fade" id = "user-tab-activity-log-container" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                                <div id = "user-activity-list-container" class = "row">
                                    <ul id = "user-activity-list" class = "media-list">

                                    </ul>
                                </div>
                            </div>
                            @endif
                            <!-- / .tab-pane -->
                        </div>
                        <!-- / .tab-content -->
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('assets/admin/js/custom/user-manage.js')}}
@endsection