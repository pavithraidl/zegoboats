<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 15/12/16 15:02.
 */
?>

@extends('admin.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header transparent">
                    <h2>System <strong>Exceptions</strong></h2>
                    <div class="additional-btn">
                        <a id="btn-exception-reload" href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div class="data-table-toolbar">
                        <div class="row">
                            <div class="col-md-4">
                                <form role="form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                </form>
                            </div>
                            <div class="col-md-8">

                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table data-sortable class="table table-hover table-striped" style="min-height: 300px;">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Controller</th>
                                <th>Function</th>
                                <th>Exception</th>
                                <th>User</th>
                                <th>Dealer</th>
                                <th>Status</th>
                                <th>Occurred&nbsp;At</th>
                            </tr>
                            </thead>

                            <tbody id="exception-table-data">
                                {{--JQuery append--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    {{HTML::script('assets/admin/js/custom/exception.js')}}
@endsection