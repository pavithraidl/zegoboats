<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 17:58.
 */
?>
<div class = "md-modal md-fade-in-scale-up" id = "mm-new-user" style = "z-index: 50;max-height: 90%;overflow-y: auto;box-shadow: -3px -3px 3px rgba(136, 136, 136, 0.76);" ng-app>
    <div id="model-add-new-user" class = "md-content">
        <div class="widget" id="add-new-user-widget">
            <div class="widget-content padding">
                <h3 id = "model-supplier-contact-popup-header">Add a New User</h3>
                <div class = "row">
                    <div class = "form-group">
                        <label>Name</label>
                        <input id = "mm-user-name" onblur="requiredValidator(this, 'Please enter the user\'s name.');" onkeyup="requiredValidator(this, 'Please enter the user\'s name.');" type = "text" class = "form-control" placeholder = "Jone" maxlength = "32" required tabindex="1">
                    </div>
                    <div class = "form-group">
                        <label>Email</label>
                        <input id = "mm-user-email" onblur="checkEmailDuplicate();" onkeyup="emailValidator(this, 'Invalid email address.');" type = "email" class = "form-control" placeholder = "someone&#64;example.com" maxlength = "60" required tabindex="2">
                    </div>
                    <div class="form-group">
                        <label>Job Title</label>
                        <input id="mm-user-job-title" onblur="requiredValidator(this, 'Please fill the job title here')" onkeyup="requiredValidator(this, 'Please enter the job title here.');" type="text" class="form-control" placeholder="Job Title" maxlength="16" required tabindex="3" >
                    </div>
                    <p style = "margin-left: 30%">
                        <button id = "mm-btn-user-cancel" onclick="cancelUser();" class = "btn btn-danger md-close">Cancel</button>
                        <button id = "mm-btn-user-add" onclick="addUser();" type = "button" class = "btn btn-primary" tabindex="3">Send Request</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End div .md-content -->
</div><!-- End div .md-modal .md-3d-flip-vertical-->
