<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 13/12/16 15:10.
 */
?>
<div class = "md-modal md-fade-in-scale-up" id = "mm-new-dealer" style = "z-index: 50;max-height: 90%;overflow-y: auto;box-shadow: -3px -3px 3px rgba(136, 136, 136, 0.76);" ng-app>
    <div id="model-add-new-dealer" class = "md-content">
        <div class="widget" id="add-new-dealer-widget">
            <div class="widget-content padding">
                <h3>Add a New Dealer</h3>
                <div class = "row">
                    <div class = "form-group">
                        <label>Dealership Name</label>
                        <input id = "mm-dealer-identity" onblur="requiredValidator(this, 'Please enter the dealer\'s identity.')" onkeyup="requiredValidator(this, 'Please enter the dealer\'s identity.')" type = "text" class = "form-control" placeholder = "NZ - Mt Roskill" maxlength = "32" required tabindex="1">
                    </div>
                    <div class = "form-group">
                        <label>Main Contact Name</label>
                        <input id = "mm-dealer-name" onblur="requiredValidator(this, 'Please enter the dealer\'s name.');" onkeyup="requiredValidator(this, 'Please enter the dealer\'s name.');" type = "text" class = "form-control" placeholder = "Jone Doe" maxlength = "32" required tabindex="2">
                    </div>
                    <div class = "form-group">
                        <label>Main Contact Email</label>
                        <input id = "mm-dealer-email" onblur="emailValidator(this, 'Invalid email address.'); checkEmailDuplicate();" type = "email" class = "form-control" placeholder = "someone&#64;example.com" maxlength = "64" required tabindex="3">
                    </div>
                    <div class = "form-group">
                        <label>Website Link</label>
                        <input id = "mm-dealer-url" onblur="requiredValidator(this, 'Please enter the website URL.')" onkeyup="requiredValidator(this, 'Please enter the website URL.')" type = "url" class = "form-control" placeholder = "zego.co.nz" maxlength = "64" tabindex="4">
                    </div>
                    <p style = "margin-left: 30%">
                        <button id = "mm-btn-dealer-cancel" onclick="cancelDealer();" class="btn btn-danger md-close" tabindex="6">Cancel</button>
                        <button id = "mm-btn-dealer-add" type = "button" class = "btn btn-primary" tabindex="5">Send Request</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End div .md-content -->
</div><!-- End div .md-modal .md-3d-flip-vertical -->
